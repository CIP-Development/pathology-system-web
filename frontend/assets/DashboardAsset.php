<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/morris.js/morris.css',
        'bower_components/Ionicons/css/ionicons.min.css',
        'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'css/style_01.css',
        'css/style_02.css',
        'css/style_03.css',
        'css/style_04.css',
        'css/style_05.css',
        'css/style_06.css',
        'css/style_07.css',
        'css/style_08.css',
        'css/style_09.css',
        'css/style_10.css',
        'chartjs/node_modules/chart.js/dist/Chart.css',
        'chartjs/node_modules/chart.js/dist/Chart.min.css',
        'css/site.css',
        'css/fonts.css'
    ];
    public $js = [
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/jquery-ui/jquery-ui.min.js',
        'bower_components/datatables.net/js/jquery.dataTables.min.js',
        'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
        'bower_components/raphael/raphael.min.js',
        'bower_components/morris.js/morris.min.js',
        'bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
        'bower_components/jquery-knob/dist/jquery.knob.min.js',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'bower_components/fastclick/lib/fastclick.js',
        'dist/js/adminlte.min.js',
        'js/script_01.js',
        'js/script_02.js',
        'js/script_03.js',
        'js/script_04.js',
        'js/script_05.js',
        'js/script_06.js',
        'js/script_07.js',
        'js/script_08.js',
        'js/script_09.js',
        'js/script_10.js',
        'chartjs/node_modules/chart.js/dist/Chart.bundle.js',
        'chartjs/node_modules/chart.js/dist/Chart.bundle.min.js',
        'chartjs/node_modules/chart.js/dist/Chart.js',
        'chartjs/node_modules/chart.js/dist/Chart.min.js',
        'js/util.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
