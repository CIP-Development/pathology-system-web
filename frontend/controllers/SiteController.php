<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\debug\models\search\User;
use yii\helpers\Url;


use frontend\modules\business\models\Request;

class SiteController extends Controller
{
    const CONT_IMAGES = 17;
    const DEFT_IMAGE = 'dist/img/avatar0.jpg';
    const DEFT_IMAGE_PATH = 'dist/img/avatar';
    const DEFT_IMAGE_TYPE = '.png';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'signup-update', 'home'],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['home']);
        }
        return $this->render('index');
    }

    public function actionHome()
    {
        $countRequestMonth = count(Request::find()
            ->where(
                [
                    'status' => 'active',
                    'registeredBy' => Yii::$app->user->identity->email,
                ]
            )->andWhere(
                [
                    '>=', 'registeredAt', date("Y-m-01", time())
                ]
            )->asArray()->all());

        $countRequest = count(Request::find()
            ->where(
                [
                    'status' => 'active',
                    'registeredBy' => Yii::$app->user->identity->email,
                ]
            )
            ->asArray()->all());
        return $this->render(
            'home',
            [
                'countRequest' =>  $countRequest,
                'countRequestMonth' => $countRequestMonth,
            ]
        );
    }

    public function actionLogin()
    {


        // try {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();

            // echo "<pre>";
            // print_r("isGuest");
            // echo "</pre>";
            // die();
        }
        // } catch (\Throwable $th) {
        //     echo "<pre>";
        //     print_r($th->getMessage());
        //     echo "</pre>";
        //     die();
        // }

        // echo "<pre>";
        // print_r("Sin problemas");
        // echo "</pre>";
        // die();


        try {


            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {

                // echo "<pre>";
                // print_r("LOGIN");
                // echo "</pre>";
                // die();

                Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                return $this->goHome();
            } else if ($model->load(Yii::$app->request->post())) {


                // echo "<pre>";
                // print_r("NO LOGIN");
                // echo "</pre>";
                // die();


                if ($model->getIsInLdap()) {

                    // echo "<pre>";
                    // print_r("PETER RULES");
                    // echo "</pre>";
                    // die();


                    $this->setInUser();
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                } else {
                    // echo "<pre>";
                    // print_r("else getIsInLdap");
                    // echo "</pre>";
                    // die();


                    $model->password = '';
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>Not registered!</h4>The user and / or password values are not registered in the web application");
                }
            }
        } catch (\Throwable $th) {
            // echo "<pre>";
            // print_r($th->getMessage());
            // echo "</pre>";
            // die();
        }




        // echo "<pre>";
        // print_r("Sin problemas");
        // echo "</pre>";
        // die();





        return $this->renderAjax('login', [
            'model' => $model,
        ]);
    }

    public function setInUser()
    {
        $modelLoginForm = new LoginForm();
        $modelLoginForm->load(Yii::$app->request->post());
        $userId = $modelLoginForm->getIsInLocal();
        if ($userId >= 1 and !is_null($userId)) {
            $modelSignupForm = new SignupForm();
            $modelSignupForm->username = $modelLoginForm->getUserName();
            $modelSignupForm->email = $modelLoginForm->getMail();
            $modelSignupForm->password = $modelLoginForm->password;
            if ($user = $modelSignupForm->signup($userId)) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        } else {
            $modelSignupForm = new SignupForm();
            $modelSignupForm->username = $modelLoginForm->getUserName();
            $modelSignupForm->email = $modelLoginForm->getMail();
            $modelSignupForm->password = $modelLoginForm->password;
            if ($user = $modelSignupForm->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
    }

    public function actionSignup()
    {

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            } else {
                Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>Existing user!</h4>The data of the user entered already exist, try another username and email");
                return $this->render('signup', ['model' => $model,]);
            }
        }
        return $this->renderAjax('signup', [
            'model' => $model,
        ]);
    }

    public function actionSignupUpdate($photoUploaded = false)
    {

        $model = new SignupForm();
        $model->actionLoad(Yii::$app->user->identity->id);
        if ($model->load(Yii::$app->request->post())) {
            if (isset(Yii::$app->request->post()['RadioAvatar'])) {
                $model->photo = Yii::$app->request->post()['RadioAvatar'];
            }
            if ($user = $model->signup(Yii::$app->user->identity->id)) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['index']);
                }
            }
        }
        if ($photoUploaded) {
            $photoDefault = $photoUploaded;
        } else {
            $photoUploadedPng = $model->username . '_upload.png';
            $photoUploadedJpg = $model->username . '_upload.jpg';
            if (file_exists($photoUploadedPng)) {
                $photoDefault = $photoUploadedPng;
            } else if (file_exists($photoUploadedJpg)) {
                $photoDefault = $photoUploadedJpg;
            } else {
                $photoDefault = self::DEFT_IMAGE;
            }
        }
        $photoUser  = $model->photo;
        $countImages = self::CONT_IMAGES; // AUTOMATE
        for ($i = 0; $i < $countImages; $i++) {
            $arrayRadioAvatar[] = [
                'name' => 'RadioAvatar',
                'value' => self::DEFT_IMAGE_PATH . $i .  self::DEFT_IMAGE_TYPE,
                'checked' => false
            ];
        }
        $arrayRadioAvatar[0]['value'] = $photoDefault;
        foreach ($arrayRadioAvatar as $key => $value) {
            if ($value['value'] == $photoUser) {
                $arrayRadioAvatar[$key]['checked'] = true;
            }
        }
        return $this->render('signup-update', [
            'model' => $model,
            'arrayRadioAvatar' => $arrayRadioAvatar,
            'photoUser' => $photoUser,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionUpload()
    {
        $modelSignupForm = new SignupForm();
        $findUser = $modelSignupForm->findModel(Yii::$app->user->identity->id);
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $fileUploaded = $model->upload($findUser->username . "_upload");
            if ($fileUploaded) {
                Yii::$app->session->setFlash('success', 'The upload of the file was successful');
            } else {
                Yii::$app->session->setFlash('warning', 'The file could not be uploaded');
            }
            // Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(['signup-update', 'photoUploaded' => $fileUploaded]);
        }
        return $this->renderAjax('upload', [
            'model' => $model,
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
