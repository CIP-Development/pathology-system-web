<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Main */

$this->title = 'Update Main: ' . $model->CIPNUMBER_key;
$this->params['breadcrumbs'][] = ['label' => 'Mains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CIPNUMBER_key, 'url' => ['view', 'id' => $model->CIPNUMBER_key]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
