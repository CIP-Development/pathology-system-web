<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Support */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Supports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="support-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'columns',
            'rows',
            'totalSlots',
            'usedSlots',
            'availableSlots',
            'ctrlQty',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'requestId',
            'cropId',
            'workFlowId',
            'numOrderId',
            'activityId',
            'essayId',
            'supportTypeId',
        ],
    ]) ?>

</div>
