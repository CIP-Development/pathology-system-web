<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\SampleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Samples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sample-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sample', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'numOrder',
            'accessionCode',
            'accessionNumber',
            'accessionName',
            //'collectingCode',
            //'collectingNumber',
            //'labCode',
            //'labNumber',
            //'female',
            //'male',
            //'isBulk',
            //'observation',
            //'acquisitionRequest',
            //'distributionRequest',
            //'details',
            //'registeredBy',
            //'registeredAt',
            //'updatedBy',
            //'updatedAt',
            //'deletedBy',
            //'deletedAt',
            //'status',
            //'plate',
            //'requestId',
            //'cropId',
            //'workFlowId',
            //'numOrderId',
            //'sampleTypeId',
            //'sourceId',
            //'sampleCrossId',
            //'graftingCount',
            //'firstUserCode',
            //'secondUserCode',
            //'thirdUserCode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>