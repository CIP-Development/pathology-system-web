<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\SampleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sample-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'numOrder') ?>

    <?= $form->field($model, 'accessionCode') ?>

    <?= $form->field($model, 'accessionNumber') ?>

    <?= $form->field($model, 'accessionName') ?>

    <?php // echo $form->field($model, 'collectingCode') 
    ?>

    <?php // echo $form->field($model, 'collectingNumber') 
    ?>

    <?php // echo $form->field($model, 'labCode') 
    ?>

    <?php // echo $form->field($model, 'labNumber') 
    ?>

    <?php // echo $form->field($model, 'female') 
    ?>

    <?php // echo $form->field($model, 'male') 
    ?>

    <?php // echo $form->field($model, 'isBulk') 
    ?>

    <?php // echo $form->field($model, 'observation') 
    ?>

    <?php // echo $form->field($model, 'acquisitionRequest') 
    ?>

    <?php // echo $form->field($model, 'distributionRequest') 
    ?>

    <?php // echo $form->field($model, 'details') 
    ?>

    <?php // echo $form->field($model, 'registeredBy') 
    ?>

    <?php // echo $form->field($model, 'registeredAt') 
    ?>

    <?php // echo $form->field($model, 'updatedBy') 
    ?>

    <?php // echo $form->field($model, 'updatedAt') 
    ?>

    <?php // echo $form->field($model, 'deletedBy') 
    ?>

    <?php // echo $form->field($model, 'deletedAt') 
    ?>

    <?php // echo $form->field($model, 'status') 
    ?>

    <?php // echo $form->field($model, 'plate') 
    ?>

    <?php // echo $form->field($model, 'requestId') 
    ?>

    <?php // echo $form->field($model, 'cropId') 
    ?>

    <?php // echo $form->field($model, 'workFlowId') 
    ?>

    <?php // echo $form->field($model, 'numOrderId') 
    ?>

    <?php // echo $form->field($model, 'sampleTypeId') 
    ?>

    <?php // echo $form->field($model, 'sourceId') 
    ?>

    <?php // echo $form->field($model, 'sampleCrossId') 
    ?>

    <?php // echo $form->field($model, 'graftingCount') 
    ?>

    <?php // echo $form->field($model, 'firstUserCode') 
    ?>

    <?php // echo $form->field($model, 'secondUserCode') 
    ?>

    <?php // echo $form->field($model, 'thirdUserCode') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>