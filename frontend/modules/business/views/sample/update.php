<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Sample */

$this->title = 'Update Sample: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Samples', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sample-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
