<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\Sample */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Samples', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sample-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'numOrder',
            'accessionCode',
            'accessionNumber',
            'accessionName',
            'collectingCode',
            'collectingNumber',
            'labCode',
            'labNumber',
            'female',
            'male',
            'isBulk',
            'observation',
            'acquisitionRequest',
            'distributionRequest',
            'details',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'plate',
            'requestId',
            'cropId',
            'workFlowId',
            'numOrderId',
            'sampleTypeId',
            'sourceId',
            'sampleCrossId',
            'graftingCount',
            'firstUserCode',
            'secondUserCode',
            'thirdUserCode',
        ],
    ]) ?>

</div>