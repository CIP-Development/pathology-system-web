<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\MainIpSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-ip-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'accession_id_key') ?>

    <?= $form->field($model, 'CIPNUMBER') ?>

    <?= $form->field($model, 'CROP') ?>

    <?= $form->field($model, 'GENUS') ?>

    <?= $form->field($model, 'SPECIES') ?>

    <?php // echo $form->field($model, 'SUBTAXON') ?>

    <?php // echo $form->field($model, 'CULTVRNAME') ?>

    <?php // echo $form->field($model, 'OTHER_IDENT') ?>

    <?php // echo $form->field($model, 'FEMALE') ?>

    <?php // echo $form->field($model, 'MALE') ?>

    <?php // echo $form->field($model, 'BIOSTAT') ?>

    <?php // echo $form->field($model, 'DATERCVD') ?>

    <?php // echo $form->field($model, 'DATECOL') ?>

    <?php // echo $form->field($model, 'COLNUMBER') ?>

    <?php // echo $form->field($model, 'COUNTRY') ?>

    <?php // echo $form->field($model, 'ADMIN1') ?>

    <?php // echo $form->field($model, 'ADMIN2') ?>

    <?php // echo $form->field($model, 'ADMIN3') ?>

    <?php // echo $form->field($model, 'LOCALITY') ?>

    <?php // echo $form->field($model, 'LATCOOR') ?>

    <?php // echo $form->field($model, 'LONGCOOR') ?>

    <?php // echo $form->field($model, 'CATALOGUE') ?>

    <?php // echo $form->field($model, 'ORDERING') ?>

    <?php // echo $form->field($model, 'AVAILABLE') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'IDENTITY_RESULT') ?>

    <?php // echo $form->field($model, 'COMMENTS') ?>

    <?php // echo $form->field($model, 'MLSSTAT') ?>

    <?php // echo $form->field($model, 'TI') ?>

    <?php // echo $form->field($model, 'ANEXO1') ?>

    <?php // echo $form->field($model, 'Health_Status') ?>

    <?php // echo $form->field($model, 'PopulationGroup') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
