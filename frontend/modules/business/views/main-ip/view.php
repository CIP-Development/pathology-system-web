<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\MainIp */

$this->title = $model->accession_id_key;
$this->params['breadcrumbs'][] = ['label' => 'Main Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="main-ip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->accession_id_key], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->accession_id_key], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'accession_id_key',
            'CIPNUMBER',
            'CROP',
            'GENUS',
            'SPECIES',
            'SUBTAXON',
            'CULTVRNAME',
            'OTHER_IDENT',
            'FEMALE',
            'MALE',
            'BIOSTAT',
            'DATERCVD',
            'DATECOL',
            'COLNUMBER',
            'COUNTRY',
            'ADMIN1',
            'ADMIN2',
            'ADMIN3',
            'LOCALITY',
            'LATCOOR',
            'LONGCOOR',
            'CATALOGUE',
            'ORDERING',
            'AVAILABLE',
            'STATUS',
            'IDENTITY_RESULT',
            'COMMENTS',
            'MLSSTAT',
            'TI',
            'ANEXO1',
            'Health_Status',
            'PopulationGroup',
        ],
    ]) ?>

</div>
