<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\business\models\MainIp */

$this->title = 'Create Main Ip';
$this->params['breadcrumbs'][] = ['label' => 'Main Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-ip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
