<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;


// ---------------------------------------------------------------------
use frontend\modules\intelligence\models\RequestProcess;
use frontend\modules\intelligence\models\RequestProcessSearch;
use frontend\modules\intelligence\models\RequestProcessDetail;
use frontend\modules\intelligence\models\RequestProcessDetailSearch;
use frontend\modules\intelligence\models\AgentByRequestProcessDetail;
use frontend\modules\intelligence\models\ReadingData;
use frontend\modules\intelligence\models\ReadingDataSearch;
use frontend\modules\intelligence\models\EssayByWorkFlowSearch;


// ---------------------------------------------------------------------
use frontend\modules\control\models\AgentByEssay;
use frontend\modules\control\models\ReadingDataTypeByEssay;
use frontend\modules\control\models\EssayByWorkFlow;
// ---------------------------------------------------------------------
use frontend\modules\configuration\models\CipSample;
use frontend\modules\configuration\models\CipSampleSearch;
use frontend\modules\configuration\models\ReadingDataType;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Cost;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Symptom;
use frontend\modules\configuration\models\Location;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Notification;
use frontend\modules\configuration\models\Composition;
// ---------------------------------------------------------------------
use frontend\modules\business\models\Request;
use frontend\modules\business\models\RequestSearch;
use frontend\modules\business\models\Main;
use frontend\modules\business\models\MainSearch;
use frontend\modules\business\models\MainIp;
use frontend\modules\business\models\MainIpSearch;
use frontend\modules\business\models\Sample;
use frontend\modules\business\models\SampleSearch;
use frontend\modules\business\models\Support;
use frontend\modules\control\models\ActivityByEssay;

// ---------------------------------------------------------------------
?>



<div class="box collapsed-box">
  <div class="box-header with-border">

    <div class="row">



      <div class="col-md-1" style="text-align: center">
        <?= HtmlPurifier::process($model['numOrderId']) ?>
      </div>


      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process(Crop::findOne($model['cropId'])->longName) ?>
        </span>
      </div>

      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process(WorkFlow::findOne($model['workFlowId'])->shortName) ?>
        </span>
      </div>

      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process($model['essayQty']) ?>
        </span>
      </div>

      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process($model['agentQty']) ?>
        </span>
      </div>


      <div class="col-md-2" style="text-align: center">
        <?php $sampleStatus =  $model['sampleStatus'];
        if (empty($sampleStatus)) {
          echo '<span class="td-red">Empty sample load <i class="fa fa-exclamation"></i></span>';
        } else {
          echo '<span class="td-green">' . $sampleStatus . ' <i class="fa fa-check"></i></span>';
        } ?>
      </div>


      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process($model['sampleQty']) ?>
        </span>
      </div>


      <div class="col-md-1" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cube"></i> -->
          <?= HtmlPurifier::process(Yii::$app->formatter->asCurrency($model['subTotalCost'], '$')) ?>
        </span>
      </div>


      <div class="col-md-1" style="text-align: center">
        <!-- <span style="font-size: 12px;" class="badge bg-teal-gradient"> -->
        <span style="font-size: 12px;">
          <?php
          $processStatusIdValue = Parameter::findOne($model['processStatusId'])->shortName;

          echo ' <span class="td-' . processStatusIdValue . '">' .  $processStatusIdValue . '</span>';
          ?>
        </span>
      </div>


      <div class="col-md-2" style="text-align: center">
        <span style="font-size: 12px;">
          <!-- <i class="icon fa fa-cubes"></i> -->
          <?= HtmlPurifier::process($model['sampleDetail']) ?>
        </span>
      </div>

    </div>

  </div>

</div>