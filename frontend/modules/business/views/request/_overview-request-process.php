<?php

use yii\helpers\HtmlPurifier;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Parameter;
?>

<div class="render-item">
  <div class="box collapsed-box">
    <div class="box-header with-border">
      <div class="row">

        <div class="col-md-2" style="text-align: center">
          <?= HtmlPurifier::process($model['numOrderId']) ?>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <!-- <i class="icon fa fa-cubes"></i> -->
            <?= HtmlPurifier::process(Crop::findOne($model['cropId'])->longName) ?>
          </span>
        </div>

        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process(WorkFlow::findOne($model['workFlowId'])->shortName) ?>
          </span>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process($model['essayQty']) ?>
          </span>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process($model['agentQty']) ?>
          </span>
        </div>

        <div class="col-md-2" style="text-align: center">
          <?php $sampleStatus =  $model['sampleStatus']; ?>
          <?php if (empty($sampleStatus)) { ?>
            <span class="label label-red">Empty sample load<i class="fa fa-exclamation"></i></span>
          <?php   } else { ?>
            <i class="label label-green"><?= $sampleStatus ?> <i class="fa fa-check"></i></i>
          <?php   } ?>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process($model['sampleQty']) ?>
          </span>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process(Yii::$app->formatter->asCurrency($model['subTotalCost'], '$')) ?>
          </span>
        </div>

        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <?= ' <span class="label label-' . Parameter::findOne($model['processStatusId'])->shortName . '">' .  Parameter::findOne($model['processStatusId'])->shortName . '</span>';
            ?>
          </span>
        </div>

      </div>
    </div>
  </div>
</div>