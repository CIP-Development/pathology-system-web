<?php

use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Essay;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


\yii\web\YiiAsset::register($this);
?>
<div class="crop-view">
  <?php $form = ActiveForm::begin(); ?>

  <div class="row">
    <div class="col-lg-12">
      <table>
        <tr>
          <?php
          foreach ($requestProcessDetailArrayList as $key1 => $requestProcessDetail) {
            ?>
            <td>
              <?php foreach ($agentByRequestProcessDetailArrayList as $key2 => $agentByRequestProcessDetail) { ?>
















                <div style="margin: 10px;">
                  <label class="control-label">
                    <?php
                        $activityId = $requestProcessDetail['activityId'];
                        $activityName = Activity::findOne($activityId)->shortName;
                        $agentId = $agentByRequestProcessDetail['agentId'];
                        $agentName = Agent::findOne($agentId)->shortName;

                        $activityType = Activity::findOne($activityId)->activityType->shortName;
                        $activityDataSource = Activity::findOne($activityId)->activityDataSource->code;

                        $readingDataArray = array_filter($readingDataArrayList, function ($element) use ($activityId, $agentId) {
                          return ($element['activityId'] === $activityId and  $element['agentId'] ===  $agentId);
                        });

                        if (empty($readingDataArray)) {
                          $name = $activityDataSource . "|" . $activityType . "_" . $requestId . "_" . $numOrderId . "_" . $essayId . "_" . $sampleId . "_" . $activityId  . "_" . $agentId;
                          $value = "";
                        } else {
                          $readingData = array_values($readingDataArray)[0];
                          $name = $activityDataSource . "|" . $activityType . "_" . $requestId . "_" . $numOrderId . "_" . $essayId . "_" . $sampleId . "_" . $activityId  . "_" . $agentId
                            . "_" . $readingData['graftingNumberId'] . "_" . $readingData['symptomId'] . "_" . $readingData['cellPosition'] . "_" . $readingData['supportId'];
                          $value = $activityType == 'qualitative' ? $readingData['cResult'] : $readingData['qResult'];
                        }

                        if (strpos(strtolower($activityName), "agent")) {
                          echo ($agentName . ' ' . strtolower($activityName));
                        } else {
                          echo ($headerValue = str_replace("agent",  $agentName, strtolower($activityName)));
                        };
                        ?>
                  </label>

                  <?php
                      if ($activityType == 'qualitative') {
                        echo (Html::dropDownList(
                          $name,
                          $value,
                          [
                            'positive' => 'positive',
                            'negative' => 'negative',
                          ],
                          [
                            'class' => "form-control",
                          ]
                        ));
                      } else {
                        echo (Html::input(
                          'text',
                          $name,
                          $value,
                          ['class' => "form-control"]
                        ));
                      }
                      ?>
                  <br>
                </div>
















              <?php } ?>
            </td>
          <?php } ?>
        </tr>

        <tr>
          <td colspan="<?= count($requestProcessDetailArrayList) ?>">
            <div style="margin: 0px 10px;">

              <label class="control-label">Observation</label>
              <?= Html::input('text', 'observation', $observation, ['class' => "form-control"]) ?>

            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <?= Html::a(
        'Save',
        [
          'update-reading-data',
          'requestId' => $requestId,
          'numOrderId' => $numOrderId,
          'essayId' => $essayId,
          'sampleId' => $sampleId
        ],
        [
          'title' => 'Generate absorbance distribution',
          'data-method' => 'post',
          'class' => 'btn btn-primary',
          'style' => "margin: 10px;"
        ]
      ); ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>
</div>