<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use frontend\modules\configuration\models\Parameter;
?>


<div class="form-render">

  <div class="box collapsed-box">
    <div class="box-header with-border">

      <div class="row">



        <div class="col-md-2" style="text-align: center">
          <?= HtmlPurifier::process($model['code']) ?>
        </div>


        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;">
            <!-- <i class="icon fa fa-cubes"></i> -->
            <?= HtmlPurifier::process($model['agentQty']) ?>
          </span>
        </div>


        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;">
            <!-- <i class="icon fa fa-cube"></i> -->
            <?= HtmlPurifier::process($model['sampleQty']) ?>
          </span>
        </div>


        <div class="col-md-2" style="text-align: center">
          <!-- <span style="font-size: 12px;" class="badge bg-teal-gradient"> -->
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process($model['totalCost']) ?>
          </span>
        </div>





        <div class="col-md-1" style="text-align: center">
          <?php

          $statusCode = Parameter::findOne($model['requestStatusId'])->code;

          $statusValue = Parameter::findOne($model['requestStatusId'])->shortName;

          if ($statusCode == '1.1') {
            echo '<span class="td-blue">' . $statusValue . ' <i class="fa fa-pencil"></i></span>';
          } else if ($statusCode == '2.1') {
            echo '<span class="td-green">' . $statusValue . ' <i class="fa fa-check"></i></span>';
          } else if ($statusCode == '3.0') {
            echo '<span class="td-red">' . $statusValue . ' <i class="fa fa-exclamation"></i></span>';
          } else if ($statusCode == '3.1') {
            echo '<span class="td-blue">' . $statusValue . ' <i class="fa fa-hourglass-2"></i></span>';
          } else if ($statusCode == '3.2') {
            echo '<span class="td-green">' . $statusValue . ' <i class="fa fa-check"></i></span>';
          } else if ($statusCode == '4.1') {
            echo '<span class="td-blue">' . $statusValue . ' <i class="fa fa-hourglass-2"></i></span>';
          } else if ($statusCode == '5.1') {
            echo '<span class="td-green">' . $statusValue . ' <i class="fa fa-check"></i></span>';
          } else {
            echo '<span class="td-blue">' . $statusValue . ' <i class="fa fa-exclamation"></i></span>';
          }

          ?>
        </div>




        <div class="col-md-2" style="text-align: center">


          <?
          // = Html::button(
          //   "<span class='glyphicon glyphicon glyphicon-plus'></span>",
          //   [
          //     'title' => Yii::t('yii', 'Add Sample(s)'),
          //     'class' => 'btn btn-default btn-xs modal-add-action',
          //     'value' => Url::to(
          //       [
          //         'request/add',
          //         'index' => $model['numOrderId'],
          //       ]
          //     ),
          //   ]
          // ); 
          ?>

          <?= Html::button(
            "<span class='glyphicon glyphicon-eye-open'></span>",
            [
              'title' => Yii::t('yii', 'View'),
              'class' => 'btn btn-default btn-xs modal-report01-action',
              'value' => Url::to(
                [
                  'request/report',
                  // 'id' => $model->id,
                  'id' => $model['id'],
                ]
              ),

            ]
          );
          ?>


          <?
          // = Html::button(
          //   "<span class='glyphicon glyphicon glyphicon-plus'></span>",
          //   [
          //     'title' => Yii::t('yii', 'Add Sample(s)'),
          //     'class' => 'btn btn-default btn-xs modal-add-action',
          //     'value' => Url::to(
          //       [
          //         'request/add',
          //         'index' => $model['numOrderId'],
          //       ]
          //     ),
          //   ]
          // ); 
          ?>









        </div>

      </div>

    </div>

  </div>

</div>