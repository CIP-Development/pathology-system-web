<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use frontend\modules\configuration\models\Cost;
use frontend\modules\configuration\models\WorkFlow;
?>
<div class="render-item">
  <div class="box collapsed-box">
    <div class="box-header with-border">
      <div class="row">
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-cubes"></i> Test set:</span>
          <br />
          <span style="font-size: 12px;">
            <?= WorkFlow::findOne($model['workFlowId'])->shortName ?>
          </span>
        </div>
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-cube"></i> Tests:</span>
          <br />
          <?php foreach (WorkFlow::findOne($model['workFlowId'])->essays as $key => $value) { ?>
            <span style='font-size: 12px;' title='<?= $value->shortName ?> FEE: <?= Cost::findOne($value->costId)->cost ?> '> <?= $value->shortName ?> </span>
          <?php  } ?>
        </div>
        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-bug"></i> Agent #:</span>
          <br />
          <span style="font-size: 12px;" class="badge bg-teal-gradient">
            <?= HtmlPurifier::process($model['agentQty']) ?>
          </span>
        </div>
        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-leaf"></i> Sample #:</span>
          <br />
          <span style="font-size: 12px;">
            <?= HtmlPurifier::process($model['sampleQty']) ?>
          </span>
        </div>
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-money "></i> Sub total:</span>
          <br />
          <span style="font-size: 12px; ">
            <b>
              <?= Yii::$app->formatter->asCurrency($model['subTotalCost'], '$') ?>
            </b>
          </span>
        </div>
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-calculator"></i> Status:</span>

          <?php $paymentStatus =  $model['paymentStatus']; ?>
          <?php if (empty($paymentStatus)) { ?>
            <?= '<span class="label label-red">Uncovered payment<i class="fa fa-exclamation"></i></span>'; ?>
          <?php } else if ($paymentStatus == 'Underpayment') { ?>
            <?= '<span class="label label-yellow">' . $paymentStatus . ' <i class="fa fa-check"></i></span>'; ?>
          <?php } else { ?>
            <?= '<span class="label label-green">' . $paymentStatus . ' <i class="fa fa-check"></i></span>'; ?>
          <?php }  ?>
        </div>
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-plus "></i> Add payment(s):</span>
          <br />
          <?= Html::button(
            "<span class='glyphicon glyphicon glyphicon-plus'></span>",
            [
              'title' => Yii::t('yii', 'Add Payment(s)'),
              'class' => 'btn btn-default btn-xs modal-payment-action',
              'value' => Url::to(
                [
                  'request/search-payment',
                  'requestId' => $model['requestId'],
                  'numOrderId' => $model['numOrderId'],
                ]
              ),
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
</div>