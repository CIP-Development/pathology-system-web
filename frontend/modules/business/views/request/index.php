<?php

use frontend\modules\configuration\models\WorkFlow;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

$this->title = 'Request';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1>All my <?= Html::encode($this->title) ?>s</h1>

    <div class="row">
        <div class="modal fade" id="modal-report01" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-lg modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Report View <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-report01-content"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-report02" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class=" modal-xl modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Report View<?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-report02-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bars "></i> <?= Html::encode($this->title) ?> list</h3>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => ['style' => 'font-size:1em;'],
                'layout'       => "{items}",
                'rowOptions' => function ($model, $key)  use ($actionId) {
                    if ($key == $actionId) {
                        return ['class' => 'success_action'];
                    } else if ($model->status == "active") {
                        return ['class' => 'success'];
                    } else if ($model->status == "disabled") {
                        return ['class' => 'danger'];
                    } else {
                        return ['class' => 'default'];
                    }
                },
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header'        => 'Order',
                    ],
                    [
                        'attribute'     => 'code',
                        'header'        => 'Code number',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'id',
                        'header'        => 'Essays',
                        'value'         => function ($model) {
                            return implode(',  ', ArrayHelper::map($model->requestProcesses, 'numOrderId', 'essayDetail'));
                        },
                        'headerOptions' => ['min-width' => '250'],
                        'enableSorting' => false,
                    ],
                    // [
                    //     'attribute'     => 'agentQty',
                    //     'header'        => 'Pathogen #',
                    //     'enableSorting' => false,
                    // ],
                    [
                        'header'        => 'Pathogen #',
                        'value'         => function ($model) {
                            return implode(', ', ArrayHelper::map($model->requestProcesses, 'numOrderId', 'agentQty'));
                        },
                        'headerOptions' => ['min-width' => '150'],
                        'enableSorting' => false,
                    ],
                    // [
                    //     'attribute'     => 'sampleQty',
                    //     'header'        => 'Sample #',
                    //     'enableSorting' => false,
                    // ],
                    [
                        'header'        => 'Sample #',
                        'value'         => function ($model) {
                            return implode(', ', ArrayHelper::map($model->requestProcesses, 'numOrderId', 'sampleQty'));
                        },
                        'headerOptions' => ['min-width' => '150'],
                        'enableSorting' => false,
                    ],
                    // [
                    //     'attribute'     => 'totalCost',
                    //     'header'        => 'Total fee',
                    //     'enableSorting' => false,
                    // ],
                    [
                        'header'        => 'Total fee',
                        'value'         => function ($model) {
                            return  Yii::$app->formatter->asCurrency(array_sum(ArrayHelper::map($model->requestProcesses, 'numOrderId', 'subTotalCost')), '$');
                        },
                        'headerOptions' => ['min-width' => '150'],
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'requestStatusId',
                        'header'        => 'Test status',
                        'value'         => 'requestStatus.shortName',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute'     => 'details',
                        'header'        => 'Detail/Comments',
                        'enableSorting' => false,
                    ],
                    // [
                    //     'class' => 'yii\grid\ActionColumn',
                    //     'header'        => 'Overview',
                    //     'template'      => '{link}',
                    //     'headerOptions' => ['min-width' => '100'],
                    //     'buttons'    => [
                    //         'link' => function ($url, $model, $key) {
                    //             return Html::button(
                    //                 "<span class='glyphicon glyphicon-eye-open'></span>",
                    //                 [
                    //                     'title' => Yii::t('yii', 'View'),
                    //                     'class' => 'btn btn-default btn-xs modal-report01-action',
                    //                     'value' => Url::to(
                    //                         [
                    //                             'request/overview',
                    //                             'id' => $key,
                    //                         ]
                    //                     ),
                    //                 ]
                    //             );
                    //         },
                    //     ]
                    // ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'        => 'Manage Request',
                        'template'      => '{link}',
                        'headerOptions' => ['min-width' => '100'],
                        'buttons'       => [
                            'link' => function ($url, $model, $key) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-th-list"></span>',
                                    [
                                        'manage-request',
                                        'requestId' => $key,
                                    ],
                                    [
                                        'class' => 'btn btn-default btn-xs',
                                    ]
                                );
                            },
                        ],
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'        => 'Remove',
                        'template'      => '{link}',
                        'headerOptions' => ['width' => '50'],
                        'buttons'       => [
                            'link' => function ($url, $model, $key) {
                                return Html::a(
                                    '<span class="fa fa-trash" style="color:red"></span>',
                                    [
                                        'manage-remove',
                                        'requestId' => $key,
                                    ],
                                    [
                                        'data' => [
                                            'confirm' => 'ARE YOU SURE YOU WANT TO DELETE THIS REQUEST?',
                                            'method' => 'post',
                                        ],
                                    ]
                                );
                            },
                        ],
                    ],

                ],
                'tableOptions' => [
                    'class' => 'table table-bordered table-striped dataTable',
                    'id'    => "gvRequest",
                ],
            ]);
            ?>
        </div>
        <div class="box-footer">
            <div class="col-md-12">
                <?= Html::a(
                    'Create Request',
                    [
                        'create'
                    ],
                    [
                        'class' => 'btn btn-success'
                    ]
                ) ?>
            </div>
        </div>
    </div>

</div>