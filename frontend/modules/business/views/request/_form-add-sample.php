<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\WorkFlow;
?>
<div class="render-item">
  <div class="box collapsed-box">
    <div class="box-header with-border">
      <div class="row">
        <!-- Nbr. Order -->
        <div class="col-md-1" style="text-align: center">
          <?= Html::a(
            $model['numOrderId'],
            [
              'edit',
              'requestId' => $model['requestId'],
              'numOrderId' => $model['numOrderId'],
            ],
            [
              'class' => 'btn btn-default btn-xs font-weight-bold',
              'title' => HtmlPurifier::process($model['numOrderId']),
              'data-method' => 'post',
            ]
          ); ?>
        </div>
        <!-- Crop  -->
        <div class="col-md-1" style="text-align: center">
          <?= HtmlPurifier::process(Crop::findOne($model['cropId'])->longName) ?>
        </div>
        <!-- Test set -->
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-cubes"></i>
            <?= HtmlPurifier::process(WorkFlow::findOne($model['workFlowId'])->shortName) ?>
          </span>
        </div>
        <!-- Test # -->
        <div class="col-md-1" style="text-align: center">
          <span style="font-size: 12px;"><i class="icon fa fa-cube"></i>
            <?= HtmlPurifier::process($model['essayQty']) ?>
          </span>
        </div>
        <!-- Pathogen $  -->
        <div class="col-md-2" style="text-align: center">
          <span style="font-size: 12px;" class="badge bg-teal-gradient">
            <?= HtmlPurifier::process($model['agentQty']) ?>
          </span>
        </div>
        <!-- Sample #  -->
        <div class="col-md-1" style="text-align: center">
          <?= HtmlPurifier::process($model['sampleQty']) ?>
        </div>
        <!-- Status  -->
        <div class="col-md-2" style="text-align: center">
          <?php $sampleStatus =  $model['sampleStatus']; ?>
          <?php if (empty($sampleStatus)) { ?>
            <?= '<span class="label label-red">Empty sample load <i class="fa fa-exclamation"></i></span>'; ?>
          <?php } else { ?>
            <?= '<span class="label label-green">' . $sampleStatus . ' <i class="fa fa-check"></i></span>'; ?>
          <?php } ?>
        </div>
        <!-- Actions  -->
        <div class="col-md-2" style="text-align: center">
          <?= Html::button(
            "<span class='glyphicon glyphicon glyphicon-plus'></span>",
            [
              'class' => 'btn btn-default btn-xs modal-add-action',
              'title' => Yii::t('yii', 'Add Sample(s)'),
              'value' => Url::to(
                [
                  'request/search-sample',
                  'requestId' => $model['requestId'],
                  'numOrderId' => $model['numOrderId'],
                ]
              ),
            ]
          ); ?>
          <?= Html::button(
            "<span class='glyphicon glyphicon-leaf'></span>",
            [
              'class' => 'btn btn-default btn-xs modal-config-action',
              'title' => Yii::t('yii', 'Show Sample(s)'),
              'value' => Url::to(
                [
                  'request/config-workflow',
                  'requestId' => $model['requestId'],
                  'numOrderId' => $model['numOrderId'],
                ]
              ),
            ]
          ); ?>
          <?= Html::a(
            "<span class='glyphicon glyphicon-trash'></span>",
            [
              'remove',
              'requestId' => $model['requestId'],
              'numOrderId' => $model['numOrderId'],
            ],
            [
              'class' => 'btn btn-default btn-xs',
              'title' => 'Remove Sample(s)',
              'data-method' => 'post',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
</div>