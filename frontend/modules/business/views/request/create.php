<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = 'Create Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$js_code_request_create =
    <<<JS
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script language="JavaScript">
$(".modal-add-action").click(function() {
	$("#modal-add")
		.modal("show")
		.find(".modal-add-content")
		.load($(this).attr("value"));
	return false;
});
$(".modal-config-action").click(function() {
	$("#modal-config")
		.modal("show")
		.find(".modal-config-content")
		.load($(this).attr("value"));
	return false;
});
$(".modal-payment-action").click(function() {
	$("#modal-payment")
		.modal("show")
		.find(".modal-payment-content")
		.load($(this).attr("value"));
	return false;
});
</script>

JS;
?>

<div class="request-create">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ MODAL-->

    <div class="row">

        <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Add sample(s)</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-add-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-config" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Config workflow</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-config-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Add payment(s)</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-payment-content"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-save" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Save request</h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-save-content"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php Pjax::begin(['id' => 'pjax-request-create']); ?>
    <?php $form_request_create = ActiveForm::begin(['id' => 'form-request-create', 'options' => ['data-pjax' => true, 'id' => 'dynamic-form-request-create',]]); ?>
    <?= $js_code_request_create ?>
    <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ FORM-->


    <!-- CROP-WORKFLOW-ESSAY-AGENTS -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Create a new Request</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row justify-content-md-justify">
                <div class="col-lg-12 ">
                    <span class="info">
                        <i class="icon fa fa-info"></i> Set information about the Request. It is necessary to select at least one box from the list of "Pathogens" / "Indicator plants" of each "Test" that belongs to the selected Workflow.
                    </span>
                </div>
            </div>
            <br />
            <div class="row justify-content-md-justify">
                <?= $this->render('_form-add-essay-agent', [
                    'selectCropValue' => $selectCropValue,
                    'selectWorkFlowValue' => $selectWorkFlowValue,
                    'arrayMapCrop' => $arrayMapCrop,
                    'arrayMapWorkFlow' => $arrayMapWorkFlow,
                    'arrayMapEssayAgent' => $arrayMapEssayAgent,
                ]) ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="col-sm-12 col-md-12 col-lg-12">


                <?= Html::a(
                    'Add essay',
                    [
                        'detail', 'requestId' => $requestId,
                    ],
                    [
                        'class' => 'btn btn-primary pull-right',
                        'title' => 'Add essay',
                        'data-method' => 'post',
                    ]
                ); ?>


            </div>
        </div>
    </div>


    <!-- SAMPLES -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add sample(s)</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row justify-content-md-justify">
                <div class="col-lg-12 ">
                    <span class="info">
                        <i class="icon fa fa-info"></i> Add samples to the request by clicking the plus button:
                    </span>
                </div>
            </div>
            <br />
            <div class="row justify-content-md-justify">
                <div class="col-lg-12">
                    <table class="new-table">
                        <thead>
                            <tr role="row">
                                <th class="col-md-1">Order</th>
                                <th class="col-md-1">Crop</th>
                                <th class="col-md-2">Test set</th>
                                <th class="col-md-1">Test #</th>
                                <th class="col-md-2">Agent #</th>
                                <th class="col-md-1">Sample #</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">Actions</th>
                            </tr>
                        </thead>
                    </table>
                    <br />
                    <div class="new-table">
                        <?= ListView::widget(
                            [
                                'dataProvider' => $dataProviderRequestProcess,
                                'itemView' => '_form-add-sample',
                                'layout' => '{items}{pager}{summary}',

                                'pager' => [
                                    'firstPageLabel' => 'first',
                                    'lastPageLabel' => 'last',
                                    'prevPageLabel' => 'previous',
                                    'nextPageLabel' => 'next',
                                ],
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- PAYMENT -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add payment(s)</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <div class="row justify-content-md-justify">
                <div class="col-lg-12 ">
                    <span class="info">
                        <i class="icon fa fa-info"></i> Add financial information:
                    </span>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-lg-12">
                    <div class="new-table">
                        <?= ListView::widget(
                            [
                                'dataProvider' => $dataProviderRequestProcess,
                                'itemView' => '_form-add-payment',
                                'layout' => '{items}',
                            ]
                        );
                        ?>
                    </div>

                    <label class="control-label">Total Cost</label>
                    <span style="font-size: 12px;">
                        <?php
                        $totalCost = 0;
                        if (is_null($dataProviderRequestProcess->allModels)) {
                            $totalCost = 0;
                        } else {
                            foreach ($dataProviderRequestProcess->allModels as $key => $value) {
                                $totalCost = $totalCost + $value['subTotalCost'];
                            }
                        }
                        ?>
                        <?= Html::textInput(
                            'totalCostTextInput',
                            Yii::$app->formatter->asCurrency($totalCost, '$'),
                            [
                                'class' => 'form-control',
                                'readOnly' => 'true',
                                'style' => 'text-align:right; font-weight: bold'
                            ]
                        ) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>


    <!-- SAVE -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <?= Html::button(
                'Create',
                [
                    'title' => Yii::t('yii', 'Save'),
                    'class' => 'btn btn-primary pull-right modal-save-action',
                    'value' => Url::to(
                        [
                            'request/save-request',
                            'requestId' => $requestId
                        ]
                    ),
                ]
            ); ?>
        </div>
    </div>


    <!-- ♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦ END-->


    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>



</div>