<?php

use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
?>

<?php
$js_code_manage_request_workflow = <<<JS
<script language="JavaScript" type="text/javascript" src="bower_components/jquery-ui/jquery-ui.min.js">
$('.gvDefaultWorkflow').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
  });
</script>
JS;
?>
<?= $js_code_manage_request_workflow ?>
<div class="render-content">

  <div class="row">

    <div class="col-md-3">

      <span style="font-size: 12px;">
        Workflow (<?= $arrayValueRequestProcess['numOrderId'] ?>): <b> <?= Workflow::findOne($arrayValueRequestProcess['workFlowId'])->shortName ?> </b>
      </span> <br /><br />

      <span style="font-size: 12px;" title="Total essays within the Workflow">
        Total essays: <b> <?= $arrayValueRequestProcess['essayQty'] ?></b>
      </span> <br /><br />

      <span style="font-size: 12px;" title="Total agents in all essays from the workflow">
        Total agents: <b> <?= $arrayValueRequestProcess['agentQty'] ?></b>
      </span> <br /><br />

      <span style="font-size: 12px;" title="Total samples for each essay within the workflow">
        Total samples: <b> <?= $arrayValueRequestProcess['sampleQty'] ?></b>
      </span> <br /><br />

    </div>

    <div class="col-md-9 border-left">



      <?php foreach (ArrayHelper::map($arrayAgentByRequestProcessDetail, 'agentId', 'agentId', 'essayId') as $key => $variable) { ?>

        <div class="row">
          <div style="text-align: left" class="col-md-12">
            <span style="font-size: 12px;">
              Essay: <b> <?= Essay::findOne($key)->shortName ?> <small> <?= Essay::findOne($key)->longName  ?> </small></b>
            </span>
          </div>
          <?php foreach ($variable as $value) { ?>
            <div style="text-align: left; margin-top: 10px;" class="col-xs-6 col-sm-6 col-md-4 col-lg-3" title="<?= HtmlPurifier::process(Agent::findOne($value)->longName) ?>">
              <span style="font-size: 11px;" class="badge bg-teal-gradient">
                <?= HtmlPurifier::process(Agent::findOne($value)->shortName) ?>
                <br />
                <small><?= HtmlPurifier::process(Agent::findOne($value)->longName) ?></small>
              </span>
            </div>
          <?php } ?>
        </div>
        <br />
      <?php } ?>
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-md-12">
      <?php
      $dataProviderSample =  new ArrayDataProvider(
        [
          'allModels' => $arrayListSample,
          'pagination' => ['pageSize' => 0,],
          //'sort' => ['attributes' => ['cropValue', 'workFlowValue', 'essayQty', 'agentQty', 'sampleQty', 'sampleStatus'],],
          //'key' => 'numOrderId',
          //'id' => $requestId
        ]
      );
      ?>
      <?= GridView::widget([
        'dataProvider' => $dataProviderSample,
        'options' => ['style' => 'font-size:1em;'],
        'layout'       => "{items}",
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          // 'id',
          // 'numOrder',

          [
            'attribute' =>  'accessionCode',
            'header' => 'Accession number'
          ],
          // [
          //   'attribute' =>  'labCode',
          //   // 'header' => 'Accession number'
          // ],
          [
            'attribute' =>  'firstUserCode',
            'header' => 'Other Code 1'
          ],
          [
            'attribute' =>  'secondUserCode',
            'header' => 'Other Code 2'
          ],
          // 'accessionNumber',
          [
            'attribute' =>  'collectingCode',
            'header' => 'Col number/Breeder code'
          ],

          [
            'attribute' =>  'accessionName',
            'header' => 'Accession name'
          ],
          // 'collectingNumber',
          // 'labCode',
          // 'labNumber',
          [
            'attribute' =>  'female',
            'header' => 'Female parent'
          ],
          [
            'attribute' =>  'male',
            'header' => 'Male parent'
          ],

          [
            'attribute'     => 'cropId',
            'header' => 'Crop',
            'value'     =>  function ($model) {
              return Crop::findOne($model['cropId'])->longName;
            },
            'enableSorting' => false,
            'format' => 'raw',
          ],

          'isBulk',
          //'observation',
          // 'acquisitionRequest',
          // 'distributionRequest',
          // 'details',

          [
            'attribute'     => 'sampleTypeId',
            'value'     =>  function ($model) {
              return Parameter::findOne($model['sampleTypeId'])->shortName;
            },
            'enableSorting' => false,
            'format' => 'raw',
          ],

          // [
          //   'attribute'     => 'sampleCrossId',
          //   'value'     =>  function ($model) {
          //     //return ($model['sampleCrossId']);
          //     return Parameter::findOne($model['sampleCrossId'])->shortName;
          //   },
          //   'enableSorting' => false,
          //   'format' => 'raw',
          // ],

          // [
          //   'attribute'     => 'sourceId',
          //   'value'     =>  function ($model) {
          //     return Parameter::findOne($model['sourceId'])->shortName;
          //   },
          //   'enableSorting' => false,
          //   'format' => 'raw',
          // ],


          // ['class' => 'yii\grid\ActionColumn'],
        ],
        'tableOptions' => [
          'class' => 'table table-bordered table-striped dataTable gvDefault gvDefaultWorkflow',
          //'id'    => "gvSamples" . $arrayValueRequestProcess['numOrderId'],
        ],
      ]); ?>
    </div>
  </div>

</div>