<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<?php
$js_code_add_sample = <<<JS
<script language="JavaScript">
$('#gvSamples').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
  });
</script>
JS;
?>

<div class="add-sample">
    <?php Pjax::begin(['id' => 'pjax-add-sample']); ?>
    <?php $form_add_sample = ActiveForm::begin(
        [
            'id' => 'form-add-sample',
            'options' =>
            [
                'data-pjax' => true,
                'id' => 'dynamic-form-add-sample',
            ]
        ]
    ); ?>
    <?= $js_code_add_sample ?>

    <div class="box-body">
        <div class="row">
            <div class="col-lg-3 small-padding">
                <fieldset class="groupbox-border">
                    <legend class="groupbox-border">
                        <span class='text-teal'>
                            <h4>Sample information</h4>
                        </span>
                    </legend>
                    <div class="content-light">
                        <label class="control-label">*Sample type</label>
                        <?= Html::dropDownList(
                            'sampleTypeId',
                            Yii::$app->request->post('sampleTypeId'),
                            ArrayHelper::map(
                                Parameter::find()->where([
                                    'deletedBy' => null,
                                    'deletedAt' => null,
                                    'entity' => 'SAMPLE',
                                    'singularity' => 'TYPE',
                                ])->all(),
                                'id',
                                'shortName'
                            ),
                            [
                                'class' => 'form-control',
                                'prompt' => 'Select...',
                            ]
                        ); ?>
                        <br />
                        <label class="control-label">*Is it bulk?</label>
                        <?= Html::dropDownList(
                            'sampleIsBulk',
                            Yii::$app->request->post('sampleIsBulk'),
                            array(
                                'no' => "No",
                                'yes' => "Yes",
                            ),
                            [
                                'class' => 'form-control',
                                'prompt' => 'Select...',
                                'options' => ['no' => ['Selected' => 'selected']]
                            ]
                        ); ?>
                        <br />
                        <label class="control-label">*Enter samples</label>
                        <?= Html::textArea(
                            'samples',
                            Yii::$app->request->post('samples'),
                            [
                                'class' => 'form-control',
                                'style' => 'height: 400px;',
                            ]
                        ) ?>
                        <br />
                        <?= Html::a(
                            'Search',
                            [
                                'search-sample',
                                'requestId' => $requestId,
                                'numOrderId' => $numOrderId,
                            ],
                            [
                                'class' => 'btn btn-default btn-sm pull-right',
                                'title' => 'Search in local sample source',
                                'data-method' => 'post',
                            ]
                        ); ?>
                    </div>
                </fieldset>
            </div>
            <div class="col-lg-9 small-padding">
                <fieldset class="groupbox-border">
                    <legend class="groupbox-border">
                        <span class='text-teal'>
                            <h4>Sample details</h4>
                        </span>
                    </legend>
                    <div class="content-light ">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout'       => "{items}",
                            'options' => ['style' => 'font-size:1em;'],
                            'columns' => [
                                [
                                    'class' => 'yii\grid\SerialColumn',
                                    'header'        => 'Order',
                                ],
                                [
                                    'attribute'     => 'CIPNUMBER',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'COLNUMBER',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'CULTVRNAME',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'FEMALE',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'MALE',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'CROP',
                                    'enableSorting' => false,
                                ],
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered table-striped dataTable',
                                'id'    => "gvSamples",
                            ],
                        ]); ?>
                        <span class="info">
                            <i class="icon fa fa-info"></i> Only 1000 entries will be displayed, but all entered are selected.
                        </span>
                    </div>
                </fieldset>
                <fieldset class="groupbox-border">
                    <legend class="groupbox-border">
                        <span class='text-teal'>
                            <h4>Observations</h4>
                        </span>
                    </legend>
                    <div class="content-light max-height-300">
                        <?= $summary ?>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <div class="btn-group  pull-right">
            <?= Html::a(
                'Add sample(s)',
                [
                    'add-sample',
                    'requestId' => $requestId,
                    'numOrderId' => $numOrderId,
                ],
                [
                    'class' => 'btn btn-primary',
                    'title' => 'Add',
                    'data-method' => 'post',
                ]
            ); ?>
            <?= Html::a(
                'Close',
                null,
                [
                    'class' => 'btn btn-default',
                    'title' => 'Close',
                    'data-dismiss' => 'modal',

                ]
            ); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>