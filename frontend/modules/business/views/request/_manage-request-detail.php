<?php

use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Activity;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
?>



<div class="render-content">


  <div class="row">

    <div class="col-sm-4">
      <span style="font-size: 12px;">Request ID: <b> <?= $modelRequest->code ?></b></span>
      <br /><br />
      <span style="font-size: 12px;">User: <b> <?= $modelRequest->user->firstName . " " . $modelRequest->user->lastName ?></b></span>
      <br /><br />
      <span style="font-size: 12px;">Request User: <b> <?= $modelRequest->registeredBy ?></b></span>
      <br /><br />
      <span style="font-size: 12px;">Owner: <b> <?= implode('; ', array_unique(ArrayHelper::map($arrayListFinancialConceptOne, 'task', 'owner'))) ?></b></span>
    </div>

    <div class="col-sm-4">

      <span style="font-size: 12px;">Details/Comments: <b> <?= $modelRequest->details ?></b></span>

      <br /><br />

      <span style="font-size: 12px;">Total workflows: <b> <?= count($arrayRequestProcess) ?></b></span>

      <br /><br />

      <span style="font-size: 12px;">Total agents:
        <b>
          <?= array_sum(ArrayHelper::map($arrayRequestProcess, 'numOrderId', 'agentQty')) ?>
        </b>
      </span>

      <br /><br />

      <span style="font-size: 12px;">Total samples:
        <b>
          <?= array_sum(ArrayHelper::map($arrayRequestProcess, 'numOrderId', 'sampleQty')) ?>
        </b>
      </span>

    </div>

    <div class="col-sm-4">

      <span style="font-size: 12px;">Total amount:
        <b>
          <?= HtmlPurifier::process(Yii::$app->formatter->asCurrency(array_sum(ArrayHelper::map($arrayRequestProcess, 'numOrderId', 'subTotalCost')), '$')) ?>
        </b>
      </span>

      <br /><br />

      <span style="font-size: 12px;">Status:</span>

      <span style="font-size: 12px;">

        <b class="label label-<?= $modelRequest->requestStatus->description ?>">

          <?= $modelRequest->requestStatus->shortName ?>

        </b>

      </span>

      <br /><br />

    </div>

  </div>

</div>