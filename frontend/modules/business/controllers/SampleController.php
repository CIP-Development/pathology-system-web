<?php

namespace frontend\modules\business\controllers;

use Yii;
use frontend\modules\business\models\Sample;
use frontend\modules\business\models\SampleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SampleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sample models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SampleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sample model.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId),
        ]);
    }

    /**
     * Creates a new Sample model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sample();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sample model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        $model = $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sample model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        $this->findModel($id, $requestId, $cropId, $workFlowId, $numOrderId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sample model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $requestId
     * @param integer $cropId
     * @param integer $workFlowId
     * @param integer $numOrderId
     * @return Sample the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        if (($model = Sample::findOne(['id' => $id, 'requestId' => $requestId, 'cropId' => $cropId, 'workFlowId' => $workFlowId, 'numOrderId' => $numOrderId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
