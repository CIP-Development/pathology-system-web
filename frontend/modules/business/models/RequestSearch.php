<?php

namespace frontend\modules\business\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\business\models\Request;

/**
 * RequestSearch represents the model behind the search form of `frontend\modules\business\models\Request`.
 */
class RequestSearch extends Request
{
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'agentQty',
                    'sampleQty',
                    'requestTypeId',
                    'paymentTypeId',
                    'requestStatusId',
                    'userId'
                ], 'integer'
            ],
            [
                [
                    'code',
                    'details',
                    'diagnostic',
                    'creationDate',
                    'approvalDate',
                    'rejectionDate',
                    'uploadDate',
                    'validationDate',
                    'reprocessDate',
                    'notificationDate',
                    'verificationDate',
                    'registeredBy',
                    'registeredAt',
                    'updatedBy',
                    'updatedAt',
                    'deletedBy',
                    'deletedAt',
                    'status',
                    'payNumber'
                ], 'safe'
            ],
            [
                [
                    'totalCost'
                ], 'number'
            ],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $auth = Yii::$app->authManager;
        $user_id = Yii::$app->user->identity->id;
        $user_mail = Yii::$app->user->identity->email;
        $role = $auth->getRolesByUser($user_id);
        $query = Request::find();

        if (array_key_exists('system_admin', $role)) {
            // NO FILTERS
        } else if (array_key_exists('business_admin', $role)) {
            $query->andWhere(
                [
                    'Request.deletedBy' => null,
                    'Request.deletedAt' => null
                ]
            );
        } else if (array_key_exists('tech_role', $role) or array_key_exists('lab_role', $role) or array_key_exists('green_role', $role)) {
            $query->andWhere(
                [
                    'Request.deletedBy' => null,
                    'Request.deletedAt' => null
                ]
            );
        } else if (array_key_exists('client', $role)) {
            $query->andWhere(
                [
                    'Request.status' => 'active',
                    'Request.deletedBy' => null,
                    'Request.deletedAt' => null,
                    'Request.userId' =>  $user_id
                ]
            );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                'id' => SORT_DESC
            ]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(
            [
                'requestType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );
        $query->joinWith(
            [
                'paymentType' => function ($query) {
                    $query->from(['B' => 'Parameter']);
                }
            ]
        );
        $query->joinWith(
            [
                'requestStatus' => function ($query) {
                    $query->from(['C' => 'Parameter']);
                }
            ]
        );

        $query->andFilterWhere([
            'id' => $this->id,
            'agentQty' => $this->agentQty,
            'sampleQty' => $this->sampleQty,
            'totalCost' => $this->totalCost,
            'creationDate' => $this->creationDate,
            'approvalDate' => $this->approvalDate,
            'rejectionDate' => $this->rejectionDate,
            'uploadDate' => $this->uploadDate,
            'validationDate' => $this->validationDate,
            'reprocessDate' => $this->reprocessDate,
            'notificationDate' => $this->notificationDate,
            'verificationDate' => $this->verificationDate,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'Request.code', $this->code])
            ->andFilterWhere(['like', 'Request.details', $this->details])
            ->andFilterWhere(['like', 'Request.diagnostic', $this->diagnostic])
            ->andFilterWhere(['like', 'Request.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Request.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Request.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Request.status', $this->status])
            ->andFilterWhere(['like', 'Request.payNumber', $this->payNumber])
            ->andFilterWhere(['like', 'A.shortName', $this->requestTypeId])
            ->andFilterWhere(['like', 'B.shortName', $this->paymentTypeId])
            ->andFilterWhere(['like', 'C.shortName', $this->requestStatusId]);

        return $dataProvider;
    }
}
