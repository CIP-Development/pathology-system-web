<?php

namespace frontend\modules\business\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\business\models\Support;

/**
 * SupportSearch represents the model behind the search form of `frontend\modules\business\models\Support`.
 */
class SupportSearch extends Support
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'columns', 'rows', 'totalSlots', 'usedSlots', 'availableSlots', 'ctrlQty', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'supportTypeId'], 'integer'],
            [['registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Support::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'columns' => $this->columns,
            'rows' => $this->rows,
            'totalSlots' => $this->totalSlots,
            'usedSlots' => $this->usedSlots,
            'availableSlots' => $this->availableSlots,
            'ctrlQty' => $this->ctrlQty,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'requestId' => $this->requestId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'numOrderId' => $this->numOrderId,
            'activityId' => $this->activityId,
            'essayId' => $this->essayId,
            'supportTypeId' => $this->supportTypeId,
        ]);

        $query->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
