<?php

namespace frontend\modules\business\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\business\models\MainIp;

/**
 * MainIpSearch represents the model behind the search form of `frontend\modules\business\models\MainIp`.
 */
class MainIpSearch extends MainIp
{
    private $_selections = [];
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accession_id_key', 'CIPNUMBER', 'CROP', 'GENUS', 'SPECIES', 'SUBTAXON', 'CULTVRNAME', 'OTHER_IDENT', 'FEMALE', 'MALE', 'BIOSTAT', 'COLNUMBER', 'COUNTRY', 'ADMIN1', 'ADMIN2', 'ADMIN3', 'LOCALITY', 'CATALOGUE', 'ORDERING', 'AVAILABLE', 'STATUS', 'IDENTITY_RESULT', 'COMMENTS', 'MLSSTAT', 'TI', 'ANEXO1', 'Health_Status', 'PopulationGroup'], 'safe'],
            [['DATERCVD', 'DATECOL'], 'integer'],
            [['LATCOOR', 'LONGCOOR'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainIp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->CIPNUMBER !== null && $this->CIPNUMBER !== '') {
            $this->_selections = preg_split('/[\s,]+/', $this->CIPNUMBER);
            $query->andFilterWhere(['IN', 'CIPNUMBER', $this->_selections]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'DATERCVD' => $this->DATERCVD,
            'DATECOL' => $this->DATECOL,
            'LATCOOR' => $this->LATCOOR,
            'LONGCOOR' => $this->LONGCOOR,
        ]);

        $query->andFilterWhere(['like', 'accession_id_key', $this->accession_id_key])
            //->andFilterWhere(['like', 'CIPNUMBER', $this->CIPNUMBER])
            ->andFilterWhere(['like', 'CROP', $this->CROP])
            ->andFilterWhere(['like', 'GENUS', $this->GENUS])
            ->andFilterWhere(['like', 'SPECIES', $this->SPECIES])
            ->andFilterWhere(['like', 'SUBTAXON', $this->SUBTAXON])
            ->andFilterWhere(['like', 'CULTVRNAME', $this->CULTVRNAME])
            ->andFilterWhere(['like', 'OTHER_IDENT', $this->OTHER_IDENT])
            ->andFilterWhere(['like', 'FEMALE', $this->FEMALE])
            ->andFilterWhere(['like', 'MALE', $this->MALE])
            ->andFilterWhere(['like', 'BIOSTAT', $this->BIOSTAT])
            ->andFilterWhere(['like', 'COLNUMBER', $this->COLNUMBER])
            ->andFilterWhere(['like', 'COUNTRY', $this->COUNTRY])
            ->andFilterWhere(['like', 'ADMIN1', $this->ADMIN1])
            ->andFilterWhere(['like', 'ADMIN2', $this->ADMIN2])
            ->andFilterWhere(['like', 'ADMIN3', $this->ADMIN3])
            ->andFilterWhere(['like', 'LOCALITY', $this->LOCALITY])
            ->andFilterWhere(['like', 'CATALOGUE', $this->CATALOGUE])
            ->andFilterWhere(['like', 'ORDERING', $this->ORDERING])
            ->andFilterWhere(['like', 'AVAILABLE', $this->AVAILABLE])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS])
            ->andFilterWhere(['like', 'IDENTITY_RESULT', $this->IDENTITY_RESULT])
            ->andFilterWhere(['like', 'COMMENTS', $this->COMMENTS])
            ->andFilterWhere(['like', 'MLSSTAT', $this->MLSSTAT])
            ->andFilterWhere(['like', 'TI', $this->TI])
            ->andFilterWhere(['like', 'ANEXO1', $this->ANEXO1])
            ->andFilterWhere(['like', 'Health_Status', $this->Health_Status])
            ->andFilterWhere(['like', 'PopulationGroup', $this->PopulationGroup]);

        return $dataProvider;
    }
}
