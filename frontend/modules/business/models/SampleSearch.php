<?php

namespace frontend\modules\business\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\business\models\Sample;

/**
 * SampleSearch represents the model behind the search form of `frontend\modules\business\models\Sample`.
 */
class SampleSearch extends Sample
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'numOrder', 'accessionNumber', 'collectingNumber', 'labNumber', 'plate', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'sampleTypeId', 'sourceId', 'sampleCrossId', 'graftingCount'], 'integer'],
            [['accessionCode', 'accessionName', 'collectingCode', 'labCode', 'female', 'male', 'isBulk', 'observation', 'acquisitionRequest', 'distributionRequest', 'details', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'firstUserCode', 'secondUserCode', 'thirdUserCode'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sample::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->joinWith(
            [
                'sampleType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'source' => function ($query) {
                    $query->from(['B' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'sampleCross' => function ($query) {
                    $query->from(['C' => 'Parameter']);
                }
            ]
        );


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'numOrder' => $this->numOrder,
            'accessionNumber' => $this->accessionNumber,
            'collectingNumber' => $this->collectingNumber,
            'labNumber' => $this->labNumber,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'plate' => $this->plate,
            'requestId' => $this->requestId,
            'cropId' => $this->cropId,
            'workFlowId' => $this->workFlowId,
            'numOrderId' => $this->numOrderId,
            // 'sampleTypeId' => $this->sampleTypeId,
            // 'sourceId' => $this->sourceId,
            // 'sampleCrossId' => $this->sampleCrossId,
            'graftingCount' => $this->graftingCount,
        ]);

        $query->andFilterWhere(['like', 'accessionCode', $this->accessionCode])
            ->andFilterWhere(['like', 'accessionName', $this->accessionName])
            ->andFilterWhere(['like', 'collectingCode', $this->collectingCode])
            ->andFilterWhere(['like', 'labCode', $this->labCode])
            ->andFilterWhere(['like', 'female', $this->female])
            ->andFilterWhere(['like', 'male', $this->male])
            ->andFilterWhere(['like', 'isBulk', $this->isBulk])
            ->andFilterWhere(['like', 'observation', $this->observation])
            ->andFilterWhere(['like', 'acquisitionRequest', $this->acquisitionRequest])
            ->andFilterWhere(['like', 'distributionRequest', $this->distributionRequest])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'firstUserCode', $this->firstUserCode])
            ->andFilterWhere(['like', 'secondUserCode', $this->secondUserCode])
            ->andFilterWhere(['like', 'thirdUserCode', $this->thirdUserCode])
            ->andFilterWhere(['like', 'A.shortName', $this->sampleTypeId])
            ->andFilterWhere(['like', 'B.shortName', $this->sourceId])
            ->andFilterWhere(['like', 'C.shortName', $this->sampleCrossId]);

        return $dataProvider;
    }
}
