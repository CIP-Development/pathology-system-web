<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessDetail */

$this->title = 'Update Request Process Detail: ' . $model->requestId;
$this->params['breadcrumbs'][] = ['label' => 'Request Process Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->requestId, 'url' => ['view', 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="request-process-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
