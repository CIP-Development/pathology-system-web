<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcessDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-process-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'numOrder') ?>

    <?= $form->field($model, 'startDate') ?>

    <?= $form->field($model, 'finishDate') ?>

    <?= $form->field($model, 'checkDate') ?>

    <?= $form->field($model, 'controlArea') ?>

    <?php // echo $form->field($model, 'controllerDirection') ?>

    <?php // echo $form->field($model, 'registeredBy') ?>

    <?php // echo $form->field($model, 'registeredAt') ?>

    <?php // echo $form->field($model, 'updatedBy') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedBy') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'requestId') ?>

    <?php // echo $form->field($model, 'cropId') ?>

    <?php // echo $form->field($model, 'workFlowId') ?>

    <?php // echo $form->field($model, 'numOrderId') ?>

    <?php // echo $form->field($model, 'activityId') ?>

    <?php // echo $form->field($model, 'essayId') ?>

    <?php // echo $form->field($model, 'processStatusDetailId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
