<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\intelligence\models\RequestProcessDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Request Process Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-process-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Request Process Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'numOrder',
            'startDate',
            'finishDate',
            'checkDate',
            'controlArea',
            //'controllerDirection',
            //'registeredBy',
            //'registeredAt',
            //'updatedBy',
            //'updatedAt',
            //'deletedBy',
            //'deletedAt',
            //'status',
            //'requestId',
            //'cropId',
            //'workFlowId',
            //'numOrderId',
            //'activityId',
            //'essayId',
            //'processStatusDetailId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
