<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\ReadingData */

$this->title = 'Update Reading Data: ' . $model->sampleId;
$this->params['breadcrumbs'][] = ['label' => 'Reading Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sampleId, 'url' => ['view', 'sampleId' => $model->sampleId, 'cellPosition' => $model->cellPosition, 'supportId' => $model->supportId, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId, 'agentId' => $model->agentId, 'graftingNumberId' => $model->graftingNumberId, 'symptomId' => $model->symptomId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reading-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
