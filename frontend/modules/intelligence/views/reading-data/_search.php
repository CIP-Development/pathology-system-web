<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\ReadingDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reading-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sampleId') ?>

    <?= $form->field($model, 'observation') ?>

    <?= $form->field($model, 'supportIdUser') ?>

    <?= $form->field($model, 'cellPositionUser') ?>

    <?= $form->field($model, 'cellPosition') ?>

    <?php // echo $form->field($model, 'evidence') ?>

    <?php // echo $form->field($model, 'supportId') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'qResult') ?>

    <?php // echo $form->field($model, 'cResult') ?>

    <?php // echo $form->field($model, 'registeredBy') ?>

    <?php // echo $form->field($model, 'registeredAt') ?>

    <?php // echo $form->field($model, 'updatedBy') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedBy') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'plantSectionId') ?>

    <?php // echo $form->field($model, 'requestId') ?>

    <?php // echo $form->field($model, 'cropId') ?>

    <?php // echo $form->field($model, 'workFlowId') ?>

    <?php // echo $form->field($model, 'numOrderId') ?>

    <?php // echo $form->field($model, 'activityId') ?>

    <?php // echo $form->field($model, 'essayId') ?>

    <?php // echo $form->field($model, 'agentId') ?>

    <?php // echo $form->field($model, 'graftingNumberId') ?>

    <?php // echo $form->field($model, 'symptomId') ?>

    <?php // echo $form->field($model, 'readingDataTypeId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
