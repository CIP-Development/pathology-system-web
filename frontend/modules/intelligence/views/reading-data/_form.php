<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\ReadingData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reading-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sampleId')->textInput() ?>

    <?= $form->field($model, 'observation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supportIdUser')->textInput() ?>

    <?= $form->field($model, 'cellPositionUser')->textInput() ?>

    <?= $form->field($model, 'cellPosition')->textInput() ?>

    <?= $form->field($model, 'evidence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supportId')->textInput() ?>

    <?= $form->field($model, 'result')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qResult')->textInput() ?>

    <?= $form->field($model, 'cResult')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registeredAt')->textInput() ?>

    <?= $form->field($model, 'updatedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'disabled' => 'Disabled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'plantSectionId')->textInput() ?>

    <?= $form->field($model, 'requestId')->textInput() ?>

    <?= $form->field($model, 'cropId')->textInput() ?>

    <?= $form->field($model, 'workFlowId')->textInput() ?>

    <?= $form->field($model, 'numOrderId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'essayId')->textInput() ?>

    <?= $form->field($model, 'agentId')->textInput() ?>

    <?= $form->field($model, 'graftingNumberId')->textInput() ?>

    <?= $form->field($model, 'symptomId')->textInput() ?>

    <?= $form->field($model, 'readingDataTypeId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
