<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\intelligence\models\ReadingDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reading Datas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reading-data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reading Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sampleId',
            'observation',
            'supportIdUser',
            'cellPositionUser',
            'cellPosition',
            //'evidence',
            //'supportId',
            //'result',
            //'qResult',
            //'cResult',
            //'registeredBy',
            //'registeredAt',
            //'updatedBy',
            //'updatedAt',
            //'deletedBy',
            //'deletedAt',
            //'status',
            //'plantSectionId',
            //'requestId',
            //'cropId',
            //'workFlowId',
            //'numOrderId',
            //'activityId',
            //'essayId',
            //'agentId',
            //'graftingNumberId',
            //'symptomId',
            //'readingDataTypeId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
