<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\ReadingData */

$this->title = $model->sampleId;
$this->params['breadcrumbs'][] = ['label' => 'Reading Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reading-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'sampleId' => $model->sampleId, 'cellPosition' => $model->cellPosition, 'supportId' => $model->supportId, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId, 'agentId' => $model->agentId, 'graftingNumberId' => $model->graftingNumberId, 'symptomId' => $model->symptomId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'sampleId' => $model->sampleId, 'cellPosition' => $model->cellPosition, 'supportId' => $model->supportId, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId, 'activityId' => $model->activityId, 'essayId' => $model->essayId, 'agentId' => $model->agentId, 'graftingNumberId' => $model->graftingNumberId, 'symptomId' => $model->symptomId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sampleId',
            'observation',
            'supportIdUser',
            'cellPositionUser',
            'cellPosition',
            'evidence',
            'supportId',
            'result',
            'qResult',
            'cResult',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'plantSectionId',
            'requestId',
            'cropId',
            'workFlowId',
            'numOrderId',
            'activityId',
            'essayId',
            'agentId',
            'graftingNumberId',
            'symptomId',
            'readingDataTypeId',
        ],
    ]) ?>

</div>
