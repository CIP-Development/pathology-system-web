<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcess */

$this->title = 'Create Request Process';
$this->params['breadcrumbs'][] = ['label' => 'Request Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-process-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
