<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\intelligence\models\RequestProcess */

$this->title = $model->requestId;
$this->params['breadcrumbs'][] = ['label' => 'Request Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-process-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'requestId' => $model->requestId, 'numOrderId' => $model->numOrderId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'requestId' => $model->requestId, 'numOrderId' => $model->numOrderId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'requestId',
            'numOrderId',
            'cropId',
            'workFlowId',
            'startDate',
            'finishDate',
            'checkDate',
            'essayQty',
            'agentQty',
            'sampleQty',
            'sampleStatus',
            'essayDetail:ntext',
            'agentDetail:ntext',
            'sampleDetail:ntext',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'subTotalCost',
            'processStatusId',
        ],
    ]) ?>

</div>