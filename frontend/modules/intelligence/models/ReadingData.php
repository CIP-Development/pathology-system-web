<?php

namespace frontend\modules\intelligence\models;

use Yii;
use frontend\modules\business\models\Support;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Agent;
use frontend\modules\business\models\Sample;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Symptom;

/**
 * This is the model class for table "ReadingData".
 *
 * @property int $sampleId
 * @property string $observation
 * @property int $supportIdUser
 * @property int $cellPositionUser
 * @property int $cellPosition
 * @property string $evidence
 * @property int $supportId
 * @property string $result
 * @property double $qResult
 * @property string $cResult
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $plantSectionId
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 * @property int $activityId
 * @property int $essayId
 * @property int $agentId
 * @property int $graftingNumberId
 * @property int $symptomId
 * @property int $readingDataTypeId
 *
 * @property Parameter $readingDataType
 * @property Agent $agent
 * @property Parameter $plantSection
 * @property RequestProcessDetail $request
 * @property Sample $sample
 * @property Support $support
 * @property Symptom $symptom
 */
class ReadingData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ReadingData';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sampleId', 'cellPosition', 'supportId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'agentId', 'graftingNumberId', 'symptomId', 'readingDataTypeId'], 'required'],
            [['sampleId', 'supportIdUser', 'cellPositionUser', 'cellPosition', 'supportId', 'plantSectionId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'agentId', 'graftingNumberId', 'symptomId', 'readingDataTypeId'], 'integer'],
            [['qResult'], 'number'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['observation', 'result', 'cResult'], 'string', 'max' => 245],
            [['evidence'], 'string', 'max' => 500],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['sampleId', 'cellPosition', 'supportId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'agentId', 'graftingNumberId', 'symptomId'], 'unique', 'targetAttribute' => ['sampleId', 'cellPosition', 'supportId', 'requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId', 'agentId', 'graftingNumberId', 'symptomId']],
            [['readingDataTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['readingDataTypeId' => 'id']],
            [['agentId'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::className(), 'targetAttribute' => ['agentId' => 'id']],
            [['plantSectionId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['plantSectionId' => 'id']],
            [['requestId', 'cropId', 'workFlowId', 'numOrderId', 'activityId', 'essayId'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessDetail::className(), 'targetAttribute' => ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId']],
            [['sampleId'], 'exist', 'skipOnError' => true, 'targetClass' => Sample::className(), 'targetAttribute' => ['sampleId' => 'id']],
            [['supportId'], 'exist', 'skipOnError' => true, 'targetClass' => Support::className(), 'targetAttribute' => ['supportId' => 'id']],
            [['symptomId'], 'exist', 'skipOnError' => true, 'targetClass' => Symptom::className(), 'targetAttribute' => ['symptomId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sampleId' => 'Sample ID',
            'observation' => 'Observation',
            'supportIdUser' => 'Support Id User',
            'cellPositionUser' => 'Cell Position User',
            'cellPosition' => 'Cell Position',
            'evidence' => 'Evidence',
            'supportId' => 'Support ID',
            'result' => 'Result',
            'qResult' => 'Q Result',
            'cResult' => 'C Result',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'plantSectionId' => 'Plant Section ID',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'activityId' => 'Activity ID',
            'essayId' => 'Essay ID',
            'agentId' => 'Agent ID',
            'graftingNumberId' => 'Grafting Number ID',
            'symptomId' => 'Symptom ID',
            'readingDataTypeId' => 'Reading Data Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDataType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'readingDataTypeId']);
    }




    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activityId']);
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id' => 'agentId']);
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantSection()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'plantSectionId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(RequestProcessDetail::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId', 'activityId' => 'activityId', 'essayId' => 'essayId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSample()
    {
        return $this->hasOne(Sample::className(), ['id' => 'sampleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::className(), ['id' => 'supportId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSymptom()
    {
        return $this->hasOne(Symptom::className(), ['id' => 'symptomId']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
