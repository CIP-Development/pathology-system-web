<?php

namespace frontend\modules\intelligence\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\business\models\Request;
use frontend\modules\control\models\WorkFlowByCrop;
use frontend\modules\control\models\AgentByEssay;
use frontend\modules\control\models\ActivityByEssay;
use frontend\modules\business\models\Sample;


class RequestProcess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%RequestProcess}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requestId', 'numOrderId', 'cropId', 'workFlowId'], 'required'],
            [['requestId', 'numOrderId', 'cropId', 'workFlowId', 'essayQty', 'agentQty', 'sampleQty', 'processStatusId'], 'integer'],
            [['paymentPercentage'], 'number'],
            [['startDate', 'finishDate', 'checkDate', 'registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status', 'essayDetail', 'agentDetail', 'sampleDetail'], 'string'],
            [['subTotalCost'], 'number'],
            [['sampleStatus', 'paymentStatus', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['requestId', 'numOrderId', 'cropId', 'workFlowId'], 'unique', 'targetAttribute' => ['requestId', 'numOrderId', 'cropId', 'workFlowId']],
            [['processStatusId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['processStatusId' => 'id']],
            [['cropId', 'workFlowId'], 'exist', 'skipOnError' => true, 'targetClass' => WorkFlowByCrop::className(), 'targetAttribute' => ['cropId' => 'cropId', 'workFlowId' => 'workFlowId']],
            // [
            //     [
            //         'requestId'
            //     ], 'exist',
            //     'skipOnError' => true,
            //     'targetClass' => Request::className(),
            //     'targetAttribute' => [
            //         'requestId' => 'id'
            //     ]
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'requestId' => 'Request ID',
            'numOrderId' => 'Num Order ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'checkDate' => 'Check Date',
            'essayQty' => 'Essay Qty',
            'agentQty' => 'Agent Qty',
            'sampleQty' => 'Sample Qty',
            'sampleStatus' => 'Sample Status',
            'paymentStatus' => 'Payment Status',
            'paymentPercentage' => 'Payment percentage',
            'essayDetail' => 'Essay Detail',
            'agentDetail' => 'Agent Detail',
            'sampleDetail' => 'Sample Detail',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'subTotalCost' => 'Sub Total Cost',
            'processStatusId' => 'Process Status ID',
        ];
    }

    public function getAgentByRequestProcessDetails()
    {
        return $this->hasMany(AgentByRequestProcessDetail::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId'])->andWhere(['status' => 'active']);
    }

    public function getAgentsByEssay()
    {
        return $this
            ->hasMany(
                AgentByEssay::className(),
                [
                    'essayId' => 'essayId',
                    'agentId' => 'agentId'
                ]
            )
            ->viaTable(
                '{{%AgentByRequestProcessDetail}}',
                [
                    'requestId' => 'requestId',
                    'cropId' => 'cropId',
                    'workFlowId' => 'workFlowId',
                    'numOrderId' => 'numOrderId',
                    'status' => 'status'
                ]
            )
            ->andWhere(
                [
                    'status' => 'active'
                ]
            );
    }

    public function getProcessStatus()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'processStatusId']);
    }

    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'requestId']);
    }

    public function getCrop()
    {
        return $this->hasOne(WorkFlowByCrop::className(), ['cropId' => 'cropId', 'workFlowId' => 'workFlowId']);
    }

    public function getRequestProcessDetails()
    {
        return $this->hasMany(RequestProcessDetail::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId'])->andWhere(['status' => 'active']);
    }

    public function getActivitiesByEssay()
    {
        return $this
            ->hasMany(
                ActivityByEssay::className(),
                [
                    'activityId' => 'activityId',
                    'essayId' => 'essayId'
                ]
            )
            ->viaTable(
                '{{%RequestProcessDetail}}',
                [
                    'requestId' => 'requestId',
                    'cropId' => 'cropId',
                    'workFlowId' => 'workFlowId',
                    'numOrderId' => 'numOrderId',
                    'status' => 'status'
                ]
            )
            ->andWhere(
                [
                    'status' => 'active'
                ]
            );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSamples()
    {
        return $this
            ->hasMany(
                Sample::className(),
                [
                    'requestId' => 'requestId',
                    'cropId' => 'cropId',
                    'workFlowId' => 'workFlowId',
                    'numOrderId' => 'numOrderId'
                ]
            )
            ->andWhere(
                [
                    'status' => 'active'
                ]
            );
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getWorkFlow()
    {
        return $this->hasOne(WorkFlow::className(), ['id' => 'workFlowId']);
    }
}
