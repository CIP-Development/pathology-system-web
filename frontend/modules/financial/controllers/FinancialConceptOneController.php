<?php

namespace frontend\modules\financial\controllers;

use Yii;
use frontend\modules\business\models\Request;
use frontend\modules\financial\models\FinancialConceptOne;
use frontend\modules\financial\models\FinancialConceptOneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use frontend\modules\financial\models\FinancialAccount;

class FinancialConceptOneController extends Controller
{
    //BEHAVIORS
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $searchModel = new FinancialConceptOneSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSize = 0;
            $dataProviderFinancialReport = new ArrayDataProvider();
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'dataProviderFinancialReport' => $dataProviderFinancialReport,
                'selection' => null,
            ]);
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();
        }
    }

    public function actionView($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            return $this->render('view', [
                'model' => $this->findModel($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId),
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionCreate()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = new FinancialConceptOne();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id, 'resnoLab' => $model->resnoLab, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionUpdate($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = $this->findModel($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id, 'resnoLab' => $model->resnoLab, 'requestId' => $model->requestId, 'cropId' => $model->cropId, 'workFlowId' => $model->workFlowId, 'numOrderId' => $model->numOrderId]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionDelete($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $this->findModel($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId)->delete();

            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    protected function findModel($id, $resnoLab, $requestId, $cropId, $workFlowId, $numOrderId)
    {
        if (($model = FinancialConceptOne::findOne(['id' => $id, 'resnoLab' => $resnoLab, 'requestId' => $requestId, 'cropId' => $cropId, 'workFlowId' => $workFlowId, 'numOrderId' => $numOrderId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRefresh()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            return $this->redirect(['index']);
            // $searchModel = new FinancialConceptOneSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProviderFinancialCharge = new ArrayDataProvider();
            // $dataProviderFinancialRecovery = new ArrayDataProvider();
            // return $this->render('index', [
            //     'searchModel' => $searchModel,
            //     'dataProvider' => $dataProvider,
            //     'dataProviderFinancialCharge' => $dataProviderFinancialCharge,
            //     'dataProviderFinancialRecovery' => $dataProviderFinancialRecovery,
            // ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionNotify()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $searchModel = new FinancialConceptOneSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $dataProviderFinancialCharge = new ArrayDataProvider();
            $dataProviderFinancialRecovery = new ArrayDataProvider();

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderFinancialCharge' => $dataProviderFinancialCharge,
                'dataProviderFinancialRecovery' => $dataProviderFinancialRecovery,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionPreview()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            if (!is_null(Yii::$app->request->post('selection'))) {
                $selection = Yii::$app->request->post('selection');
                $searchModel = new FinancialConceptOneSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $modelListFinancialConceptOne = FinancialConceptOne::find()->where(['in', 'id', Yii::$app->request->post('selection')])->all();
                $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['in', 'id', Yii::$app->request->post('selection')])->asArray()->all();
                $arrayListFinancialReport[] = [
                    'Request' => null,
                    'Period' => null,
                    'Account' => null,
                    'Resno' => null,
                    'Bus' => null,
                    'Task' => null,
                    'Description Final' => null,
                    'Agreement' => null,
                    'Sub total cost USD' => null,
                ];
                foreach ($arrayListFinancialConceptOne as $key => $value) {
                    $arrayListFinancialReport[] =  [
                        //'Request' =>  Request::findOne($value['requestId'])->code,
                        'Period' => $value['period'],
                        'Account' => $value['account'],
                        'Resno' => $value['resno'],
                        'Bus' => $value['bus'],
                        'Task' => $value['task'],
                        'Description Final' => $value['descriptionFinal'],
                        'Agreement' => $value['agreement'],
                        'Sub total cost USD' => $value['totalPercentage'],
                    ];
                }
                foreach ($arrayListFinancialConceptOne as $key => $value) {
                    $arrayListFinancialReport[] =  [
                        //'Request' =>  Request::findOne($value['requestId'])->code,
                        'Period' => $value['period'],
                        'Account' => FinancialAccount::findOne($value['resnoLab'])->accountToRecovery,
                        'Resno' => $value['resnoLab'],
                        'Bus' => FinancialAccount::findOne($value['resnoLab'])->busAgreement,
                        'Task' =>  FinancialAccount::findOne($value['resnoLab'])->taskAgreement,
                        'Description Final' =>  FinancialAccount::findOne($value['resnoLab'])->accountDescription,
                        'Agreement' => FinancialAccount::findOne($value['resnoLab'])->recoveryAgreement,
                        'Sub total cost USD' => -1 * (float) $value['totalPercentage'],
                    ];
                }
                ArrayHelper::remove($arrayListFinancialReport, '0');
                $dataProviderFinancialReport = new ArrayDataProvider(
                    [
                        'allModels' => $arrayListFinancialReport,
                        'pagination' => ['pageSize' => 0,],
                        //'key' => 'id',
                    ]
                );
                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'dataProviderFinancialReport' => $dataProviderFinancialReport,
                    'selection' => $selection
                ]);
            }
            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionSend($implodeSelection = null)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            if (!is_null($implodeSelection)) {
                $selection = explode(",", $implodeSelection);

                $arrayListFinancialConceptOne = FinancialConceptOne::find()->where(['in', 'id', $selection])->asArray()->all();

                $arrayListFinancialReport[] = [
                    'Request' => null,
                    'Period' => null,
                    'Account' => null,
                    'Resno' => null,
                    'Bus' => null,
                    'Task' => null,
                    'Description Final' => null,
                    'Agreement' => null,
                    'Sub total cost USD' => null,
                ];

                foreach ($arrayListFinancialConceptOne as $key => $value) {
                    $arrayListFinancialReport[] =  [
                        'Request' =>  Request::findOne($value['requestId'])->code,
                        'Period' => $value['period'],
                        'Account' => $value['account'],
                        'Resno' => $value['resno'],
                        'Bus' => $value['bus'],
                        'Task' => $value['task'],
                        'Description Final' => $value['descriptionFinal'],
                        'Agreement' => $value['agreement'],
                        //'Sub total cost USD' => $value['totalPercentage'],
                        'Sub total cost USD' => number_format((float) $value['totalPercentage'], 2, '.', ''),
                    ];
                    $modelFinancialConceptOne = FinancialConceptOne::findOne($value['id']);
                    $modelFinancialConceptOne->statusConcept = 'sent';
                    $modelFinancialConceptOne->save();
                }
                foreach ($arrayListFinancialConceptOne as $key => $value) {
                    $arrayListFinancialReport[] =  [
                        'Request' =>  Request::findOne($value['requestId'])->code,
                        'Period' => $value['period'],
                        'Account' => FinancialAccount::findOne($value['resnoLab'])->accountToRecovery,
                        'Resno' => $value['resnoLab'],
                        'Bus' => FinancialAccount::findOne($value['resnoLab'])->busAgreement,
                        'Task' =>  FinancialAccount::findOne($value['resnoLab'])->taskAgreement,
                        'Description Final' =>  FinancialAccount::findOne($value['resnoLab'])->accountDescription,
                        'Agreement' => FinancialAccount::findOne($value['resnoLab'])->recoveryAgreement,
                        //'Sub total cost USD' => -1 * (float) $value['totalPercentage'],
                        'Sub total cost USD' => -1 * number_format((float) $value['totalPercentage'], 2, '.', ''),
                    ];
                }
                ArrayHelper::remove($arrayListFinancialReport, '0');
                $dataProviderFinancialReport = new ArrayDataProvider(
                    [
                        'allModels' => $arrayListFinancialReport,
                        'pagination' => ['pageSize' => 0,],

                    ]
                );
                return $this->renderPartial(
                    'reportFinancialConceptOne',
                    [
                        'model' => $arrayListFinancialReport,
                    ]
                );
            }
            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }
}
