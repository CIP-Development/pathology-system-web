<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialAccount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="financial-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'accountToRecovery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountDescription')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'resnoLab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recoveryAgreement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'busAgreement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taskAgreement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountToCharge')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'disabled' => 'Disabled',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>