<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialAccount */

$this->title = $model->resnoLab;
$this->params['breadcrumbs'][] = ['label' => 'Financial Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="financial-account-view">
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'resnoLab',
            'accountToCharge',
            'accountToRecovery',
            'accountDescription',
            'description',
            'status',
            'recoveryAgreement',
            'busAgreement',
            'taskAgreement',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
        ],
    ]) ?>
</div>