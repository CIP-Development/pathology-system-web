<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\financial\models\FinancialConceptOne */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Financial Concept Ones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="financial-concept-one-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Update',
            [
                'update', 'id' => $model->id,
                'resnoLab' => $model->resnoLab,
                'requestId' => $model->requestId,
                'cropId' => $model->cropId,
                'workFlowId' => $model->workFlowId,
                'numOrderId' => $model->numOrderId,
            ],
            [
                'class' => 'btn btn-primary'
            ]
        ) ?>
        <?= Html::a(
            'Delete',
            [
                'delete', 'id' => $model->id,
                'resnoLab' => $model->resnoLab,
                'requestId' => $model->requestId,
                'cropId' => $model->cropId,
                'workFlowId' => $model->workFlowId,
                'numOrderId' => $model->numOrderId,
            ],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'essayId',
            'statusConcept',
            'statusBus',
            'period',
            'dateTo',
            'account',
            'resno',
            'bus',
            'task',
            'crop',
            'testOfLaboratory',
            'owner',
            'pathogens:ntext',
            'numberOfExperiments',
            'numberOfPathogens',
            'descriptionFinal:ntext',
            'ACE',
            'agreement',
            'samples',
            'costId',
            'fee',
            'total',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
            'status',
            'resnoLab',
            'requestId',
            'cropId',
            'workFlowId',
            'numOrderId',
            'percent',
            'totalPercentage',
        ],
    ]) ?>

</div>