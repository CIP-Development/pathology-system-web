<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="report.xls"');
header('Cache-Control: max-age=0');
?>

<table class="table table-bordered table-striped dataTable">
  <tr>
    <th>Request</th>
    <th>Period</th>
    <th>Account</th>
    <th>Resno</th>
    <th>Bus</th>
    <th>Task</th>
    <th>Description Final</th>
    <th>Agreement</th>
    <th>Sub total cost USD</th>
  </tr>
  <?php foreach ($model as $key => $value) { ?>
  <tr>
    <td><?= $value['Request'] ?></td>
    <td><?= $value['Period'] ?></td>
    <td><?= $value['Account'] ?></td>
    <td><?= $value['Resno'] ?></td>
    <td><?= $value['Bus'] ?></td>
    <td><?= $value['Task'] ?></td>
    <td><?= $value['Description Final'] ?></td>
    <td><?= $value['Agreement'] ?></td>
    <td><?= $value['Sub total cost USD'] ?></td>
  </tr>
  <?php } ?>
</table>