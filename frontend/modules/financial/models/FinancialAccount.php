<?php

namespace frontend\modules\financial\models;

use Yii;

/**
 * This is the model class for table "{{%FinancialAccount}}".
 *
 * @property string $resnoLab
 * @property string $accountToCharge
 * @property string $accountToRecovery
 * @property string $accountDescription
 * @property string $description
 * @property string $recoveryAgreement
 * @property string $busAgreement
 * @property string $taskAgreement
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property FinancialTransaction[] $financialTransactions
 */
class FinancialAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%FinancialAccount}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resnoLab'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['resnoLab'], 'string', 'max' => 6],
            [['accountToCharge', 'accountToRecovery'], 'string', 'max' => 5],
            [['accountDescription', 'description', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['recoveryAgreement', 'busAgreement'], 'string', 'max' => 9],
            [['taskAgreement'], 'string', 'max' => 12],
            [['resnoLab'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'resnoLab' => 'Resno Lab',
            'accountToCharge' => 'Account To Charge',
            'accountToRecovery' => 'Account To Recovery',
            'accountDescription' => 'Account Description',
            'description' => 'Description',
            'recoveryAgreement' => 'Recovery Agreement',
            'busAgreement' => 'Bus Agreement',
            'taskAgreement' => 'Task Agreement',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialTransactions()
    {
        return $this->hasMany(FinancialTransaction::className(), ['resnoLab' => 'resnoLab'])->andWhere(['status' => 'active']);
    }
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
