<?php

namespace frontend\modules\financial\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\financial\models\FinancialAccount;

/**
 * FinancialAccountSearch represents the model behind the search form of `frontend\modules\financial\models\FinancialAccount`.
 */
class FinancialAccountSearch extends FinancialAccount
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resnoLab', 'accountToCharge', 'accountToRecovery', 'accountDescription', 'description', 'recoveryAgreement', 'busAgreement', 'taskAgreement', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancialAccount::find()->where(['status' => 'active']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'resnoLab', $this->resnoLab])
            ->andFilterWhere(['like', 'accountToCharge', $this->accountToCharge])
            ->andFilterWhere(['like', 'accountToRecovery', $this->accountToRecovery])
            ->andFilterWhere(['like', 'accountDescription', $this->accountDescription])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'recoveryAgreement', $this->recoveryAgreement])
            ->andFilterWhere(['like', 'busAgreement', $this->busAgreement])
            ->andFilterWhere(['like', 'taskAgreement', $this->taskAgreement])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
