<?php

namespace frontend\modules\financial\models;

use Yii;
use frontend\modules\configuration\models\Cost;
use frontend\modules\configuration\models\Essay;
use frontend\modules\business\models\Request;

/**
 * This is the model class for table "FinancialConceptOne".
 *
 * @property int $essayId
 * @property string $statusConcept
 * @property string $statusBus
 * @property string $period
 * @property string $account
 * @property string $resno
 * @property string $bus
 * @property string $task
 * @property string $crop
 * @property string $testOfLaboratory
 * @property string $owner
 * @property string $pathogens
 * @property int $numberOfExperiments
 * @property int $numberOfPathogens
 * @property string $descriptionFinal
 * @property string $ACE
 * @property string $agreement
 * @property int $samples
 * @property int $costId
 * @property double $fee
 * @property double $total
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property string $resnoLab
 * @property int $requestId
 * @property int $cropId
 * @property int $workFlowId
 * @property int $numOrderId
 * @property double $percent
 * @property double $totalPercentage
 * @property string $dateTo
 * @property string $tab
 * @property string $projectDescription
 * @property string $busDescription
 * @property string $busStatus
 * @property string $resourceId
 * @property string $taskDescription
 * @property string $taskStatus
 * @property string $email
 *
 * @property Cost $cost
 * @property FinancialTransaction $resnoLab0
 * @property Essay $essay
 */
class FinancialConceptOne extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FinancialConceptOne';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['essayId', 'task', 'costId', 'resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'required'],
            [['essayId', 'numberOfExperiments', 'numberOfPathogens', 'samples', 'costId', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'integer'],
            [['statusConcept', 'statusBus', 'pathogens', 'descriptionFinal', 'status'], 'string'],
            [['fee', 'total', 'percent', 'totalPercentage'], 'number'],
            [['registeredAt', 'updatedAt', 'deletedAt', 'dateTo'], 'safe'],
            [['period', 'resno', 'resnoLab', 'resourceId'], 'string', 'max' => 6],
            [['account'], 'string', 'max' => 5],
            [['bus', 'agreement'], 'string', 'max' => 9],
            [['task'], 'string', 'max' => 12],
            [['crop', 'testOfLaboratory', 'owner', 'ACE', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['tab', 'busStatus', 'taskStatus'], 'string', 'max' => 1],
            [['projectDescription', 'busDescription', 'taskDescription', 'email'], 'string', 'max' => 255],
            [['essayId', 'task', 'costId', 'resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId'], 'unique', 'targetAttribute' => ['essayId', 'task', 'costId', 'resnoLab', 'requestId', 'cropId', 'workFlowId', 'numOrderId']],
            [['costId'], 'exist', 'skipOnError' => true, 'targetClass' => Cost::className(), 'targetAttribute' => ['costId' => 'id']],

            // [
            //     [
            //         'resnoLab',
            //         'requestId',
            //         'cropId',
            //         'workFlowId',
            //         'numOrderId'
            //     ],
            //     'exist',
            //     'skipOnError' => true,
            //     'targetClass' => FinancialTransaction::className(),
            //     'targetAttribute' => [
            //         'resnoLab' => 'resnoLab',
            //         'requestId' => 'requestId',
            //         'cropId' => 'cropId',
            //         'workFlowId' => 'workFlowId',
            //         'numOrderId' => 'numOrderId'
            //     ]
            // ],

            [['essayId'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essayId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'essayId' => 'Essay ID',
            'statusConcept' => 'Status Concept',
            'statusBus' => 'Status Bus',
            'period' => 'Period',
            'account' => 'Account',
            'resno' => 'Resno',
            'bus' => 'Bus',
            'task' => 'Task',
            'crop' => 'Crop',
            'testOfLaboratory' => 'Test Of Laboratory',
            'owner' => 'Owner',
            'pathogens' => 'Pathogens',
            'numberOfExperiments' => 'Number Of Experiments',
            'numberOfPathogens' => 'Number Of Pathogens',
            'descriptionFinal' => 'Description Final',
            'ACE' => 'Ace',
            'agreement' => 'Agreement',
            'samples' => 'Samples',
            'costId' => 'Cost ID',
            'fee' => 'Fee',
            'total' => 'Total',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'resnoLab' => 'Resno Lab',
            'requestId' => 'Request ID',
            'cropId' => 'Crop ID',
            'workFlowId' => 'Work Flow ID',
            'numOrderId' => 'Num Order ID',
            'percent' => 'Percent',
            'totalPercentage' => 'Total Percentage',
            'dateTo' => 'Date To',

            'tab' => 'Tab',
            'projectDescription' => 'Project Description',
            'busDescription' => 'Bus Description',
            'busStatus' => 'Bus Status',
            'resourceId' => 'Resource ID',
            'taskDescription' => 'Task Description',
            'taskStatus' => 'Task Status',
            'email' => 'Email',
        ];
    }

    public function getCost()
    {
        return $this->hasOne(Cost::className(), ['id' => 'costId']);
    }

    public function getResnoLab0()
    {
        return $this->hasOne(FinancialTransaction::className(), ['resnoLab' => 'resnoLab', 'requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }

    public function getFinancialTransaction()
    {
        return $this->hasOne(FinancialTransaction::className(), ['resnoLab' => 'resnoLab', 'requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId', 'numOrderId' => 'numOrderId']);
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'requestId']);
    }
}
