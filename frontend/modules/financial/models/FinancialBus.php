<?php

namespace frontend\modules\financial\models;

use Yii;

/**
 * This is the model class for table "FinancialBus".
 *
 * @property int $id
 * @property string $tab
 * @property string $projectId
 * @property string $project
 * @property string $busId
 * @property string $bus
 * @property string $busStatus
 * @property string $dateTo
 * @property string $resourceId
 * @property string $description
 * @property string $taskId
 * @property string $task
 * @property string $taskStatus
 * @property string $email
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property FinancialConceptOne[] $financialConceptOnes
 */
class FinancialBus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FinancialBus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dateTo', 'registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['tab', 'busStatus', 'taskStatus'], 'string', 'max' => 1],
            [['projectId', 'busId'], 'string', 'max' => 9],
            [['project', 'bus', 'task', 'email'], 'string', 'max' => 255],
            [['resourceId'], 'string', 'max' => 6],
            [['description', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['taskId'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tab' => 'Tab',
            'projectId' => 'Project ID',
            'project' => 'Project',
            'busId' => 'Bus ID',
            'bus' => 'Bus',
            'busStatus' => 'Bus Status',
            'dateTo' => 'Date To',
            'resourceId' => 'Resource ID',
            'description' => 'Description',
            'taskId' => 'Task ID',
            'task' => 'Task',
            'taskStatus' => 'Task Status',
            'email' => 'Email',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialConceptOnes()
    {
        return $this->hasMany(FinancialConceptOne::className(), ['financialBusId' => 'id'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
