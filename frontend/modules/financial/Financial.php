<?php

namespace frontend\modules\financial;

/**
 * financial module definition class
 */
class Financial extends \yii\base\Module
{
  /**
   * {@inheritdoc}
   */
  public $controllerNamespace = 'frontend\modules\financial\controllers';

  /**
   * {@inheritdoc}
   */
  public function init()
  {
    parent::init();

    // custom initialization code goes here
  }
}
