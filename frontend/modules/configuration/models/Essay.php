<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Location;
use frontend\modules\control\models\ActivityByEssay;


class Essay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Essay';
    }

    /** 
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shortName', 'longName', 'costId', 'status', 'essayTypeId'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status',], 'string'],
            [['costId', 'essayTypeId'], 'integer'],
            [['shortName', 'longName', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['costId'], 'exist', 'skipOnError' => true, 'targetClass' => Cost::className(), 'targetAttribute' => ['costId' => 'id']],
            [['essayTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['essayTypeId' => 'id']],
            [['locationId'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['locationId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'costId' => 'Cost Code',
            'essayTypeId' => 'Essay Type',
            'locationId' => 'Essay Location',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentByEssays()
    {
        return $this->hasMany(AgentByEssay::className(), ['essayId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgents()
    {
        return $this->hasMany(Agent::className(), ['id' => 'agentId'])->viaTable('AgentByEssay', ['essayId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCost()
    {
        return $this->hasOne(Cost::className(), ['id' => 'costId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'locationId']);
    }

    public function getLocations()
    {
        return ArrayHelper::map(
            Location::find()->where(
                [
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active',
                ]
            )->all(),
            'id',
            'shortName'
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssayType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'essayTypeId']);
    }

    public function getEssayTypes()
    {
        return ArrayHelper::map(
            Parameter::find()->where(
                [
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active',
                    'entity' => 'ESSAY',
                    'singularity' => 'TYPE',

                ]
            )->orWhere(['id' => 1])->all(),
            'id',
            'shortName'
        );
    }

    public function getEssayByWorkFlows()
    {
        return $this->hasMany(EssayByWorkFlow::className(), ['essayId' => 'id'])->andWhere(['status' => 'active']);
    }

    public function getWorkFlows()
    {
        return $this->hasMany(WorkFlow::className(), ['id' => 'workFlowId'])->viaTable('EssayByWorkFlow', ['essayId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    public function getActivityByEssays()
    {
        return $this->hasMany(ActivityByEssay::className(), ['essayId' => 'id'])->andWhere(['status' => 'active']);
    }

    public function getActivities()
    {

        // return $this
        //     ->hasMany(
        //         Activity::className(),
        //         [
        //             'id' => 'activityId',
        //             'status' => 'active'
        //         ]
        //     )
        //     ->viaTable(
        //         'ActivityByEssay',
        //         [
        //             'essayId' => 'id',
        //             'status' => 'status'
        //         ],
        //         function ($query) {
        //             $query->orderBy(['activityOrder' => SORT_ASC]);
        //         }
        //     )
        //     ->andWhere(
        //         [
        //             'status' => 'active'
        //         ]
        //     );

        $query = Activity::find();
        $query->innerJoin('ActivityByEssay', 'Activity.id = ActivityByEssay.activityId ');
        $query->innerJoin('Essay', 'ActivityByEssay.essayId = Essay.id');
        $query->andWhere(['ActivityByEssay.essayId' => $this->id, 'ActivityByEssay.deletedBy' => null, 'ActivityByEssay.deletedAt' => null,  'ActivityByEssay.status' => 'active']);
        $query->andWhere(['Activity.deletedBy' => null, 'Activity.deletedAt' => null, 'Activity.status' => 'active']);
        $query->orderBy(['ActivityByEssay.activityOrder' => SORT_ASC]);
        return $query->all();
    }

    public function getCosts()
    {
        $array1 = [];
        $array1 = Cost::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active'
            ]
        )->all();

        $array2 = [];

        $array2[0] = ArrayHelper::map(
            $array1,
            'id',
            'code'
        );

        $array2[1] = ArrayHelper::map(
            $array1,
            'id',
            'cost'
        );

        $array2[2] = ArrayHelper::map(
            $array1,
            'id',
            'parameter.shortName'
        );

        foreach ($array2[0] as $key => $value) {
            $array2[0][$key] = $value . " (" . (string) $array2[1][$key] . " " . (string) $array2[2][$key] . ")";
        }

        return $array2[0];
    }

    public function getCodeId()
    {
        return ArrayHelper::map(
            Cost::find()->where(
                [
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active'
                ]
            )->all(),
            'id',
            'code'
        );
    }

    public function getCurrencyId()
    {
        return ArrayHelper::map(
            Parameter::find()->where(
                [
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active',
                    'singularity' => 'CURRENCY',
                    'entity' => 'COST',

                ]
            )->all(),
            'id',
            'shortName'
        );
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
