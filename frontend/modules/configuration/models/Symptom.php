<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Symptom".
 *
 * @property int $id
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property GreenProcess[] $greenProcesses
 */
class Symptom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Symptom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shortName', 'longName', 'status'], 'required'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['shortName', 'longName', 'description', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['shortName'], 'unique'],
            [['longName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
