<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Reagent;

/**
 * ReagentSearch represents the model behind the search form of `frontend\modules\configuration\models\Reagent`.
 */
class ReagentSearch extends Reagent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'quantity', 'respId'], 'integer'],
            [['shortName', 'longName', 'description', 'invCode', 'acqDate', 'expDate', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reagent::find();

        // add conditions that should always apply here
        $query->andWhere(['Reagent.deletedBy' => null, 'Reagent.deletedAt' => null, 'Reagent.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Reagent.id' => $this->id,
            'Reagent.quantity' => $this->quantity,
            'Reagent.acqDate' => $this->acqDate,
            'Reagent.expDate' => $this->expDate,
            'Reagent.respId' => $this->respId,
            'Reagent.registeredAt' => $this->registeredAt,
            'Reagent.updatedAt' => $this->updatedAt,
            'Reagent.deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'Reagent.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Reagent.longName', $this->longName])
            ->andFilterWhere(['like', 'Reagent.description', $this->description])
            ->andFilterWhere(['like', 'Reagent.invCode', $this->invCode])
            ->andFilterWhere(['like', 'Reagent.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Reagent.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Reagent.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Reagent.status', $this->status]);

        return $dataProvider;
    }
}
