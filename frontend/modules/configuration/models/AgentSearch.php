<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Agent;

/**
 * AgentSearch represents the model behind the search form of `frontend\modules\configuration\models\Agent`.
 */
class AgentSearch extends Agent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['shortName', 'longName', 'description', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'agentTypeId', 'agentGroupId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agent::find();

        // add conditions that should always apply here
        $query->andWhere(['Agent.deletedBy' => null, 'Agent.deletedAt' => null, 'Agent.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith(
            [
                'agentType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'agentGroup' => function ($query) {
                    $query->from(['B' => 'Parameter']);
                }
            ]
        );

        $query->andFilterWhere([
            'id' => $this->id,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            //'agentTypeId' => $this->agentTypeId,
            //'agentGroupId' => $this->agentGroupId,
        ]);

        $query->andFilterWhere(['like', 'Agent.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Agent.longName', $this->longName])
            ->andFilterWhere(['like', 'Agent.description', $this->description])
            ->andFilterWhere(['like', 'Agent.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Agent.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Agent.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Agent.status', $this->status])
            ->andFilterWhere(['like', 'A.shortName', $this->agentTypeId])
            ->andFilterWhere(['like', 'B.shortName', $this->agentGroupId]);

        return $dataProvider;
    }
}
