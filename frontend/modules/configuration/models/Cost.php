<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%Cost}}".
 *
 * @property int $id
 * @property string $code
 * @property string $description
 * @property double $cost
 * @property int $period
 * @property string $status
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property int $currencyTypeId
 *
 * @property Essay[] $essays
 * @property FinancialTransaction[] $financialTransactions
 */
class Cost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%Cost}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'cost', 'period', 'status', 'currencyTypeId'], 'required'],
            [['cost'], 'number'],
            [['period', 'currencyTypeId'], 'integer'],
            [['status'], 'string'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['code', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'description' => 'Description',
            'cost' => 'Cost',
            'period' => 'Period',
            'status' => 'Status',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'currencyTypeId' => 'Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['costId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancialTransactions()
    {
        return $this->hasMany(FinancialTransaction::className(), ['costId' => 'id'])->andWhere(['status' => 'active']);
    }

    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'currencyTypeId']);
    }

    public function getCurrencyTypeId()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
                'singularity' => "CURRENCY",
                'entity' => "COST"
            ]
        )->all(), 'id', 'shortName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
