<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Activity;

/**
 * ActivitySearch represents the model behind the search form of `frontend\modules\configuration\models\Activity`.
 */
class ActivitySearch extends Activity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['shortName', 'longName', 'description', 'details', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find();

        // add conditions that should always apply here
        $query->andWhere(['Activity.deletedBy' => null, 'Activity.deletedAt' => null, 'Activity.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'shortName' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(
            [
                'activityType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'activityDataSource' => function ($query) {
                    $query->from(['B' => 'Parameter']);
                }
            ]
        );

        $query->joinWith(
            [
                'activityProperty' => function ($query) {
                    $query->from(['C' => 'Parameter']);
                }
            ]
        );


        // grid filtering conditions
        $query->andFilterWhere([
            'Activity.id' => $this->id,
            'Activity.registeredAt' => $this->registeredAt,
            'Activity.updatedAt' => $this->updatedAt,
            'Activity.deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'Activity.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Activity.longName', $this->longName])
            ->andFilterWhere(['like', 'Activity.description', $this->description])
            ->andFilterWhere(['like', 'Activity.details', $this->details])
            ->andFilterWhere(['like', 'Activity.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Activity.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Activity.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'A.shortName', $this->activityTypeId])
            ->andFilterWhere(['like', 'B.shortName', $this->activityDataSourceId])
            ->andFilterWhere(['like', 'C.shortName', $this->activityPropertyId])
            ->andFilterWhere(['like', 'Activity.status', $this->status]);


        return $dataProvider;
    }
}
