<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\WorkFlow;

/**
 * WorkFlowSearch represents the model behind the search form of `frontend\modules\configuration\models\WorkFlow`.
 */
class WorkFlowSearch extends WorkFlow
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'repetition'], 'integer'],
            [['shortName', 'longName', 'description', 'registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkFlow::find();

        // add conditions that should always apply here
        $query->andWhere(['WorkFlow.deletedBy' => null, 'WorkFlow.deletedAt' => null, 'WorkFlow.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'repetition' => $this->repetition,
        ]);

        $query->andFilterWhere(['like', 'shortName', $this->shortName])
            ->andFilterWhere(['like', 'longName', $this->longName])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
