<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Agent".
 *
 * @property int $id
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $agentTypeId
 * @property int $agentGroupId
 *
 * @property Parameter $agentType
 * @property Parameter $agentGroup
 * @property AgentByEssay[] $agentByEssays
 * @property Essay[] $essays
 */
class Agent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['shortName', 'longName', 'agentTypeId', 'agentGroupId', 'status'], 'required'],
            [['agentTypeId', 'agentGroupId'], 'integer'],
            [['shortName', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['longName'], 'string', 'max' => 145],
            [['description'], 'string', 'max' => 245],
            [['agentTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['agentTypeId' => 'id']],
            [['agentGroupId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['agentGroupId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'agentTypeId' => 'Agent Type',
            'agentGroupId' => 'Agent Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentGroup()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'agentGroupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentType()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'agentTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentByEssays()
    {
        return $this->hasMany(AgentByEssay::className(), ['agentId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['id' => 'essayId'])->viaTable('AgentByEssay', ['agentId' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }


    public function getGroupAgentTypes()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'singularity' => "GROUP",
                'entity' => "AGENT",
                'status' => 'active',
            ]
        )->all(), 'id', 'shortName');
    }

    public function getAgentTypes()
    {
        if (isset($this->agentGroup->shortName)) {
            $singularity = "TYPE";
            $entity = "AGENT-" . $this->agentGroup->shortName;
        } else {
            $singularity = "Undetermined";
            $entity = "Undetermined";
        }

        return ArrayHelper::map(Parameter::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'singularity' =>  $singularity,
                'entity' => $entity,
                'status' => 'active',
            ]
        )->all(), 'id', 'shortName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
