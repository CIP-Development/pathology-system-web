<?php

namespace frontend\modules\configuration\models;

use Yii;

/**
 * This is the model class for table "{{%Composition}}".
 *
 * @property int $id
 * @property int $notificationId
 * @property string $header
 * @property string $body
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property Notification $notification
 */
class Composition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%Composition}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notificationId'], 'required'],
            [['notificationId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['header', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['body'], 'string', 'max' => 245],
            [['notificationId'], 'exist', 'skipOnError' => true, 'targetClass' => Notification::className(), 'targetAttribute' => ['notificationId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notificationId' => 'Notification ID',
            'header' => 'Header',
            'body' => 'Body',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notificationId']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
