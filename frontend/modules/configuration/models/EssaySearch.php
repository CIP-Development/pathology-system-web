<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Essay;

/**
 * EssaySearch represents the model behind the search form of `frontend\modules\configuration\models\Essay`.
 */
class EssaySearch extends Essay
{

    public $currency;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                ], 'integer'
            ],
            [
                [
                    'shortName',
                    'longName',
                    'description',
                    'registeredBy',
                    'registeredAt',
                    'updatedBy',
                    'updatedAt',
                    'deletedBy',
                    'deletedAt',
                    'status',
                    'currency',
                    'costId',
                    'essayTypeId',
                    'locationId',
                ], 'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Essay::find();

        // add conditions that should always apply here
        $query->andWhere(['Essay.deletedBy' => null, 'Essay.deletedAt' => null, 'Essay.status' => 'active']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['shortName' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith(
            'cost'
        );

        $query->joinWith(
            'location'
        );

        $query->leftJoin(
            'Parameter',
            '`Parameter`.`id` = `Cost`.`currencyTypeId`'
        );

        //grid filtering conditions extra
        $query->joinWith(
            [
                'essayType' => function ($query) {
                    $query->from(['A' => 'Parameter']);
                }
            ]
        );

        $query->andFilterWhere([
            'Essay.id' => $this->id,
            'Essay.registeredAt' => $this->registeredAt,
            'Essay.updatedAt' => $this->updatedAt,
            'Essay.deletedAt' => $this->deletedAt,
            'Essay.costId' => $this->costId,
            'Essay.locationId' => $this->locationId,
            'Parameter.id' => $this->currency,
        ]);

        $query->andFilterWhere(['like', 'Essay.shortName', $this->shortName])
            ->andFilterWhere(['like', 'Essay.longName', $this->longName])
            ->andFilterWhere(['like', 'Essay.description', $this->description])
            ->andFilterWhere(['like', 'Essay.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'Essay.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'Essay.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'Essay.status', $this->status])
            ->andFilterWhere(['like', 'A.shortName', $this->essayTypeId]);

        return $dataProvider;
    }
}
