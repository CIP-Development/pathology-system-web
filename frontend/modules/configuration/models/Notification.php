<?php

namespace frontend\modules\configuration\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%Notification}}".
 *
 * @property int $id
 * @property string $subject
 * @property string $from
 * @property string $to
 * @property string $code
 * @property string $description
 * @property string $details
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 * @property int $fromUserId
 * @property int $toUserId
 *
 * @property Composition[] $compositions
 * @property User $fromUser
 * @property User $toUser
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%Notification}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['fromUserId', 'toUserId'], 'integer'],
            [['toUserId'], 'required'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['subject', 'to', 'description', 'details', 'from', 'code'], 'string', 'max' => 245],
            [['fromUserId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fromUserId' => 'id']],
            [['toUserId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['toUserId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'from' => 'From',
            'to' => 'To',
            'code' => 'Code',
            'description' => 'Description',
            'details' => 'Details',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'fromUserId' => 'From User ID',
            'toUserId' => 'To User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompositions()
    {
        return $this->hasMany(Composition::className(), ['notificationId' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fromUserId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'toUserId']);
    }
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
