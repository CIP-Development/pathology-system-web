<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\User;

/**
 * This is the model class for table "Reagent".
 *
 * @property int $id
 * @property string $shortName
 * @property string $longName
 * @property string $description
 * @property int $quantity
 * @property string $invCode
 * @property string $acqDate
 * @property string $expDate
 * @property int $respId
 * @property string $registeredBy
 * @property string $registeredAt
 * @property string $updatedBy
 * @property string $updatedAt
 * @property string $deletedBy
 * @property string $deletedAt
 * @property string $status
 *
 * @property User $resp
 * @property ReagentByLabProcess[] $reagentByLabProcesses
 * @property LabProcess[] $labProcesses
 */
class Reagent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Reagent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shortName', 'longName', 'respId', 'status'], 'required'],
            [['quantity', 'respId'], 'integer'],
            [['acqDate', 'expDate', 'registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['shortName', 'longName', 'invCode', 'registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['shortName'], 'unique'],
            [['longName'], 'unique'],
            [['respId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['respId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'longName' => 'Long Name',
            'description' => 'Description',
            'quantity' => 'Quantity',
            'invCode' => 'Inv Code',
            'acqDate' => 'Acq Date',
            'expDate' => 'Exp Date',
            'respId' => 'Resp ID',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResp()
    {
        return $this->hasOne(User::className(), ['id' => 'respId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReagentByLabProcesses()
    {
        return $this->hasMany(ReagentByLabProcess::className(), ['Reagent_id' => 'id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabProcesses()
    {
        return $this->hasMany(LabProcess::className(), ['id' => 'LabProcess_id'])->viaTable('ReagentByLabProcess', ['Reagent_id' => 'id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
