<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\CipSample;
use frontend\modules\configuration\models\CipSampleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CipSampleController implements the CRUD actions for CipSample model.
 */
class CipSampleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CipSample models.
     * @return mixed
     */
    public function actionIndex()
    {
        $permission = (string)Yii::$app->controller->action->id . '-' . str_replace("-", "", (string)Yii::$app->controller->action->controller->id);
        if (Yii::$app->user->can($permission)) {
            $searchModel = new CipSampleSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            /***********************************************/
            return $this->goHome();
            /***********************************************/
        }
    }

    /**
     * Displays a single CipSample model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $permission = (string)Yii::$app->controller->action->id . '-' . str_replace("-", "", (string)Yii::$app->controller->action->controller->id);
        if (Yii::$app->user->can($permission)) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            /***********************************************/
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Creates a new CipSample model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $permission = (string)Yii::$app->controller->action->id . '-' . str_replace("-", "", (string)Yii::$app->controller->action->controller->id);
        if (Yii::$app->user->can($permission)) {
            $model = new CipSample();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->sampleId]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Updates an existing CipSample model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $permission = (string)Yii::$app->controller->action->id . '-' . str_replace("-", "", (string)Yii::$app->controller->action->controller->id);
        if (Yii::$app->user->can($permission)) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->sampleId]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Deletes an existing CipSample model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $permission = (string)Yii::$app->controller->action->id . '-' . str_replace("-", "", (string)Yii::$app->controller->action->controller->id);
        if (Yii::$app->user->can($permission)) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            /***********************************************/
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    /**
     * Finds the CipSample model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CipSample the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CipSample::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
