<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Cost */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-view">

    <h1><?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <p>
        <?php
        // echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) 
        ?>
        <?php
        // echo  Html::a('Delete', ['delete', 'id' => $model->id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'id',
            'code',
            'status',
            'cost',
            'parameter.shortName',
            'period',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            // 'deletedBy',
            // 'deletedAt',
        ],
    ]) ?>

</div>