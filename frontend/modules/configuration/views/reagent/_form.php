<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Reagent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reagent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shortName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'invCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acqDate')->textInput() ?>


    <?php
    // echo $form->field($model, 'acqDate')->widget(
    //     DatePicker::className(),
    //     [
    //         'clientOptions' =>
    //         [
    //             'dateFormat' => 'yy-mm-dd'
    //         ]
    //     ]
    // )
    ?>


    <?= $form->field($model, 'expDate')->textInput() ?>

    <?= $form->field($model, 'respId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>