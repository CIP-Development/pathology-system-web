<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\ReagentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reagent-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'shortName') ?>
            <?= $form->field($model, 'longName') ?>
            <?= $form->field($model, 'description') ?>
            <?= $form->field($model, 'status')->dropDownList(
                [
                    'active' => 'Active',
                    'disabled' => 'Disabled',
                ],
                [
                    'prompt' => 'All',
                ]
            ) ?>

        </div>
        <div class="col-md-6">
            <?php echo $form->field($model, 'quantity') ?>
            <?php echo $form->field($model, 'invCode') ?>
            <?php echo $form->field($model, 'respId') ?>
            <?php echo $form->field($model, 'acqDate') ?>

            <?php
            // echo $form->field($model, 'acqDate')->widget(
            //     DatePicker::className(),
            //     [
            //         'clientOptions' =>
            //         [
            //             'dateFormat' => 'yy-mm-dd'
            //         ]
            //     ]
            // )
            ?>


            <?php echo $form->field($model, 'expDate') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <i class="icon fa fa-info-circle"></i>
            You may optionally enter a comparison operator (<, <=,>, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done.
                    <br />
                    <br />
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>