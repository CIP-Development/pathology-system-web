<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\ParameterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parameters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-index">

    <h1><?= Html::encode($this->title) ?>
    </h1>

    <div class="row">
        <div class="col-md-12">

            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">
                        <i class="fa fa-bars "></i>
                        <?= Html::encode($this->title) ?> List
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12">
                        <!-- CREATE MODAL -->
                        <?php
                        Modal::begin([
                            'header' => '<div class="box-header"><i class="fa fa-edit"></i><h3 class="box-title">Create Parameter</h3></div>',
                            'id'     => 'modal',
                            'size'   => 'modal-mm',
                        ]);
                        echo "<div id='modalContent'></div>";
                        Modal::end();
                        ?>
                        <!-- VIEW MODAL -->
                        <?php
                        Modal::begin([
                            'header' => '<div class="box-header"><i class="glyphicon glyphicon-eye-open"></i><h3 class="box-title">View Parameter</h3></div>',
                            'id'     => 'modal-view',
                            'size'   => 'modal-mm',
                        ]);
                        echo "<div class='modal-view-content'></div>";
                        Modal::end();
                        ?>
                        <!-- UPDATE MODAL -->
                        <?php
                        Modal::begin([
                            'header' => '<div class="box-header"><i class="glyphicon glyphicon-pencil"></i><h3 class="box-title">Edit Parameter</h3></div>',
                            'id'     => 'modal-update',
                            'size'   => 'modal-mm',
                        ]);
                        echo "<div class='modal-update-content'></div>";
                        Modal::end();
                        ?>
                    </div>
                    <div class="col-md-12">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout'       => "{items}",
                            'columns' => [
                                [
                                    'class' => 'yii\grid\SerialColumn'
                                ],
                                [
                                    'attribute'     => 'code',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'singularity',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'entity',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'shortName',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'longName',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'description',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'details',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'status',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'registeredBy',
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute'     => 'registeredAt',
                                    'enableSorting' => false,
                                ],

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header'        => 'Action',
                                    'template'      => '{view} {update} ',
                                    'headerOptions' => ['width' => '50'],
                                    'buttons'       => [
                                        'view' => function ($url, $model, $key) {
                                            return Html::a(
                                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                                null,
                                                [
                                                    'class' => 'modal-view-action',
                                                    'title' => Yii::t('yii', 'View'),
                                                    'value' => Yii::$app->urlManager->createUrl(
                                                        [
                                                            'configuration/parameter/view',
                                                            'id' => $key
                                                        ]
                                                    ),
                                                ]
                                            );
                                        },
                                        'update' => function ($url, $model, $key) {
                                            return Html::a(
                                                "<span class='glyphicon glyphicon-pencil'></span>",
                                                null,
                                                [
                                                    'class' => 'modal-update-action',
                                                    'title' => Yii::t('yii', 'Update'),
                                                    'value' => Yii::$app->urlManager->createUrl(
                                                        [
                                                            'configuration/parameter/update',
                                                            'id' => $key
                                                        ]
                                                    ),

                                                ]
                                            );
                                        },
                                    ],
                                ],
                            ],

                            'tableOptions' => [
                                'class' => 'table table-bordered table-striped dataTable',
                                'id'    => "gvParameter",
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="col-md-12">
                        <?= Html::button(
                            '<i class="fa fa-edit "></i> Create Parameter',
                            [
                                'value' => Url::to('index.php?r=configuration/parameter/create'),
                                'class' => 'btn btn-success-cip btn-app',
                                'id' => 'modalButton'
                            ]
                        )
                        ?>
                    </div>
                </div>

            </div>

            <div class="box box-solid bg-aqua-gradient collapsed-box">
                <div class="box-header">
                    <h3 class="box-title">Busquedas Avanzada</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bg-aqua btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        <button type="button" class="btn bg-aqua btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php echo $this->render(
                        '_search',
                        [
                            'model' => $searchModel
                        ]
                    ); ?>
                </div>
            </div>

        </div>
    </div>


</div>