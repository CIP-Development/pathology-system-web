<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\CipSampleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cip Samples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cip-sample-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  ?>

    <p>
        <?= Html::a('Create Cip Sample', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'font-size:12px;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sampleId',
            'CIPNUMBER',
            'CIPNUMBER_key',
            'CROP',
            'WebSMTA_bool',
            //'WebSMTA2',
            //'FAO',
            //'CULTVRNAME',
            //'COLNUMBER',
            //'ALTERID',
            //'OTHER_IDENT',
            //'SINONIMO',
            //'CCODE',
            //'MISSCODE',
            //'COUNTRY',
            //'STATE',
            //'REGION',
            //'REGION2',
            //'LOCALITY',
            //'SOURCE',
            //'SOURCIP',
            //'ALT',
            //'LATCOOR',
            //'LONGCOOR',
            //'BIOSTAT',
            //'SPP',
            //'SPECIES',
            //'GENUS',
            //'PopulationGroup',
            //'FEMALE',
            //'MALE',
            //'DCCODE',
            //'DCTRY',
            //'DATERCVD',
            //'DATECOL',
            //'DONOR',
            //'COMMENTS',
            //'STATUS',
            //'Health_Status',
            //'Distribution_Status',
            //'STORA',
            //'BIOSTAT2',
            //'corporate',
            //'CULTVRNAME2',
            //'FEMALE2',
            //'MALE2',
            //'COLNUMBER2',
            //'Health_Status2',
            //'Corporativa',
            //'CREATE',
            //'CIPFBS',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div> 