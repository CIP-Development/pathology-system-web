<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\CipSample */

$this->title = 'Create Cip Sample';
$this->params['breadcrumbs'][] = ['label' => 'Cip Samples', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cip-sample-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
