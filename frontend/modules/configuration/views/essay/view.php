<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Essay */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="essay-view">
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            'id',
            'shortName',
            'longName',
            'description',
            'status',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'cost.code',
            'cost.cost',
            [
                'label' => 'Currency',
                'value' => $model->cost->parameter->shortName,
            ],
            [
                'label' => 'Essay Type agent',
                'value' => $model->essayType->shortName,
            ],
            [
                'label' => 'Location',
                'value' => $model->location->shortName,
            ],
        ],
    ]) ?>

</div>