<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Essay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="essay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shortName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'costId')->dropDownList(
        $model->getCosts(),
        [
            'id' => 'ddlCostIds',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'essayTypeId')->dropDownList(
        $model->getEssayTypes(),
        [
            'id' => 'ddlEssayTypes',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'locationId')->dropDownList(
        $model->getLocations(),
        [
            'id' => 'ddlLocations',
            'prompt' => 'Select...',
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>