<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use yii\helpers\ArrayHelper;
// use frontend\modules\configuration\models\Parameter;
/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Agent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shortName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'agentGroupId')->dropDownList(
        $model->getGroupAgentTypes(),
        [
            'id' => 'ddlGroupAgentTypes',
            'prompt' => 'Select...',
            'onchange' =>
            '$.post("index.php?r=configuration/agent/drop-down-lists&shortName=' . '"+$("#ddlGroupAgentTypes option:selected").text() ,
                function( data ) {
                    $( "#ddlAgentType" ).html(data);
                }
            );'
        ]
    ) ?>

    <?= $form->field($model, 'agentTypeId')->dropDownList(
        $model->getAgentTypes(),
        [
            'id' => 'ddlAgentType',
            'prompt' => 'Select...',
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>