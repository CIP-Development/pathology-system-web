<?php

namespace frontend\modules\security\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use frontend\modules\security\models\SecurityRole;
use common\models\User;

class ItemRoleController extends Controller
{
  //BEHAVIORS
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'only' => [],
        'rules' => [
          [
            'allow' => false,
            'actions' =>  [],
            'roles' => ['?'],
          ],
          [
            'allow' => true,
            'actions' => [],
            'roles' => ['@'],
          ],
        ],
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
          Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
          return $this->goHome();
        }
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function actionIndex($actionId = null, $roleName = null)
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $postArray = Yii::$app->request->post();
      if (!empty($postArray)) {
        $cbxActionArray =  isset($postArray['cbxActionArray']) ? $postArray['cbxActionArray'] : [];
        $this->setPermissionsByRole($roleName, $cbxActionArray);
      }
      $auth = Yii::$app->authManager;
      $getRolesArray = $auth->getRoles();
      $dataProviders['getRolesArray'] = new ArrayDataProvider([
        'allModels' => $getRolesArray,
        'id' => 'getRolesArray',
        'sort' => ['attributes' => ['numOrder'],],
        'pagination' => ['pageSize' => 1000]
      ]);
      return $this->render('index', [
        'dataProviders' => $dataProviders,
        'actionId' => $actionId,
      ]);
    } else {
      Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->goHome();
    }
  }

  public function actionView($roleName)
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      return $this->renderAjax('view', [
        'model' => $this->findModel($roleName),
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function actionCreate()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $model = new SecurityRole();
      if ($model->load(Yii::$app->request->post()) and  $model->saveSecurity()) {

        return $this->redirect(['index']);
      }
      return $this->renderAjax('create', [
        'model' => $model,
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function actionUpdate($roleName)
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $model = $this->findModel($roleName);
      $postArray = Yii::$app->request->post();
      $objectRoleOld = $this->findModel($roleName);
      if (isset($postArray['objectRoleNew'])) {
        $auth = Yii::$app->authManager;
        $name = $objectRoleOld->name;
        $objectRoleOld->name = $postArray['objectRoleNew']['name'];
        $objectRoleOld->description = $postArray['objectRoleNew']['description'];
        $objectRoleOld->data = $postArray['objectRoleNew']['data'];
        $auth->update($name, $objectRoleOld);
        return $this->redirect(['index']);
      }
      return $this->renderAjax('update', [
        'model' => $model,
      ]);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function actionDelete($roleName)
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $auth = Yii::$app->authManager;
      $role = $auth->getRole($roleName);
      try {
        if ($auth->remove($role)) {
          Yii::$app->session->setFlash('info',  "<h4><i class='icon fa fa-check'></i>Info!</h4> You have successfully delete Role called: " . $roleName . "");
        }
      } catch (\Throwable $th) {

        Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>Warning!</h4> You not have successfully delete Role called: " . $roleName . ". Before deleting a role, you must first remove the users and permissions related to the role.");
      }
      return $this->redirect(['index']);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function findModel($roleName)
  {
    $auth = Yii::$app->authManager;
    $authObject = $auth->getRole($roleName);
    return $authObject;
  }
}
