<?php

namespace frontend\modules\security\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;

use common\models\User;

class ItemPermissionController extends Controller
{
  //BEHAVIORS
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'only' => [],
        'rules' => [
          [
            'allow' => false,
            'actions' =>  [],
            'roles' => ['?'],
          ],
          [
            'allow' => true,
            'actions' => [],
            'roles' => ['@'],
          ],
        ],
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
          Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
          return $this->goHome();
        }
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function actionIndex($actionId = null)
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $auth = Yii::$app->authManager;
      $getPermissionsArray = $auth->getPermissions();
      $dataProviders['getPermissionsArray'] = new ArrayDataProvider([
        'allModels' => $getPermissionsArray,
        'id' => 'getPermissionsArray',
        'sort' => ['attributes' => ['numOrder'],],
        'pagination' => ['pageSize' => 0]
      ]);
      return $this->render('index', [
        'dataProviders' => $dataProviders,
        'actionId' => $actionId,
      ]);
    } else {
      Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->goHome();
    }
  }

  public function actionRefreshPermissions()
  {
    if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id) or array_key_exists('system_admin',  Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id))) {
      $auth = Yii::$app->authManager;
      $getObjectsArray = $this->getAllObjectsArray();

      foreach ($auth->getRoles() as $roleName => $role) {
        $arrayChildRoles[$roleName] =  array_keys($auth->getChildRoles($roleName));
        $arrayPermissionsByRole[$roleName] =  array_keys($auth->getPermissionsByRole($roleName));
        $auth->removeChildren($role); // Remove CHILD_ROLES and PERMISSIONS
      }
      $auth->removeAllPermissions();
      //--------------------------------------------//CREATE ALL PERMISSIONS
      foreach ($getObjectsArray as $key => $value) {
        if (is_null($auth->getPermission($value['permission']))) {
          $createPermission = $auth->createPermission($value['permission']); //CONTROLLER_ACTION
          $createPermission->description = $value['description'];
          $auth->add($createPermission);
        }
      }
      //--------------------------------------------//ROLES DELEGATES RECONSTRUCTION
      foreach ($arrayChildRoles as $roleName => $arrayChildRolesName) {
        if ($roleName != 'system_admin') {
          foreach ($arrayChildRolesName as $key => $childRoleName) {
            if ($roleName != $childRoleName)
              if (!is_null($auth->getRole($roleName)))
                if ($auth->canAddChild($auth->getRole($roleName), $auth->getRole($childRoleName)))
                  $auth->addChild($auth->getRole($roleName), $auth->getRole($childRoleName));
          }
        }
      }
      //--------------------------------------------//PERMISSIONS DELEGATES RECONSTRUCTION
      foreach ($arrayPermissionsByRole as $roleName => $arrayChildPermissionsName) {
        if ($roleName != 'system_admin') {
          foreach ($arrayChildPermissionsName as $key => $childPermissionName) {
            if (!is_null($auth->getPermission($childPermissionName)))
              if ($auth->canAddChild($auth->getRole($roleName), $auth->getPermission($childPermissionName)))
                $auth->addChild($auth->getRole($roleName), $auth->getPermission($childPermissionName));
          }
        }
      }
      //--------------------------------------------//DEFINE SYSTEM_ADMIN
      foreach ($auth->getRoles() as $roleName => $role) {
        if ($roleName != 'system_admin')
          $auth->addChild($auth->getRole('system_admin'), $role);
      }

      foreach ($auth->getPermissions() as $permissionName => $permission) {
        $auth->addChild($auth->getRole('system_admin'), $permission);
      }
      //--------------------------------------------

      return $this->redirect(['index']);
    } else {
      /***********************************************/
      Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
      return $this->redirect(
        [
          'index',
          'id' => null,
          'from' =>  Yii::$app->controller->action->id,
        ]
      );
      /***********************************************/
    }
  }

  public function getAllObjectsArray()
  {
    $allObjectsArray = [];
    $modules = $this->getAllModules();
    foreach ($modules as $key1 => $value1) {
      $controllers = $this->getAllControllers($value1);
      foreach ($controllers as $key2 => $value2) {
        $actions = $this->getAllActions($value1, $value2['controllerName']);
        foreach ($actions as $key3 => $value3) {
          $allObjectsArray[] = [
            'module' => $value1,
            'controller' => strtolower(explode('-controller', $value2['controller'])[0]),
            'action' => strtolower($value3),
            'permission' =>  strtolower(explode('-controller', $value2['controller'])[0]) . '_' . strtolower($value3),
            'description' =>
            "The permission allows executing the action: " .  strtoupper($value3) .
              " within the model/controller: " . strtoupper(explode('-controller', $value2['controller'])[0]) .
              " of the module: " . strtoupper($value1),
          ];
        }
      }
    }
    return $allObjectsArray;
  }

  public function getAllModules()
  {
    $modules = scandir('../modules');
    ArrayHelper::remove($modules, '0');
    ArrayHelper::remove($modules, '1');
    $modules = array_values($modules);
    return $modules;
  }

  public function getAllControllers($module)
  {
    $modules = scandir('../modules/' . $module . '/controllers');
    $controllers = [];
    foreach ($modules as $key => $value) {
      if ($controller = $this->extractController($value)) {


        $controllers[] = [
          'controllerName' => $controller,
          'controller' => substr(strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', $controller)), 1, 255),
        ];
      }
    }
    return $controllers;
  }

  public function getAllActions($module, $controller)
  {
    $class = 'frontend\modules\\' . $module . '\\controllers\\' . $controller;
    $functions = [];
    $actions = [];
    $functions = get_class_methods($class);

    array_walk($functions, function (&$element) {
      return $element = strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', $element));
    });

    $actions = array_filter($functions, function ($element) {
      return (strpos($element, 'action-') !== false
        and strpos($element, 'bind-action-params') === false);
    });

    array_walk($actions, function (&$element) {
      return $element = explode('action-', $element)[1];
    });

    return $actions;
  }

  protected function extractController($name)
  {
    $filename = explode('.php', $name);
    if (count(explode('Controller.php', $name)) > 1) {
      if (count($filename) > 1) {
        return $filename[0];
      }
    }
    return false;
  }
}
