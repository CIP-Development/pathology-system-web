<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;


$this->title = 'Permissions';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='item-role-index'>
  <h1><?= Html::encode($this->title) ?> </h1>

  <!-- MODALS -->
  <div class="box-body">
    <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Create <?php echo Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-create-content"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-view-default" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-mm modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>View default <?php echo Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-view-default-content"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- 

  <div class="box collapsed-box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" data-widget="collapse">
        <i class="fa fa-search"></i> Advanced Search
      </button>
    </div>
    <div class="box-body">
      <?php
      // echo $this->render('_search', ['model' => $searchModel]);
      ?>
    </div>
  </div>
 -->


  <!-- CONTENT -->
  <div class='box'>

    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-bars"></i> Permissions</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>


    <div class='box-body'>

      <div class="row">

        <div class="col-md-12">

          <!-- LIST ROLES -->
          <?php
          echo GridView::widget([
            'dataProvider' => $dataProviders['getPermissionsArray'],
            'options' => ['style' => 'font-size:12px;'],
            'layout'       => "{items}",
            'rowOptions' => function ($model, $key)  use ($actionId) {
              if ($key == $actionId and !is_Null($actionId)) {
                return ['class' => 'success_action'];
              } else {
                return ['class' => 'default'];
              }
            },
            'columns' => [
              [
                'class' => 'yii\grid\SerialColumn',
                'header'        => 'Order',
              ],
              [
                'header' => 'Name',
                'value' => 'name'
              ],
              [
                'header' => 'Description',
                'value' => 'description'
              ],
              [
                'header' => 'Rule Name',
                'value' => 'ruleName'
              ],
              [
                'header' => 'Data',
                'value' => 'data'
              ],
            ],
            'tableOptions' => [
              'class' => 'table table-bordered table-striped',
              'id' => 'gvRoles'
            ],
          ]);
          ?>
        </div>
      </div>
    </div>
    <div class='box-footer'>



      <?= Html::a(
        'Refresh permissions',
        [
          'refresh-permissions',
        ],
        [
          'class' => 'btn btn-primary pull-right',
          'title'        => 'save',
          // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
          'data-method'  => 'post',
        ]
      ); ?>


    </div>
  </div>

</div>