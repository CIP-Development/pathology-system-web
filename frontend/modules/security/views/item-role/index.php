<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;


$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='item-role-index'>
  <h1><?= Html::encode($this->title) ?> </h1>

  <!-- MODALS -->
  <div class="box-body">
    <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Create <?php echo Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-create-content"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>Update action <?php echo Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-update-content"></div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-edit"></i>View action <?php echo Html::encode($this->title) ?></h3>
          </div>
          <div class="modal-body">
            <div class="modal-view-content"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- 

  <div class="box collapsed-box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" data-widget="collapse">
        <i class="fa fa-search"></i> Advanced Search
      </button>
    </div>
    <div class="box-body">
      <?php
      // echo $this->render('_search', ['model' => $searchModel]);
      ?>
    </div>
  </div>
 -->


  <!-- CONTENT -->
  <div class='box'>

    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-bars"></i> Roles</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>


    <div class='box-body'>

      <div class="row">

        <div class="col-md-12">

          <!-- LIST ROLES -->
          <?php
          echo GridView::widget([
            'dataProvider' => $dataProviders['getRolesArray'],
            'options' => ['style' => 'font-size:12px;'],
            'layout'       => "{items}",
            'rowOptions' => function ($model, $key)  use ($actionId) {
              if ($key == $actionId and !is_Null($actionId)) {
                return ['class' => 'success_action'];
              } else {
                return ['class' => 'default'];
              }
            },
            'columns' => [
              [
                'class' => 'yii\grid\SerialColumn',
                'header'        => 'Order',
              ],
              [
                'header' => 'Name',
                'value' => 'name'
              ],
              [
                'header' => 'Description',
                'value' => 'description'
              ],
              // [
              //   'header' => 'Rule Name',
              //   'value' => 'ruleName'
              // ],
              [
                'header' => 'Data',
                'value' => 'data'
              ],
              // [
              //   'header' => 'Assignments permissions',
              //   'format' => 'raw',
              //   'headerOptions' => [
              //     'width' => '50',
              //   ],
              //   'contentOptions' => [
              //     'class' => 'text-center'
              //   ],
              //   'value' => function ($data, $key) {
              //     return
              //       Html::a(
              //         '<span class="glyphicon glyphicon-hand-right"></span>',
              //         [
              //           'item/index',
              //           'roleName' => $key,
              //           '#' => 'permissionsId'
              //         ],
              //         [
              //           'class' => 'btn btn-default btn-xs'
              //         ]
              //       );
              //   },
              // ],
              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Actions role',
                'template'      => '{view} {update} {delete}',
                'headerOptions' => ['width' => '150'],
                'contentOptions' => [
                  'class' => 'text-center'
                ],
                'buttons'       => [




                  'view' => function ($url, $model, $key) {
                    return Html::button(
                      '<span class="glyphicon glyphicon-eye-open"></span>',
                      // [
                      //   'update',
                      //   'roleName' => $key,
                      // ],
                      [
                        'class' => 'btn btn-default btn-xs modal-view-action',
                        'title'        => Yii::t('yii', 'View'),
                        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        // 'data-method'  => 'post',

                        'value' => Url::to(
                          [
                            'item-role/view',
                            'roleName' => $key
                          ]
                        ),

                      ]
                    );
                  },




                  'update' => function ($url, $model, $key) {
                    return Html::button(
                      '<span class="glyphicon glyphicon-pencil"></span>',
                      // [
                      //   'update',
                      //   'roleName' => $key,
                      // ],
                      [
                        'class' => 'btn btn-default btn-xs modal-update-action',
                        'title'        => Yii::t('yii', 'View'),
                        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        // 'data-method'  => 'post',

                        'value' => Url::to(
                          [
                            'item-role/update',
                            'roleName' => $key
                          ]
                        ),

                      ]
                    );
                  },


                  // 'update' => function ($url, $model, $key) {
                  //   return Html::a(
                  //     '<span class="glyphicon glyphicon-pencil"></span>',
                  //     [
                  //       'update',
                  //       'roleName' => $key,
                  //     ],
                  //     [
                  //       'class' => 'btn btn-default btn-xs  modal-view-action',
                  //       'title'        => Yii::t('yii', 'View'),
                  //       // 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                  //       // 'data-method'  => 'post',
                  //     ]
                  //   );
                  // },




                  'delete' => function ($url, $model, $key) {
                    return Html::a(
                      '<span class="glyphicon glyphicon-trash"></span>',
                      [
                        'delete',
                        'roleName' => $key,
                      ],
                      [
                        'class' => 'btn btn-default btn-xs',
                        'title'        => 'delete',
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method'  => 'post',
                      ]
                    );
                  },
                ],
              ],
            ],
            'tableOptions' => [
              'class' => 'table table-bordered table-striped',
              'id' => 'gvRoles'
            ],
          ]);
          ?>
        </div>
      </div>
    </div>
    <div class='box-footer'>
      <?php
      echo Html::button(
        'Create Role',
        [
          'value' => Url::to(['item-role/create']),
          'class' => 'btn btn-success modal-create-action',
        ]
      )
      ?>
    </div>
  </div>

</div>