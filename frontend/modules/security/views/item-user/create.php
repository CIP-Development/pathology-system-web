<?php

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-user-create">
  <h1>
    <?php
    // echo Html::encode($this->title) 
    ?>
  </h1>

  <?= $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>