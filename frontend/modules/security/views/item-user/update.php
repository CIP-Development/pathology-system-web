<?php

use yii\helpers\Html;

$this->title = 'Update Crop: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-user-update">

  <h1>
    <?php
    // echo Html::encode($this->title) 
    ?>
  </h1>

  <?= $this->render('_form', [
    'model' => $model,
  ]) ?>

</div>