<?php

namespace frontend\modules\security\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;


class UserSearch extends User
{
  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [
        [
          'username',
          'firstName',
          'lastName',
          'email',
          'role',


          'created_at',
          'created_by',

          'updated_at',
          'updated_by',

          'deleted_at',
          'deleted_by',

          'password',

          'photo',
          'details',

        ], 'safe'
      ],
    ];
  }


  public function scenarios()
  {
    return Model::scenarios();
  }


  public function search($params)
  {
    $query = User::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['username' => SORT_ASC]]
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,

      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
      'deleted_at' => $this->deleted_at,

    ]);

    $query->andFilterWhere(['like', 'username', $this->username])
      ->andFilterWhere(['like', 'firstName', $this->firstName])
      ->andFilterWhere(['like', 'lastName', $this->lastName])
      ->andFilterWhere(['like', 'email', $this->email])
      ->andFilterWhere(['like', 'role', $this->role])

      ->andFilterWhere(['like', 'created_by', $this->created_by])
      ->andFilterWhere(['like', 'updated_by', $this->updated_by])
      ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])

      ->andFilterWhere(['like', 'password', $this->password])
      ->andFilterWhere(['like', 'photo', $this->photo])
      ->andFilterWhere(['like', 'details', $this->details]);

    return $dataProvider;
  }
}
