<?php

namespace frontend\modules\security\models;

use Yii;
use yii\base\Model;

class SecurityRole extends Model
{
  public $name;
  public $data;
  public $description;



  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['name'], 'unique'],
      [['name'], 'string', 'max' => 64],
      [['data', 'description'], 'string', 'max' => 245],
    ];
  }

  public function attributeLabels()
  {
    return [
      'name' => 'Role Name',
      'data' => 'Data',
      'description' => 'Description',
    ];
  }

  public function saveSecurity()
  {

    $auth = Yii::$app->authManager;



    try {
      $objectRoleNew = $auth->createRole($this->name);
      $objectRoleNew->data = $this->data;
      $objectRoleNew->description = $this->description;
      $auth->add($objectRoleNew);
    } catch (\Throwable $th) {
      Yii::$app->session->setFlash(
        'danger',
        "<h4><i class='icon fa fa-check'></i>Error!</h4>" . $th->errorInfo[2]
      );
    }
    Yii::$app->session->setFlash(
      'success',
      "<h4><i class='icon fa fa-check'></i>New Role!</h4>A new role was successfully registered: <h5><b>" . (string) $this->name . "</b></h5>"
    );

    return true;
  }
}
