<?php

namespace frontend\modules\control\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\control\models\ActivityByEssay;

/**
 * ActivityByEssaySearch represents the model behind the search form of `frontend\modules\control\models\ActivityByEssay`.
 */
class ActivityByEssaySearch extends ActivityByEssay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityId', 'essayId', 'activityOrder'], 'integer'],
            [['registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivityByEssay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'essayId' => SORT_ASC,
                    'activityOrder' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('essay');
        $query->joinWith('activity');

        // grid filtering conditions
        $query->andFilterWhere([
            'ActivityByEssay.registeredAt' => $this->registeredAt,
            'ActivityByEssay.updatedAt' => $this->updatedAt,
            'ActivityByEssay.deletedAt' => $this->deletedAt,
            'ActivityByEssay.activityOrder' => $this->activityOrder,
        ]);

        $query->andFilterWhere(['like', 'ActivityByEssay.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'ActivityByEssay.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'ActivityByEssay.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'ActivityByEssay.status', $this->status])
            ->andFilterWhere(['like', 'Activity.shortName', $this->activityId])
            ->andFilterWhere(['like', 'Essay.shortName', $this->essayId]);

        return $dataProvider;
    }
}
