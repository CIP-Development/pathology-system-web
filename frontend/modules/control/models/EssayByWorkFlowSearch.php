<?php

namespace frontend\modules\control\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\control\models\EssayByWorkFlow;

/**
 * EssayByWorkFlowSearch represents the model behind the search form of `frontend\modules\control\models\EssayByWorkFlow`.
 */
class EssayByWorkFlowSearch extends EssayByWorkFlow
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['workFlowId', 'essayId'], 'integer'],
            [['registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'workFlowId', 'essayId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EssayByWorkFlow::find();

        // add conditions that should always apply here
        $query->andWhere(['EssayByWorkFlow.deletedBy' => null, 'EssayByWorkFlow.deletedAt' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith('essay');
        $query->joinWith('workFlow');

        $query->andFilterWhere([
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'EssayByWorkFlow.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'EssayByWorkFlow.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'EssayByWorkFlow.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'EssayByWorkFlow.status', $this->status])

            ->andFilterWhere(['like', 'WorkFlow.shortName', $this->workFlowId])
            ->andFilterWhere(['like', 'Essay.shortName', $this->essayId]);

        return $dataProvider;
    }
}
