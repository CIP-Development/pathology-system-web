<?php

namespace frontend\modules\control\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\control\models\WorkFlowByCrop;

/**
 * WorkFlowByCropSearch represents the model behind the search form of `frontend\modules\control\models\WorkFlowByCrop`.
 */
class WorkFlowByCropSearch extends WorkFlowByCrop
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['cropId', 'workFlowId'], 'integer'],
            [['registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'cropId', 'workFlowId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkFlowByCrop::find();

        // add conditions that should always apply here
        $query->andWhere(['WorkFlowByCrop.deletedBy' => null, 'WorkFlowByCrop.deletedAt' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith('crop');
        $query->joinWith('workFlow');

        $query->andFilterWhere([
            // 'cropId' => $this->cropId,
            // 'workFlowId' => $this->workFlowId,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'WorkFlowByCrop.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'WorkFlowByCrop.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'WorkFlowByCrop.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'WorkFlowByCrop.status', $this->status])

            ->andFilterWhere(['like', 'Crop.shortName', $this->cropId])
            ->andFilterWhere(['like', 'WorkFlow.shortName', $this->workFlowId]);
        return $dataProvider;
    }
}
