<?php

namespace frontend\modules\control\models;

use Yii;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Essay;
use yii\helpers\ArrayHelper;

class AgentByEssay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'AgentByEssay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['essayId', 'agentId'], 'required'],
            [['essayId', 'agentId'], 'integer'],
            [['registeredAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['status'], 'string'],
            [['registeredBy', 'updatedBy', 'deletedBy'], 'string', 'max' => 45],
            [['essayId', 'agentId'], 'unique', 'targetAttribute' => ['essayId', 'agentId']],
            [['agentId'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::className(), 'targetAttribute' => ['agentId' => 'id']],
            [['essayId'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essayId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'essayId' => 'Essay',
            'agentId' => 'Agent',
            'registeredBy' => 'Registered By',
            'registeredAt' => 'Registered At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
            'deletedBy' => 'Deleted By',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id' => 'agentId']);
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essayId']);
    }

    public function getRequestProcessDetails()
    {
        return $this->hasMany(RequestProcessDetail::className(), ['essayId' => 'essayId', 'agentId' => 'agentId'])->andWhere(['status' => 'active'])->andWhere(['status' => 'active']);
    }

    public function getRequests()
    {
        return $this->hasMany(RequestProcess::className(), ['requestId' => 'requestId', 'cropId' => 'cropId', 'workFlowId' => 'workFlowId'])->viaTable('RequestProcessDetail', ['essayId' => 'essayId', 'agentId' => 'agentId', 'status' => 'status'])->andWhere(['status' => 'active'])->andWhere(['status' => 'active']);
    }

    public function getEssayArray()
    {
        return ArrayHelper::map(Essay::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->orderBy(['shortName' => SORT_ASC])->all(), 'id', 'shortName');
    }

    public function getAgentArray()
    {
        return ArrayHelper::map(Agent::find()->where(
            [
                'deletedBy' => null,
                'deletedAt' => null,
                'status' => 'active',
            ]
        )->orderBy(['shortName' => SORT_ASC])->all(), 'id', 'longName');
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registeredBy = \Yii::$app->user->identity->username;
            $this->registeredAt = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updatedBy = \Yii::$app->user->identity->username;
            $this->updatedAt = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}
