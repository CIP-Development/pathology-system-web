<?php

namespace frontend\modules\control\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\control\models\AgentByEssay;

/**
 * AgentByEssaySearch represents the model behind the search form of `frontend\modules\control\models\AgentByEssay`.
 */
class AgentByEssaySearch extends AgentByEssay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['essayId', 'agentId'], 'integer'],
            [['registeredBy', 'registeredAt', 'updatedBy', 'updatedAt', 'deletedBy', 'deletedAt', 'status', 'essayId', 'agentId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgentByEssay::find();

        // add conditions that should always apply here
        $query->andWhere(['AgentByEssay.deletedBy' => null, 'AgentByEssay.deletedAt' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinWith('agent');
        $query->joinWith('essay');

        $query->andFilterWhere([
            //'essayId' => $this->essayId,
            //'agentId' => $this->agentId,
            'registeredAt' => $this->registeredAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'AgentByEssay.registeredBy', $this->registeredBy])
            ->andFilterWhere(['like', 'AgentByEssay.updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'AgentByEssay.deletedBy', $this->deletedBy])
            ->andFilterWhere(['like', 'AgentByEssay.status', $this->status])

            ->andFilterWhere(['like', 'Agent.shortName', $this->agentId])
            ->andFilterWhere(['like', 'Essay.shortName', $this->essayId]);


        return $dataProvider;
    }
}
