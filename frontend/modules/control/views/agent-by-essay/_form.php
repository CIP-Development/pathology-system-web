<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="agent-by-essay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'essayId')->dropDownList(
        $model->getEssayArray(),
        [
            'id' => 'ddlEssay',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'agentId')->dropDownList(
        $model->getAgentArray(),
        [
            'id' => 'ddlAgent',
            'prompt' => 'Select...',
        ]
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            'active' => 'Active',
            'disabled' => 'Disabled',
        ],
        [
            'prompt' => 'Select...',
            'options' => [
                'active'  => [
                    'selected' => true
                ]
            ]
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>