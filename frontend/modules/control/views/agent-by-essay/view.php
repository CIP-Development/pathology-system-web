<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\AgentByEssay */

$this->title = $model->essayId;
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agent-by-essay-view">

    <h1><?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <p>
        <?php
        // echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) 
        ?>
        <?php
        // echo  Html::a('Delete', ['delete', 'id' => $model->id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            [
                'attribute' => 'essayId',
                'value' => $model->essay->shortName,
            ],
            [
                'attribute' => 'agentId',
                'value' => $model->agent->shortName,
            ],
            'status',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
        ],
    ]) ?>

</div>