<?php

use yii\helpers\Html;

$this->title = 'Create Agent By Essay';
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>