<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\ActivityByEssay */

$this->title = $model->activityId;
$this->params['breadcrumbs'][] = ['label' => 'Activity By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="activity-by-essay-view">

    <h1><?php
        // echo Html::encode($this->title) 
        ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-condensed'],
        'attributes' => [
            [
                'attribute' => 'essayId',
                'value' => $model->essay->shortName,
            ],
            [
                'attribute' => 'activityId',
                'value' => $model->activity->shortName,
            ],
            'status',
            'activityOrder',
            // 'type',
            // 'activityByType.shortName',
            // 'dataSource',
            // 'dataType',
            'registeredBy',
            'registeredAt',
            'updatedBy',
            'updatedAt',
            'deletedBy',
            'deletedAt',
        ],
    ]) ?>

</div>