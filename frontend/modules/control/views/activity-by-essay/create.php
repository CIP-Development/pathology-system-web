<?php

use yii\helpers\Html;

$this->title = 'Create Activity By Essay';
$this->params['breadcrumbs'][] = ['label' => 'Activity By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-by-essay-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>