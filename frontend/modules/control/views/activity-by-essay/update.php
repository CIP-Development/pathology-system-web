<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\control\models\ActivityByEssay */

$this->title = 'Update Activity By Essay: ' . $model->activityId;
$this->params['breadcrumbs'][] = ['label' => 'Activity By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->activityId, 'url' => ['view', 'activityId' => $model->activityId, 'essayId' => $model->essayId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activity-by-essay-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>