<?php

namespace frontend\modules\control\controllers;

use Yii;
use frontend\modules\control\models\EssayByWorkFlow;
use frontend\modules\control\models\EssayByWorkFlowSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\configuration\models\WorkFlow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Agent;
use yii\helpers\ArrayHelper;

class EssayByWorkFlowController extends Controller
{
    //BEHAVIORS
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' =>  [],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
                    Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
                    return $this->goHome();
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $searchModel = new EssayByWorkFlowSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination = false;
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            Yii::$app->session->setFlash('danger',  "<h4><i class='icon fa fa-check'></i>No access!</h4>You are not allowed to access this page");
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->goHome();
        }
    }

    public function actionView($workFlowId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($workFlowId, $essayId),
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionCreate()
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = new EssayByWorkFlow();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'workFlowId' => $model->workFlowId, 'essayId' => $model->essayId]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionUpdate($workFlowId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $model = $this->findModel($workFlowId, $essayId);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //return $this->redirect(['view', 'workFlowId' => $model->workFlowId, 'essayId' => $model->essayId]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    public function actionDelete($workFlowId, $essayId)
    {
        if (Yii::$app->user->can((string) $this->action->controller->id . '_' . (string) $this->action->id)) {
            $this->findModel($workFlowId, $essayId)->delete();

            return $this->redirect(['index']);
        } else {
            /***********************************************/
            Yii::$app->request->setHostInfo("https://research.cip.cgiar.org");
            return $this->redirect(
                [
                    'index',
                    'id' => null,
                    'from' =>  Yii::$app->controller->action->id,
                ]
            );
            /***********************************************/
        }
    }

    protected function findModel($workFlowId, $essayId)
    {
        if (($model = EssayByWorkFlow::findOne(['workFlowId' => $workFlowId, 'essayId' => $essayId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCheckLists($id, $arrayAgents  = null)
    {
        $countWorkFlows = WorkFlow::find()
            ->where(
                [
                    'id' => $id,
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active'
                ]
            )
            ->count();
        $workFlow = WorkFlow::find()
            ->where(
                [
                    'id' => $id,
                    'deletedBy' => null,
                    'deletedAt' => null,
                    'status' => 'active'
                ]
            )
            ->one();
        $result = "";
        if ($countWorkFlows > 0) {
            $result = "<fieldset class='groupbox-border'>
            <legend class='groupbox-border'><h4><i class='icon fa fa-cubes'></i> " . $workFlow->shortName . " </h4></legend>";
            $essays = ArrayHelper::map($workFlow->essays, 'id', 'shortName');

            foreach ($essays as $key0 => $value0) {

                $countEssays = Essay::find()
                    ->where(
                        [
                            'id' => $key0,
                            'deletedBy' => null,
                            'deletedAt' => null,
                        ]
                    )
                    ->count();


                $essay = Essay::find()
                    ->where(
                        [
                            'id' => $key0,
                            'deletedBy' => null,
                            'deletedAt' => null,
                        ]
                    )
                    ->one();





                if ($workFlow->id == 14 and $essay->essayTypeId == 95) {
                    $result = $result . "
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='position: absolute; visibility: hidden;'>
                        <hr style='margin-bottom: 0px; margin-top: 0px;'>
                        <label class='cbxlStyle'>
                            <span  style='font-size: 12px;' ><i class='icon fa fa-cube'></i> " . $value0 .  "</span> 
                        </label>";
                } else {
                    $result = $result . "
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                        <hr style='margin-bottom: 0px; margin-top: 0px;'>
                        <label class='cbxlStyle'>
                            <span  style='font-size: 12px;' ><i class='icon fa fa-cube'></i> " . $value0 .  "</span> 
                        </label>";
                }













                if ($countEssays > 0) {

                    $agents = ArrayHelper::map($essay->agents, 'id', 'shortName');
                    $agentsLongName = ArrayHelper::map($essay->agents, 'id', 'longName');
                    foreach ($agents as $key1 => $value1) {
                        $result = $result . "<div class='col-xs-6 col-sm-6 col-md-4 col-lg-3'>";
                        $result = $result . "  <label  class='cbxStyle' title='" .  $agentsLongName[$key1] .  "'>";
                        $extra = " ";

                        // first exception
                        if (!is_null($arrayAgents)) {
                            $arrayAgentsExp = [];
                            $arrayAgentsExp = explode(",", $arrayAgents);
                            foreach ($arrayAgentsExp as $key2 => $value2) {
                                if (($key0 == explode('.', $value2, 2)[0] and $key1 == explode('.', $value2, 2)[1])) {
                                    $extra = " checked='true' ";
                                }
                            }
                        }
                        // second exception
                        if ($essay->essayTypeId == 95) {
                            $extra = " checked='true' ";
                        }

                        $result = $result . "      <input type='checkbox' name='selectEssayAgentName[" . $key0 . "][" . $key1 . "]' " . $extra . " value='" . $value1 . "'>";
                        $result = $result . "          <span style='font-size: 10px;' class = 'badge bg-teal-gradient' style='white-space: normal;' >" . $value1 . "</span>";
                        $result = $result . "  </label>";
                        $result = $result . "</div>";
                    }
                } else {
                    $result = $result . "";
                }
                $result = $result . "</div>";
                // $result = $result . "<br /><br />";
            }
            $result = $result . "</fieldset>";
        }
        // $result = $result . "<br /><br />";
        return $result;
    }
}
