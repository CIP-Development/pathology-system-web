<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadFile extends Model
// class UploadForm extends Model
{
  public $documentFile;
  // public $imageFile;

  public function rules()
  {
    return [
      [['documentFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
    ];
  }

  public function upload($stampText)
  {
    $result = [];

    if ($this->validate()) {

      $uploadName = $this->documentFile->baseName . $stampText . '.' . $this->documentFile->extension;

      try {
        $this->documentFile->saveAs('documents/absorbances/' .  $uploadName);
      } catch (\Throwable $th) { }

      $result = [
        'name' => $uploadName,
        'path' => 'documents/absorbances/',
      ];

      return $result;
    } else {
      return false;
    }
  }
}
