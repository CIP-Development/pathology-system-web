<?php

namespace frontend\models;

use yii\base\Model;
use common\models\User;
use yii\web\UploadedFile;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public $firstName;
    public $lastName;
    public $photo;
    public $details;

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['firstName', 'lastName', 'firstName', 'lastName'], 'safe'],
            [['firstName', 'lastName', 'firstName', 'lastName'], 'string'],
            [['firstName', 'lastName'], 'string', 'max' => 45],
            [['photo', 'details'], 'string', 'max' => 255],

            //[['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            //[['requestStatusId'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['requestStatusId' => 'id']],
        ];
    }



    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'firstName' => 'Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'status' => 'Status',
            'role' => 'User Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'details' => 'Details',
            'imageFile' => 'Photo',
        ];
    }

    public function signup($id = null)
    {
        if (is_null($id)) {
            $user = new User();
            $user->photo = 'dist/img/avatar0.jpg';
        } else {
            $user = User::findOne($id);
        }
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        isset($this->firstName) ?  $user->firstName =  $this->firstName : null;
        isset($this->lastName) ?  $user->lastName =  $this->lastName : null;
        isset($this->photo) ?  $user->photo =  $this->photo : null;
        isset($this->details) ?  $user->details =  $this->details : null;
        try {
            if ($user->save()) {
                $result = $user;
                $userId = $user->id;
                $auth = Yii::$app->authManager;
                $getRole = $auth->getRole("client");
                $auth->assign($getRole, $userId);
            }
        } catch (\Throwable $th) {
            $result = false;
        }
        return $result;
    }


    public function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLoad($id)
    {
        if (($model = User::findOne($id)) !== null) {

            $this->username = $model->username;
            $this->email = $model->email;
            $this->firstName = $model->firstName;
            $this->lastName = $model->lastName;
            $this->photo = $model->photo;
            $this->details = $model->details;
            $this->password  = $model->password;

            return true;
        }
    }
}
