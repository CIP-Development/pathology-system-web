<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class SecurityRole extends Model
{
  public $name;
  public $description;
  public $data;


  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['name'], 'string', 'max' => 64],
      [['data', 'description'], 'string', 'max' => 245],
      [['name'], 'unique'],
    ];
  }


  protected function loadSecurity($postArray)
  {
    $auth = Yii::$app->authManager;


    $objectRoleNew = $auth->createRole($postArray['objectRoleNew']['name']);
    $objectRoleNew->description = $postArray['objectRoleNew']['description'];
    $objectRoleNew->data = $postArray['objectRoleNew']['data'];


    $auth->add($objectRoleNew);
  }


  protected function saveSecurity()
  {
    $auth = Yii::$app->authManager;


    $objectRoleNew = $auth->createRole('new_name');
    // $objectRoleNew->description = $postArray['objectRoleNew']['description'];
    // $objectRoleNew->data = $postArray['objectRoleNew']['data'];


    $auth->add($objectRoleNew);
  }
}
