<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Security extends Model
{
  public $name;
  public $description;
  public $data;


  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['data', 'description'], 'string', 'max' => 245],
      [['name'], 'unique'],
    ];
  }
}
