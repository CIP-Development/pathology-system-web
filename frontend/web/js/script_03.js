// DROP DOWN LIST GROUP
$("#ddl01 > li").click(function (e) {
	$("#ddl01 > li").removeClass("active");
});
$("#ddl02 > li").click(function (e) {
	$("#ddl02 > li").removeClass("active");
});

// ALERT FADE IN SECONDS
// $(".alert")
// 	.animate({ opacity: 1.0 }, 3000)
// 	.fadeOut("slow");

// CHECK ALL
$(document).ready(function () {
	$("input:checkbox").click(function (event) {
		var val01 = $(this).val();
		var class01 = ".class_" + val01;
		if (this.checked) {
			$(class01).each(function () {
				this.checked = true;
			});
		} else {
			$(class01).each(function () {
				this.checked = false;
			});
		}
	});
});

// RADIO
$(document).ready(function () {
	$("input[name=RadioAvatar]").click(function (e) {
		document.getElementById("userImageId").src = $(e.currentTarget).val();
	});
});

// LOADING

$(document).on("pjax:send", function () {
	$("#loader").show();
});

$(document).on("pjax:complete", function () {
	$("#loader").hide();
});

//TABS

$('#myTab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});

$("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
	var id = $(e.target).attr("href").substr(1);
	window.location.hash = id;
});

var hash = window.location.hash;
$('#myTab a[href="' + hash + '"]').tab('show');