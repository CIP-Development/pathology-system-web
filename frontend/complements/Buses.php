<?php

/*
Copyright (C) 2021 International Potato Centre (CIP)

This file is part of CIP Pathlogy System, developed by CIP IT staff from RIU unit and external software provider contracted by CIP.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

This Software and the work of the developers shall be expressly acknowledged in any modified or derivative product based on the Software.

This notice shall be included in all copies or substantial portions of the Software.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

International Potato Centre
Apartado 1558, Lima 12, Peru
cip@cgiar.org - www.cipotato.org
*/

try {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require_once 'WS//MELOCSWSAutoload.php';
  $_credentials = new MELOCSWSStructWSCredentials();
  $_credentials->setClient('p2');
  $_credentials->setUsername('WS-Genebank');
  $_credentials->setPassword('G3n3b4nk');
  $_templateResultOptions = new MELOCSWSStructTemplateResultOptions(true, true, false, true, true, false, -1, 1, "");
  $_templateResultInput = new MELOCSWSStructInputForTemplateResult(2309, $_templateResultOptions, null, "");
  $_MELOCSWSServiceGet = new MELOCSWSServiceGet();
  try {
    if ($_MELOCSWSServiceGet->GetTemplateResultAsXML(new MELOCSWSStructGetTemplateResultAsXML($_templateResultInput, $_credentials))) {
      $_xmlResult = $_MELOCSWSServiceGet->getResult()->getGetTemplateResultAsXMLResult()->getGetTemplateResultAsXMLResult()->getTemplateResult();
      $_objectXML = simplexml_load_string($_xmlResult);
      $array = [];
      foreach ($_objectXML as $key => $value) {
        $array[] =
          [
            'tab' => (string) $value->tab,
            'project_id' => (string) $value->project_id,
            'project' => (string) $value->project,
            'bus' => (string) $value->bus,
            'bus_id' => (string) $value->bus_id,
            'bus_status' => (string) $value->bus_status,
            'date_to' => (date("Y-m-d H:i:s", strtotime($value->date_to))),
            'resource_id' => (string) $value->resource_id,
            'description' => (string) $value->description,
            'task_id' => (string) $value->task_id,
            'task' => (string) $value->task,
            'task_satus' => (string) $value->task_satus,
            'e_mail' => (string) $value->e_mail,
          ];
      }
      $paymentInfoResult =  $array;
    }
  } catch (\Throwable $th1) {
    $paymentInfoResult =  null;
  }
} catch (\Throwable $th2) {
  $paymentInfoResult =  null;
}
return
  [
    'paymentInfo' => $paymentInfoResult,
  ];
