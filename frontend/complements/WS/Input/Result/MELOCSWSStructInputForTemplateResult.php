<?php
/**
 * File for class MELOCSWSStructInputForTemplateResult
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructInputForTemplateResult originally named InputForTemplateResult
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructInputForTemplateResult extends MELOCSWSWsdlClass
{
    /**
     * The TemplateId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $TemplateId;
    /**
     * The TemplateResultOptions
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateResultOptions
     */
    public $TemplateResultOptions;
    /**
     * The SearchCriteriaPropertiesList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public $SearchCriteriaPropertiesList;
    /**
     * The PipelineAssociatedName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $PipelineAssociatedName;
    /**
     * Constructor method for InputForTemplateResult
     * @see parent::__construct()
     * @param long $_templateId
     * @param MELOCSWSStructTemplateResultOptions $_templateResultOptions
     * @param MELOCSWSStructArrayOfSearchCriteriaProperties $_searchCriteriaPropertiesList
     * @param string $_pipelineAssociatedName
     * @return MELOCSWSStructInputForTemplateResult
     */
    public function __construct($_templateId,$_templateResultOptions = NULL,$_searchCriteriaPropertiesList = NULL,$_pipelineAssociatedName = NULL)
    {
        parent::__construct(array('TemplateId'=>$_templateId,'TemplateResultOptions'=>$_templateResultOptions,'SearchCriteriaPropertiesList'=>$_searchCriteriaPropertiesList,'PipelineAssociatedName'=>$_pipelineAssociatedName),false);
    }
    /**
     * Get TemplateId value
     * @return long
     */
    public function getTemplateId()
    {
        return $this->TemplateId;
    }
    /**
     * Set TemplateId value
     * @param long $_templateId the TemplateId
     * @return long
     */
    public function setTemplateId($_templateId)
    {
        return ($this->TemplateId = $_templateId);
    }
    /**
     * Get TemplateResultOptions value
     * @return MELOCSWSStructTemplateResultOptions|null
     */
    public function getTemplateResultOptions()
    {
        return $this->TemplateResultOptions;
    }
    /**
     * Set TemplateResultOptions value
     * @param MELOCSWSStructTemplateResultOptions $_templateResultOptions the TemplateResultOptions
     * @return MELOCSWSStructTemplateResultOptions
     */
    public function setTemplateResultOptions($_templateResultOptions)
    {
        return ($this->TemplateResultOptions = $_templateResultOptions);
    }
    /**
     * Get SearchCriteriaPropertiesList value
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties|null
     */
    public function getSearchCriteriaPropertiesList()
    {
        return $this->SearchCriteriaPropertiesList;
    }
    /**
     * Set SearchCriteriaPropertiesList value
     * @param MELOCSWSStructArrayOfSearchCriteriaProperties $_searchCriteriaPropertiesList the SearchCriteriaPropertiesList
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public function setSearchCriteriaPropertiesList($_searchCriteriaPropertiesList)
    {
        return ($this->SearchCriteriaPropertiesList = $_searchCriteriaPropertiesList);
    }
    /**
     * Get PipelineAssociatedName value
     * @return string|null
     */
    public function getPipelineAssociatedName()
    {
        return $this->PipelineAssociatedName;
    }
    /**
     * Set PipelineAssociatedName value
     * @param string $_pipelineAssociatedName the PipelineAssociatedName
     * @return string
     */
    public function setPipelineAssociatedName($_pipelineAssociatedName)
    {
        return ($this->PipelineAssociatedName = $_pipelineAssociatedName);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructInputForTemplateResult
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
