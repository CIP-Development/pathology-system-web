<?php
/**
 * File for class MELOCSWSStructWSCredentials
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructWSCredentials originally named WSCredentials
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructWSCredentials extends MELOCSWSWsdlClass
{
    /**
     * The Username
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Username;
    /**
     * The Client
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Client;
    /**
     * The Password
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Password;
    /**
     * Constructor method for WSCredentials
     * @see parent::__construct()
     * @param string $_username
     * @param string $_client
     * @param string $_password
     * @return MELOCSWSStructWSCredentials
     */
    public function __construct($_username = NULL,$_client = NULL,$_password = NULL)
    {
        parent::__construct(array('Username'=>$_username,'Client'=>$_client,'Password'=>$_password),false);
    }
    /**
     * Get Username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->Username;
    }
    /**
     * Set Username value
     * @param string $_username the Username
     * @return string
     */
    public function setUsername($_username)
    {
        return ($this->Username = $_username);
    }
    /**
     * Get Client value
     * @return string|null
     */
    public function getClient()
    {
        return $this->Client;
    }
    /**
     * Set Client value
     * @param string $_client the Client
     * @return string
     */
    public function setClient($_client)
    {
        return ($this->Client = $_client);
    }
    /**
     * Get Password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->Password;
    }
    /**
     * Set Password value
     * @param string $_password the Password
     * @return string
     */
    public function setPassword($_password)
    {
        return ($this->Password = $_password);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructWSCredentials
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
