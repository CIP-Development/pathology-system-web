<?php
/**
 * File for class MELOCSWSStructFormatProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructFormatProperties originally named FormatProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructFormatProperties extends MELOCSWSWsdlClass
{
    /**
     * The Break
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $Break;
    /**
     * The ConditionInUse
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $ConditionInUse;
    /**
     * The ConditionOnlyBreaks
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $ConditionOnlyBreaks;
    /**
     * The FontBold
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $FontBold;
    /**
     * The FontItalic
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $FontItalic;
    /**
     * The FontStrikeOut
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $FontStrikeOut;
    /**
     * The FontUnderline
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $FontUnderline;
    /**
     * The Show
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $Show;
    /**
     * The ShowText
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $ShowText;
    /**
     * The ConditionLevel
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $ConditionLevel;
    /**
     * The DataCase
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataCase;
    /**
     * The DataLength
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataLength;
    /**
     * The DataType
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DataType;
    /**
     * The DisplayLength
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DisplayLength;
    /**
     * The DisplayOrder
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $DisplayOrder;
    /**
     * The SortOrder
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $SortOrder;
    /**
     * The TextDisplayLength
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $TextDisplayLength;
    /**
     * The SequenceNo
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $SequenceNo;
    /**
     * The ConditionFrom
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var double
     */
    public $ConditionFrom;
    /**
     * The ConditionTo
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var double
     */
    public $ConditionTo;
    /**
     * The AmountDisplayFormat
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $AmountDisplayFormat;
    /**
     * The AttributeId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $AttributeId;
    /**
     * The BreakColName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $BreakColName;
    /**
     * The BreakText
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $BreakText;
    /**
     * The Color
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Color;
    /**
     * The ColumnHeaderText
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ColumnHeaderText;
    /**
     * The ColumnName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ColumnName;
    /**
     * The ConditionType
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $ConditionType;
    /**
     * The FontFamily
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $FontFamily;
    /**
     * The Formula
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Formula;
    /**
     * The SectionType
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $SectionType;
    /**
     * The SectionFormatID
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $SectionFormatID;
    /**
     * The Type
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Type;
    /**
     * Constructor method for FormatProperties
     * @see parent::__construct()
     * @param boolean $_break
     * @param boolean $_conditionInUse
     * @param boolean $_conditionOnlyBreaks
     * @param boolean $_fontBold
     * @param boolean $_fontItalic
     * @param boolean $_fontStrikeOut
     * @param boolean $_fontUnderline
     * @param boolean $_show
     * @param boolean $_showText
     * @param int $_conditionLevel
     * @param int $_dataCase
     * @param int $_dataLength
     * @param int $_dataType
     * @param int $_displayLength
     * @param int $_displayOrder
     * @param int $_sortOrder
     * @param int $_textDisplayLength
     * @param int $_sequenceNo
     * @param double $_conditionFrom
     * @param double $_conditionTo
     * @param string $_amountDisplayFormat
     * @param string $_attributeId
     * @param string $_breakColName
     * @param string $_breakText
     * @param string $_color
     * @param string $_columnHeaderText
     * @param string $_columnName
     * @param string $_conditionType
     * @param string $_fontFamily
     * @param string $_formula
     * @param string $_sectionType
     * @param string $_sectionFormatID
     * @param string $_type
     * @return MELOCSWSStructFormatProperties
     */
    public function __construct($_break,$_conditionInUse,$_conditionOnlyBreaks,$_fontBold,$_fontItalic,$_fontStrikeOut,$_fontUnderline,$_show,$_showText,$_conditionLevel,$_dataCase,$_dataLength,$_dataType,$_displayLength,$_displayOrder,$_sortOrder,$_textDisplayLength,$_sequenceNo,$_conditionFrom,$_conditionTo,$_amountDisplayFormat = NULL,$_attributeId = NULL,$_breakColName = NULL,$_breakText = NULL,$_color = NULL,$_columnHeaderText = NULL,$_columnName = NULL,$_conditionType = NULL,$_fontFamily = NULL,$_formula = NULL,$_sectionType = NULL,$_sectionFormatID = NULL,$_type = NULL)
    {
        parent::__construct(array('Break'=>$_break,'ConditionInUse'=>$_conditionInUse,'ConditionOnlyBreaks'=>$_conditionOnlyBreaks,'FontBold'=>$_fontBold,'FontItalic'=>$_fontItalic,'FontStrikeOut'=>$_fontStrikeOut,'FontUnderline'=>$_fontUnderline,'Show'=>$_show,'ShowText'=>$_showText,'ConditionLevel'=>$_conditionLevel,'DataCase'=>$_dataCase,'DataLength'=>$_dataLength,'DataType'=>$_dataType,'DisplayLength'=>$_displayLength,'DisplayOrder'=>$_displayOrder,'SortOrder'=>$_sortOrder,'TextDisplayLength'=>$_textDisplayLength,'SequenceNo'=>$_sequenceNo,'ConditionFrom'=>$_conditionFrom,'ConditionTo'=>$_conditionTo,'AmountDisplayFormat'=>$_amountDisplayFormat,'AttributeId'=>$_attributeId,'BreakColName'=>$_breakColName,'BreakText'=>$_breakText,'Color'=>$_color,'ColumnHeaderText'=>$_columnHeaderText,'ColumnName'=>$_columnName,'ConditionType'=>$_conditionType,'FontFamily'=>$_fontFamily,'Formula'=>$_formula,'SectionType'=>$_sectionType,'SectionFormatID'=>$_sectionFormatID,'Type'=>$_type),false);
    }
    /**
     * Get Break value
     * @return boolean
     */
    public function getBreak()
    {
        return $this->Break;
    }
    /**
     * Set Break value
     * @param boolean $_break the Break
     * @return boolean
     */
    public function setBreak($_break)
    {
        return ($this->Break = $_break);
    }
    /**
     * Get ConditionInUse value
     * @return boolean
     */
    public function getConditionInUse()
    {
        return $this->ConditionInUse;
    }
    /**
     * Set ConditionInUse value
     * @param boolean $_conditionInUse the ConditionInUse
     * @return boolean
     */
    public function setConditionInUse($_conditionInUse)
    {
        return ($this->ConditionInUse = $_conditionInUse);
    }
    /**
     * Get ConditionOnlyBreaks value
     * @return boolean
     */
    public function getConditionOnlyBreaks()
    {
        return $this->ConditionOnlyBreaks;
    }
    /**
     * Set ConditionOnlyBreaks value
     * @param boolean $_conditionOnlyBreaks the ConditionOnlyBreaks
     * @return boolean
     */
    public function setConditionOnlyBreaks($_conditionOnlyBreaks)
    {
        return ($this->ConditionOnlyBreaks = $_conditionOnlyBreaks);
    }
    /**
     * Get FontBold value
     * @return boolean
     */
    public function getFontBold()
    {
        return $this->FontBold;
    }
    /**
     * Set FontBold value
     * @param boolean $_fontBold the FontBold
     * @return boolean
     */
    public function setFontBold($_fontBold)
    {
        return ($this->FontBold = $_fontBold);
    }
    /**
     * Get FontItalic value
     * @return boolean
     */
    public function getFontItalic()
    {
        return $this->FontItalic;
    }
    /**
     * Set FontItalic value
     * @param boolean $_fontItalic the FontItalic
     * @return boolean
     */
    public function setFontItalic($_fontItalic)
    {
        return ($this->FontItalic = $_fontItalic);
    }
    /**
     * Get FontStrikeOut value
     * @return boolean
     */
    public function getFontStrikeOut()
    {
        return $this->FontStrikeOut;
    }
    /**
     * Set FontStrikeOut value
     * @param boolean $_fontStrikeOut the FontStrikeOut
     * @return boolean
     */
    public function setFontStrikeOut($_fontStrikeOut)
    {
        return ($this->FontStrikeOut = $_fontStrikeOut);
    }
    /**
     * Get FontUnderline value
     * @return boolean
     */
    public function getFontUnderline()
    {
        return $this->FontUnderline;
    }
    /**
     * Set FontUnderline value
     * @param boolean $_fontUnderline the FontUnderline
     * @return boolean
     */
    public function setFontUnderline($_fontUnderline)
    {
        return ($this->FontUnderline = $_fontUnderline);
    }
    /**
     * Get Show value
     * @return boolean
     */
    public function getShow()
    {
        return $this->Show;
    }
    /**
     * Set Show value
     * @param boolean $_show the Show
     * @return boolean
     */
    public function setShow($_show)
    {
        return ($this->Show = $_show);
    }
    /**
     * Get ShowText value
     * @return boolean
     */
    public function getShowText()
    {
        return $this->ShowText;
    }
    /**
     * Set ShowText value
     * @param boolean $_showText the ShowText
     * @return boolean
     */
    public function setShowText($_showText)
    {
        return ($this->ShowText = $_showText);
    }
    /**
     * Get ConditionLevel value
     * @return int
     */
    public function getConditionLevel()
    {
        return $this->ConditionLevel;
    }
    /**
     * Set ConditionLevel value
     * @param int $_conditionLevel the ConditionLevel
     * @return int
     */
    public function setConditionLevel($_conditionLevel)
    {
        return ($this->ConditionLevel = $_conditionLevel);
    }
    /**
     * Get DataCase value
     * @return int
     */
    public function getDataCase()
    {
        return $this->DataCase;
    }
    /**
     * Set DataCase value
     * @param int $_dataCase the DataCase
     * @return int
     */
    public function setDataCase($_dataCase)
    {
        return ($this->DataCase = $_dataCase);
    }
    /**
     * Get DataLength value
     * @return int
     */
    public function getDataLength()
    {
        return $this->DataLength;
    }
    /**
     * Set DataLength value
     * @param int $_dataLength the DataLength
     * @return int
     */
    public function setDataLength($_dataLength)
    {
        return ($this->DataLength = $_dataLength);
    }
    /**
     * Get DataType value
     * @return int
     */
    public function getDataType()
    {
        return $this->DataType;
    }
    /**
     * Set DataType value
     * @param int $_dataType the DataType
     * @return int
     */
    public function setDataType($_dataType)
    {
        return ($this->DataType = $_dataType);
    }
    /**
     * Get DisplayLength value
     * @return int
     */
    public function getDisplayLength()
    {
        return $this->DisplayLength;
    }
    /**
     * Set DisplayLength value
     * @param int $_displayLength the DisplayLength
     * @return int
     */
    public function setDisplayLength($_displayLength)
    {
        return ($this->DisplayLength = $_displayLength);
    }
    /**
     * Get DisplayOrder value
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->DisplayOrder;
    }
    /**
     * Set DisplayOrder value
     * @param int $_displayOrder the DisplayOrder
     * @return int
     */
    public function setDisplayOrder($_displayOrder)
    {
        return ($this->DisplayOrder = $_displayOrder);
    }
    /**
     * Get SortOrder value
     * @return int
     */
    public function getSortOrder()
    {
        return $this->SortOrder;
    }
    /**
     * Set SortOrder value
     * @param int $_sortOrder the SortOrder
     * @return int
     */
    public function setSortOrder($_sortOrder)
    {
        return ($this->SortOrder = $_sortOrder);
    }
    /**
     * Get TextDisplayLength value
     * @return int
     */
    public function getTextDisplayLength()
    {
        return $this->TextDisplayLength;
    }
    /**
     * Set TextDisplayLength value
     * @param int $_textDisplayLength the TextDisplayLength
     * @return int
     */
    public function setTextDisplayLength($_textDisplayLength)
    {
        return ($this->TextDisplayLength = $_textDisplayLength);
    }
    /**
     * Get SequenceNo value
     * @return int
     */
    public function getSequenceNo()
    {
        return $this->SequenceNo;
    }
    /**
     * Set SequenceNo value
     * @param int $_sequenceNo the SequenceNo
     * @return int
     */
    public function setSequenceNo($_sequenceNo)
    {
        return ($this->SequenceNo = $_sequenceNo);
    }
    /**
     * Get ConditionFrom value
     * @return double
     */
    public function getConditionFrom()
    {
        return $this->ConditionFrom;
    }
    /**
     * Set ConditionFrom value
     * @param double $_conditionFrom the ConditionFrom
     * @return double
     */
    public function setConditionFrom($_conditionFrom)
    {
        return ($this->ConditionFrom = $_conditionFrom);
    }
    /**
     * Get ConditionTo value
     * @return double
     */
    public function getConditionTo()
    {
        return $this->ConditionTo;
    }
    /**
     * Set ConditionTo value
     * @param double $_conditionTo the ConditionTo
     * @return double
     */
    public function setConditionTo($_conditionTo)
    {
        return ($this->ConditionTo = $_conditionTo);
    }
    /**
     * Get AmountDisplayFormat value
     * @return string|null
     */
    public function getAmountDisplayFormat()
    {
        return $this->AmountDisplayFormat;
    }
    /**
     * Set AmountDisplayFormat value
     * @param string $_amountDisplayFormat the AmountDisplayFormat
     * @return string
     */
    public function setAmountDisplayFormat($_amountDisplayFormat)
    {
        return ($this->AmountDisplayFormat = $_amountDisplayFormat);
    }
    /**
     * Get AttributeId value
     * @return string|null
     */
    public function getAttributeId()
    {
        return $this->AttributeId;
    }
    /**
     * Set AttributeId value
     * @param string $_attributeId the AttributeId
     * @return string
     */
    public function setAttributeId($_attributeId)
    {
        return ($this->AttributeId = $_attributeId);
    }
    /**
     * Get BreakColName value
     * @return string|null
     */
    public function getBreakColName()
    {
        return $this->BreakColName;
    }
    /**
     * Set BreakColName value
     * @param string $_breakColName the BreakColName
     * @return string
     */
    public function setBreakColName($_breakColName)
    {
        return ($this->BreakColName = $_breakColName);
    }
    /**
     * Get BreakText value
     * @return string|null
     */
    public function getBreakText()
    {
        return $this->BreakText;
    }
    /**
     * Set BreakText value
     * @param string $_breakText the BreakText
     * @return string
     */
    public function setBreakText($_breakText)
    {
        return ($this->BreakText = $_breakText);
    }
    /**
     * Get Color value
     * @return string|null
     */
    public function getColor()
    {
        return $this->Color;
    }
    /**
     * Set Color value
     * @param string $_color the Color
     * @return string
     */
    public function setColor($_color)
    {
        return ($this->Color = $_color);
    }
    /**
     * Get ColumnHeaderText value
     * @return string|null
     */
    public function getColumnHeaderText()
    {
        return $this->ColumnHeaderText;
    }
    /**
     * Set ColumnHeaderText value
     * @param string $_columnHeaderText the ColumnHeaderText
     * @return string
     */
    public function setColumnHeaderText($_columnHeaderText)
    {
        return ($this->ColumnHeaderText = $_columnHeaderText);
    }
    /**
     * Get ColumnName value
     * @return string|null
     */
    public function getColumnName()
    {
        return $this->ColumnName;
    }
    /**
     * Set ColumnName value
     * @param string $_columnName the ColumnName
     * @return string
     */
    public function setColumnName($_columnName)
    {
        return ($this->ColumnName = $_columnName);
    }
    /**
     * Get ConditionType value
     * @return string|null
     */
    public function getConditionType()
    {
        return $this->ConditionType;
    }
    /**
     * Set ConditionType value
     * @param string $_conditionType the ConditionType
     * @return string
     */
    public function setConditionType($_conditionType)
    {
        return ($this->ConditionType = $_conditionType);
    }
    /**
     * Get FontFamily value
     * @return string|null
     */
    public function getFontFamily()
    {
        return $this->FontFamily;
    }
    /**
     * Set FontFamily value
     * @param string $_fontFamily the FontFamily
     * @return string
     */
    public function setFontFamily($_fontFamily)
    {
        return ($this->FontFamily = $_fontFamily);
    }
    /**
     * Get Formula value
     * @return string|null
     */
    public function getFormula()
    {
        return $this->Formula;
    }
    /**
     * Set Formula value
     * @param string $_formula the Formula
     * @return string
     */
    public function setFormula($_formula)
    {
        return ($this->Formula = $_formula);
    }
    /**
     * Get SectionType value
     * @return string|null
     */
    public function getSectionType()
    {
        return $this->SectionType;
    }
    /**
     * Set SectionType value
     * @param string $_sectionType the SectionType
     * @return string
     */
    public function setSectionType($_sectionType)
    {
        return ($this->SectionType = $_sectionType);
    }
    /**
     * Get SectionFormatID value
     * @return string|null
     */
    public function getSectionFormatID()
    {
        return $this->SectionFormatID;
    }
    /**
     * Set SectionFormatID value
     * @param string $_sectionFormatID the SectionFormatID
     * @return string
     */
    public function setSectionFormatID($_sectionFormatID)
    {
        return ($this->SectionFormatID = $_sectionFormatID);
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param string $_type the Type
     * @return string
     */
    public function setType($_type)
    {
        return ($this->Type = $_type);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructFormatProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
