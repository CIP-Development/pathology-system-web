<?php
/**
 * File for class MELOCSWSStructTemplateHeader
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateHeader originally named TemplateHeader
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateHeader extends MELOCSWSWsdlClass
{
    /**
     * The TemplateId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $TemplateId;
    /**
     * The CompanyCode
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $CompanyCode;
    /**
     * The Frame
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Frame;
    /**
     * The FunctionId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $FunctionId;
    /**
     * The Module
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Module;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Name;
    /**
     * The UserId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $UserId;
    /**
     * Constructor method for TemplateHeader
     * @see parent::__construct()
     * @param long $_templateId
     * @param string $_companyCode
     * @param string $_frame
     * @param string $_functionId
     * @param string $_module
     * @param string $_name
     * @param string $_userId
     * @return MELOCSWSStructTemplateHeader
     */
    public function __construct($_templateId,$_companyCode = NULL,$_frame = NULL,$_functionId = NULL,$_module = NULL,$_name = NULL,$_userId = NULL)
    {
        parent::__construct(array('TemplateId'=>$_templateId,'CompanyCode'=>$_companyCode,'Frame'=>$_frame,'FunctionId'=>$_functionId,'Module'=>$_module,'Name'=>$_name,'UserId'=>$_userId),false);
    }
    /**
     * Get TemplateId value
     * @return long
     */
    public function getTemplateId()
    {
        return $this->TemplateId;
    }
    /**
     * Set TemplateId value
     * @param long $_templateId the TemplateId
     * @return long
     */
    public function setTemplateId($_templateId)
    {
        return ($this->TemplateId = $_templateId);
    }
    /**
     * Get CompanyCode value
     * @return string|null
     */
    public function getCompanyCode()
    {
        return $this->CompanyCode;
    }
    /**
     * Set CompanyCode value
     * @param string $_companyCode the CompanyCode
     * @return string
     */
    public function setCompanyCode($_companyCode)
    {
        return ($this->CompanyCode = $_companyCode);
    }
    /**
     * Get Frame value
     * @return string|null
     */
    public function getFrame()
    {
        return $this->Frame;
    }
    /**
     * Set Frame value
     * @param string $_frame the Frame
     * @return string
     */
    public function setFrame($_frame)
    {
        return ($this->Frame = $_frame);
    }
    /**
     * Get FunctionId value
     * @return string|null
     */
    public function getFunctionId()
    {
        return $this->FunctionId;
    }
    /**
     * Set FunctionId value
     * @param string $_functionId the FunctionId
     * @return string
     */
    public function setFunctionId($_functionId)
    {
        return ($this->FunctionId = $_functionId);
    }
    /**
     * Get Module value
     * @return string|null
     */
    public function getModule()
    {
        return $this->Module;
    }
    /**
     * Set Module value
     * @param string $_module the Module
     * @return string
     */
    public function setModule($_module)
    {
        return ($this->Module = $_module);
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $_name the Name
     * @return string
     */
    public function setName($_name)
    {
        return ($this->Name = $_name);
    }
    /**
     * Get UserId value
     * @return string|null
     */
    public function getUserId()
    {
        return $this->UserId;
    }
    /**
     * Set UserId value
     * @param string $_userId the UserId
     * @return string
     */
    public function setUserId($_userId)
    {
        return ($this->UserId = $_userId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateHeader
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
