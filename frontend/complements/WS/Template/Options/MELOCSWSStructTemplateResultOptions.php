<?php
/**
 * File for class MELOCSWSStructTemplateResultOptions
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateResultOptions originally named TemplateResultOptions
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateResultOptions extends MELOCSWSWsdlClass
{
    /**
     * The ShowDescriptions
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $ShowDescriptions;
    /**
     * The Aggregated
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $Aggregated;
    /**
     * The OverrideAggregation
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $OverrideAggregation;
    /**
     * The CalculateFormulas
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $CalculateFormulas;
    /**
     * The FormatAlternativeBreakColumns
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $FormatAlternativeBreakColumns;
    /**
     * The RemoveHiddenColumns
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $RemoveHiddenColumns;
    /**
     * The FirstRecord
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $FirstRecord;
    /**
     * The LastRecord
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $LastRecord;
    /**
     * The Filter
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $Filter;
    /**
     * Constructor method for TemplateResultOptions
     * @see parent::__construct()
     * @param boolean $_showDescriptions
     * @param boolean $_aggregated
     * @param boolean $_overrideAggregation
     * @param boolean $_calculateFormulas
     * @param boolean $_formatAlternativeBreakColumns
     * @param boolean $_removeHiddenColumns
     * @param long $_firstRecord
     * @param long $_lastRecord
     * @param string $_filter
     * @return MELOCSWSStructTemplateResultOptions
     */
    public function __construct($_showDescriptions,$_aggregated,$_overrideAggregation,$_calculateFormulas,$_formatAlternativeBreakColumns,$_removeHiddenColumns,$_firstRecord,$_lastRecord,$_filter = NULL)
    {
        parent::__construct(array('ShowDescriptions'=>$_showDescriptions,'Aggregated'=>$_aggregated,'OverrideAggregation'=>$_overrideAggregation,'CalculateFormulas'=>$_calculateFormulas,'FormatAlternativeBreakColumns'=>$_formatAlternativeBreakColumns,'RemoveHiddenColumns'=>$_removeHiddenColumns,'FirstRecord'=>$_firstRecord,'LastRecord'=>$_lastRecord,'Filter'=>$_filter),false);
    }
    /**
     * Get ShowDescriptions value
     * @return boolean
     */
    public function getShowDescriptions()
    {
        return $this->ShowDescriptions;
    }
    /**
     * Set ShowDescriptions value
     * @param boolean $_showDescriptions the ShowDescriptions
     * @return boolean
     */
    public function setShowDescriptions($_showDescriptions)
    {
        return ($this->ShowDescriptions = $_showDescriptions);
    }
    /**
     * Get Aggregated value
     * @return boolean
     */
    public function getAggregated()
    {
        return $this->Aggregated;
    }
    /**
     * Set Aggregated value
     * @param boolean $_aggregated the Aggregated
     * @return boolean
     */
    public function setAggregated($_aggregated)
    {
        return ($this->Aggregated = $_aggregated);
    }
    /**
     * Get OverrideAggregation value
     * @return boolean
     */
    public function getOverrideAggregation()
    {
        return $this->OverrideAggregation;
    }
    /**
     * Set OverrideAggregation value
     * @param boolean $_overrideAggregation the OverrideAggregation
     * @return boolean
     */
    public function setOverrideAggregation($_overrideAggregation)
    {
        return ($this->OverrideAggregation = $_overrideAggregation);
    }
    /**
     * Get CalculateFormulas value
     * @return boolean
     */
    public function getCalculateFormulas()
    {
        return $this->CalculateFormulas;
    }
    /**
     * Set CalculateFormulas value
     * @param boolean $_calculateFormulas the CalculateFormulas
     * @return boolean
     */
    public function setCalculateFormulas($_calculateFormulas)
    {
        return ($this->CalculateFormulas = $_calculateFormulas);
    }
    /**
     * Get FormatAlternativeBreakColumns value
     * @return boolean
     */
    public function getFormatAlternativeBreakColumns()
    {
        return $this->FormatAlternativeBreakColumns;
    }
    /**
     * Set FormatAlternativeBreakColumns value
     * @param boolean $_formatAlternativeBreakColumns the FormatAlternativeBreakColumns
     * @return boolean
     */
    public function setFormatAlternativeBreakColumns($_formatAlternativeBreakColumns)
    {
        return ($this->FormatAlternativeBreakColumns = $_formatAlternativeBreakColumns);
    }
    /**
     * Get RemoveHiddenColumns value
     * @return boolean
     */
    public function getRemoveHiddenColumns()
    {
        return $this->RemoveHiddenColumns;
    }
    /**
     * Set RemoveHiddenColumns value
     * @param boolean $_removeHiddenColumns the RemoveHiddenColumns
     * @return boolean
     */
    public function setRemoveHiddenColumns($_removeHiddenColumns)
    {
        return ($this->RemoveHiddenColumns = $_removeHiddenColumns);
    }
    /**
     * Get FirstRecord value
     * @return long
     */
    public function getFirstRecord()
    {
        return $this->FirstRecord;
    }
    /**
     * Set FirstRecord value
     * @param long $_firstRecord the FirstRecord
     * @return long
     */
    public function setFirstRecord($_firstRecord)
    {
        return ($this->FirstRecord = $_firstRecord);
    }
    /**
     * Get LastRecord value
     * @return long
     */
    public function getLastRecord()
    {
        return $this->LastRecord;
    }
    /**
     * Set LastRecord value
     * @param long $_lastRecord the LastRecord
     * @return long
     */
    public function setLastRecord($_lastRecord)
    {
        return ($this->LastRecord = $_lastRecord);
    }
    /**
     * Get Filter value
     * @return string|null
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param string $_filter the Filter
     * @return string
     */
    public function setFilter($_filter)
    {
        return ($this->Filter = $_filter);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateResultOptions
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
