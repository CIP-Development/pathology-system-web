<?php
/**
 * File for class MELOCSWSStructTemplateList
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateList originally named TemplateList
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateList extends MELOCSWSWsdlClass
{
    /**
     * The TemplateHeaderList
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructArrayOfTemplateHeader
     */
    public $TemplateHeaderList;
    /**
     * Constructor method for TemplateList
     * @see parent::__construct()
     * @param MELOCSWSStructArrayOfTemplateHeader $_templateHeaderList
     * @return MELOCSWSStructTemplateList
     */
    public function __construct($_templateHeaderList = NULL)
    {
        parent::__construct(array('TemplateHeaderList'=>($_templateHeaderList instanceof MELOCSWSStructArrayOfTemplateHeader)?$_templateHeaderList:new MELOCSWSStructArrayOfTemplateHeader($_templateHeaderList)),false);
    }
    /**
     * Get TemplateHeaderList value
     * @return MELOCSWSStructArrayOfTemplateHeader|null
     */
    public function getTemplateHeaderList()
    {
        return $this->TemplateHeaderList;
    }
    /**
     * Set TemplateHeaderList value
     * @param MELOCSWSStructArrayOfTemplateHeader $_templateHeaderList the TemplateHeaderList
     * @return MELOCSWSStructArrayOfTemplateHeader
     */
    public function setTemplateHeaderList($_templateHeaderList)
    {
        return ($this->TemplateHeaderList = $_templateHeaderList);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateList
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
