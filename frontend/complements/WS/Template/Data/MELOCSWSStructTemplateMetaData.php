<?php
/**
 * File for class MELOCSWSStructTemplateMetaData
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructTemplateMetaData originally named TemplateMetaData
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructTemplateMetaData extends MELOCSWSWsdlClass
{
    /**
     * The SearchCriteria
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructSearchCriteria
     */
    public $SearchCriteria;
    /**
     * The TemplateProperties
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateProperties
     */
    public $TemplateProperties;
    /**
     * The FormatInfo
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructFormatInfo
     */
    public $FormatInfo;
    /**
     * The Expression
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructExpression
     */
    public $Expression;
    /**
     * The StatisticalFormula
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructStatisticalFormula
     */
    public $StatisticalFormula;
    /**
     * Constructor method for TemplateMetaData
     * @see parent::__construct()
     * @param MELOCSWSStructSearchCriteria $_searchCriteria
     * @param MELOCSWSStructTemplateProperties $_templateProperties
     * @param MELOCSWSStructFormatInfo $_formatInfo
     * @param MELOCSWSStructExpression $_expression
     * @param MELOCSWSStructStatisticalFormula $_statisticalFormula
     * @return MELOCSWSStructTemplateMetaData
     */
    public function __construct($_searchCriteria = NULL,$_templateProperties = NULL,$_formatInfo = NULL,$_expression = NULL,$_statisticalFormula = NULL)
    {
        parent::__construct(array('SearchCriteria'=>$_searchCriteria,'TemplateProperties'=>$_templateProperties,'FormatInfo'=>$_formatInfo,'Expression'=>$_expression,'StatisticalFormula'=>$_statisticalFormula),false);
    }
    /**
     * Get SearchCriteria value
     * @return MELOCSWSStructSearchCriteria|null
     */
    public function getSearchCriteria()
    {
        return $this->SearchCriteria;
    }
    /**
     * Set SearchCriteria value
     * @param MELOCSWSStructSearchCriteria $_searchCriteria the SearchCriteria
     * @return MELOCSWSStructSearchCriteria
     */
    public function setSearchCriteria($_searchCriteria)
    {
        return ($this->SearchCriteria = $_searchCriteria);
    }
    /**
     * Get TemplateProperties value
     * @return MELOCSWSStructTemplateProperties|null
     */
    public function getTemplateProperties()
    {
        return $this->TemplateProperties;
    }
    /**
     * Set TemplateProperties value
     * @param MELOCSWSStructTemplateProperties $_templateProperties the TemplateProperties
     * @return MELOCSWSStructTemplateProperties
     */
    public function setTemplateProperties($_templateProperties)
    {
        return ($this->TemplateProperties = $_templateProperties);
    }
    /**
     * Get FormatInfo value
     * @return MELOCSWSStructFormatInfo|null
     */
    public function getFormatInfo()
    {
        return $this->FormatInfo;
    }
    /**
     * Set FormatInfo value
     * @param MELOCSWSStructFormatInfo $_formatInfo the FormatInfo
     * @return MELOCSWSStructFormatInfo
     */
    public function setFormatInfo($_formatInfo)
    {
        return ($this->FormatInfo = $_formatInfo);
    }
    /**
     * Get Expression value
     * @return MELOCSWSStructExpression|null
     */
    public function getExpression()
    {
        return $this->Expression;
    }
    /**
     * Set Expression value
     * @param MELOCSWSStructExpression $_expression the Expression
     * @return MELOCSWSStructExpression
     */
    public function setExpression($_expression)
    {
        return ($this->Expression = $_expression);
    }
    /**
     * Get StatisticalFormula value
     * @return MELOCSWSStructStatisticalFormula|null
     */
    public function getStatisticalFormula()
    {
        return $this->StatisticalFormula;
    }
    /**
     * Set StatisticalFormula value
     * @param MELOCSWSStructStatisticalFormula $_statisticalFormula the StatisticalFormula
     * @return MELOCSWSStructStatisticalFormula
     */
    public function setStatisticalFormula($_statisticalFormula)
    {
        return ($this->StatisticalFormula = $_statisticalFormula);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructTemplateMetaData
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
