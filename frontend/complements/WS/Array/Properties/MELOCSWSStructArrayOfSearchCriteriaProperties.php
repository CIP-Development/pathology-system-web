<?php
/**
 * File for class MELOCSWSStructArrayOfSearchCriteriaProperties
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructArrayOfSearchCriteriaProperties originally named ArrayOfSearchCriteriaProperties
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructArrayOfSearchCriteriaProperties extends MELOCSWSWsdlClass
{
    /**
     * The SearchCriteriaProperties
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var MELOCSWSStructSearchCriteriaProperties
     */
    public $SearchCriteriaProperties;
    /**
     * Constructor method for ArrayOfSearchCriteriaProperties
     * @see parent::__construct()
     * @param MELOCSWSStructSearchCriteriaProperties $_searchCriteriaProperties
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public function __construct($_searchCriteriaProperties = NULL)
    {
        parent::__construct(array('SearchCriteriaProperties'=>$_searchCriteriaProperties),false);
    }
    /**
     * Get SearchCriteriaProperties value
     * @return MELOCSWSStructSearchCriteriaProperties|null
     */
    public function getSearchCriteriaProperties()
    {
        return $this->SearchCriteriaProperties;
    }
    /**
     * Set SearchCriteriaProperties value
     * @param MELOCSWSStructSearchCriteriaProperties $_searchCriteriaProperties the SearchCriteriaProperties
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function setSearchCriteriaProperties($_searchCriteriaProperties)
    {
        return ($this->SearchCriteriaProperties = $_searchCriteriaProperties);
    }
    /**
     * Returns the current element
     * @see MELOCSWSWsdlClass::current()
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see MELOCSWSWsdlClass::item()
     * @param int $_index
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function item($_index)
    {
        return parent::item($_index);
    }
    /**
     * Returns the first element
     * @see MELOCSWSWsdlClass::first()
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see MELOCSWSWsdlClass::last()
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see MELOCSWSWsdlClass::last()
     * @param int $_offset
     * @return MELOCSWSStructSearchCriteriaProperties
     */
    public function offsetGet($_offset)
    {
        return parent::offsetGet($_offset);
    }
    /**
     * Returns the attribute name
     * @see MELOCSWSWsdlClass::getAttributeName()
     * @return string SearchCriteriaProperties
     */
    public function getAttributeName()
    {
        return 'SearchCriteriaProperties';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructArrayOfSearchCriteriaProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
