<?php
/**
 * File for class MELOCSWSStructAbout
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructAbout originally named About
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructAbout extends MELOCSWSWsdlClass
{
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
