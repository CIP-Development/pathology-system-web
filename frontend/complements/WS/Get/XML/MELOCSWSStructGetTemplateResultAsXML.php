<?php
/**
 * File for class MELOCSWSStructGetTemplateResultAsXML
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateResultAsXML originally named GetTemplateResultAsXML
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateResultAsXML extends MELOCSWSWsdlClass
{
    /**
     * The input
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructInputForTemplateResult
     */
    public $input;
    /**
     * The credentials
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructWSCredentials
     */
    public $credentials;
    /**
     * Constructor method for GetTemplateResultAsXML
     * @see parent::__construct()
     * @param MELOCSWSStructInputForTemplateResult $_input
     * @param MELOCSWSStructWSCredentials $_credentials
     * @return MELOCSWSStructGetTemplateResultAsXML
     */
    public function __construct($_input = NULL,$_credentials = NULL)
    {
        parent::__construct(array('input'=>$_input,'credentials'=>$_credentials),false);
    }
    /**
     * Get input value
     * @return MELOCSWSStructInputForTemplateResult|null
     */
    public function getInput()
    {
        return $this->input;
    }
    /**
     * Set input value
     * @param MELOCSWSStructInputForTemplateResult $_input the input
     * @return MELOCSWSStructInputForTemplateResult
     */
    public function setInput($_input)
    {
        return ($this->input = $_input);
    }
    /**
     * Get credentials value
     * @return MELOCSWSStructWSCredentials|null
     */
    public function getCredentials()
    {
        return $this->credentials;
    }
    /**
     * Set credentials value
     * @param MELOCSWSStructWSCredentials $_credentials the credentials
     * @return MELOCSWSStructWSCredentials
     */
    public function setCredentials($_credentials)
    {
        return ($this->credentials = $_credentials);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateResultAsXML
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
