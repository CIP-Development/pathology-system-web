<?php
/**
 * File for class MELOCSWSStructGetSearchCriteria
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetSearchCriteria originally named GetSearchCriteria
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetSearchCriteria extends MELOCSWSWsdlClass
{
    /**
     * The templateId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var long
     */
    public $templateId;
    /**
     * The hideUnused
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $hideUnused;
    /**
     * The credentials
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructWSCredentials
     */
    public $credentials;
    /**
     * Constructor method for GetSearchCriteria
     * @see parent::__construct()
     * @param long $_templateId
     * @param boolean $_hideUnused
     * @param MELOCSWSStructWSCredentials $_credentials
     * @return MELOCSWSStructGetSearchCriteria
     */
    public function __construct($_templateId,$_hideUnused,$_credentials = NULL)
    {
        parent::__construct(array('templateId'=>$_templateId,'hideUnused'=>$_hideUnused,'credentials'=>$_credentials),false);
    }
    /**
     * Get templateId value
     * @return long
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }
    /**
     * Set templateId value
     * @param long $_templateId the templateId
     * @return long
     */
    public function setTemplateId($_templateId)
    {
        return ($this->templateId = $_templateId);
    }
    /**
     * Get hideUnused value
     * @return boolean
     */
    public function getHideUnused()
    {
        return $this->hideUnused;
    }
    /**
     * Set hideUnused value
     * @param boolean $_hideUnused the hideUnused
     * @return boolean
     */
    public function setHideUnused($_hideUnused)
    {
        return ($this->hideUnused = $_hideUnused);
    }
    /**
     * Get credentials value
     * @return MELOCSWSStructWSCredentials|null
     */
    public function getCredentials()
    {
        return $this->credentials;
    }
    /**
     * Set credentials value
     * @param MELOCSWSStructWSCredentials $_credentials the credentials
     * @return MELOCSWSStructWSCredentials
     */
    public function setCredentials($_credentials)
    {
        return ($this->credentials = $_credentials);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetSearchCriteria
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
