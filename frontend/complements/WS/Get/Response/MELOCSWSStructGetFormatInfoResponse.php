<?php
/**
 * File for class MELOCSWSStructGetFormatInfoResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetFormatInfoResponse originally named GetFormatInfoResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetFormatInfoResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetFormatInfoResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructFormatInfo
     */
    public $GetFormatInfoResult;
    /**
     * Constructor method for GetFormatInfoResponse
     * @see parent::__construct()
     * @param MELOCSWSStructFormatInfo $_getFormatInfoResult
     * @return MELOCSWSStructGetFormatInfoResponse
     */
    public function __construct($_getFormatInfoResult = NULL)
    {
        parent::__construct(array('GetFormatInfoResult'=>$_getFormatInfoResult),false);
    }
    /**
     * Get GetFormatInfoResult value
     * @return MELOCSWSStructFormatInfo|null
     */
    public function getGetFormatInfoResult()
    {
        return $this->GetFormatInfoResult;
    }
    /**
     * Set GetFormatInfoResult value
     * @param MELOCSWSStructFormatInfo $_getFormatInfoResult the GetFormatInfoResult
     * @return MELOCSWSStructFormatInfo
     */
    public function setGetFormatInfoResult($_getFormatInfoResult)
    {
        return ($this->GetFormatInfoResult = $_getFormatInfoResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetFormatInfoResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
