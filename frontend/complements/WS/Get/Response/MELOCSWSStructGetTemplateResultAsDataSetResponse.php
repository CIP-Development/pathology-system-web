<?php
/**
 * File for class MELOCSWSStructGetTemplateResultAsDataSetResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateResultAsDataSetResponse originally named GetTemplateResultAsDataSetResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateResultAsDataSetResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplateResultAsDataSetResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateResultAsDataSet
     */
    public $GetTemplateResultAsDataSetResult;
    /**
     * Constructor method for GetTemplateResultAsDataSetResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateResultAsDataSet $_getTemplateResultAsDataSetResult
     * @return MELOCSWSStructGetTemplateResultAsDataSetResponse
     */
    public function __construct($_getTemplateResultAsDataSetResult = NULL)
    {
        parent::__construct(array('GetTemplateResultAsDataSetResult'=>$_getTemplateResultAsDataSetResult),false);
    }
    /**
     * Get GetTemplateResultAsDataSetResult value
     * @return MELOCSWSStructTemplateResultAsDataSet|null
     */
    public function getGetTemplateResultAsDataSetResult()
    {
        return $this->GetTemplateResultAsDataSetResult;
    }
    /**
     * Set GetTemplateResultAsDataSetResult value
     * @param MELOCSWSStructTemplateResultAsDataSet $_getTemplateResultAsDataSetResult the GetTemplateResultAsDataSetResult
     * @return MELOCSWSStructTemplateResultAsDataSet
     */
    public function setGetTemplateResultAsDataSetResult($_getTemplateResultAsDataSetResult)
    {
        return ($this->GetTemplateResultAsDataSetResult = $_getTemplateResultAsDataSetResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateResultAsDataSetResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
