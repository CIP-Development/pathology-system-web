<?php
/**
 * File for class MELOCSWSStructGetTemplateResultOptionsResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateResultOptionsResponse originally named GetTemplateResultOptionsResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateResultOptionsResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplateResultOptionsResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateResultOptions
     */
    public $GetTemplateResultOptionsResult;
    /**
     * Constructor method for GetTemplateResultOptionsResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateResultOptions $_getTemplateResultOptionsResult
     * @return MELOCSWSStructGetTemplateResultOptionsResponse
     */
    public function __construct($_getTemplateResultOptionsResult = NULL)
    {
        parent::__construct(array('GetTemplateResultOptionsResult'=>$_getTemplateResultOptionsResult),false);
    }
    /**
     * Get GetTemplateResultOptionsResult value
     * @return MELOCSWSStructTemplateResultOptions|null
     */
    public function getGetTemplateResultOptionsResult()
    {
        return $this->GetTemplateResultOptionsResult;
    }
    /**
     * Set GetTemplateResultOptionsResult value
     * @param MELOCSWSStructTemplateResultOptions $_getTemplateResultOptionsResult the GetTemplateResultOptionsResult
     * @return MELOCSWSStructTemplateResultOptions
     */
    public function setGetTemplateResultOptionsResult($_getTemplateResultOptionsResult)
    {
        return ($this->GetTemplateResultOptionsResult = $_getTemplateResultOptionsResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateResultOptionsResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
