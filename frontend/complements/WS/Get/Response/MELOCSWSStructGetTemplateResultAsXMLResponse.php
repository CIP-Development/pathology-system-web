<?php
/**
 * File for class MELOCSWSStructGetTemplateResultAsXMLResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetTemplateResultAsXMLResponse originally named GetTemplateResultAsXMLResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetTemplateResultAsXMLResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetTemplateResultAsXMLResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructTemplateResultAsXML
     */
    public $GetTemplateResultAsXMLResult;
    /**
     * Constructor method for GetTemplateResultAsXMLResponse
     * @see parent::__construct()
     * @param MELOCSWSStructTemplateResultAsXML $_getTemplateResultAsXMLResult
     * @return MELOCSWSStructGetTemplateResultAsXMLResponse
     */
    public function __construct($_getTemplateResultAsXMLResult = NULL)
    {
        parent::__construct(array('GetTemplateResultAsXMLResult'=>$_getTemplateResultAsXMLResult),false);
    }
    /**
     * Get GetTemplateResultAsXMLResult value
     * @return MELOCSWSStructTemplateResultAsXML|null
     */
    public function getGetTemplateResultAsXMLResult()
    {
        return $this->GetTemplateResultAsXMLResult;
    }
    /**
     * Set GetTemplateResultAsXMLResult value
     * @param MELOCSWSStructTemplateResultAsXML $_getTemplateResultAsXMLResult the GetTemplateResultAsXMLResult
     * @return MELOCSWSStructTemplateResultAsXML
     */
    public function setGetTemplateResultAsXMLResult($_getTemplateResultAsXMLResult)
    {
        return ($this->GetTemplateResultAsXMLResult = $_getTemplateResultAsXMLResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetTemplateResultAsXMLResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
