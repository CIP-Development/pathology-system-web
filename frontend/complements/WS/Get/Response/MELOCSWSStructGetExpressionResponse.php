<?php
/**
 * File for class MELOCSWSStructGetExpressionResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetExpressionResponse originally named GetExpressionResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetExpressionResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetExpressionResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructExpression
     */
    public $GetExpressionResult;
    /**
     * Constructor method for GetExpressionResponse
     * @see parent::__construct()
     * @param MELOCSWSStructExpression $_getExpressionResult
     * @return MELOCSWSStructGetExpressionResponse
     */
    public function __construct($_getExpressionResult = NULL)
    {
        parent::__construct(array('GetExpressionResult'=>$_getExpressionResult),false);
    }
    /**
     * Get GetExpressionResult value
     * @return MELOCSWSStructExpression|null
     */
    public function getGetExpressionResult()
    {
        return $this->GetExpressionResult;
    }
    /**
     * Set GetExpressionResult value
     * @param MELOCSWSStructExpression $_getExpressionResult the GetExpressionResult
     * @return MELOCSWSStructExpression
     */
    public function setGetExpressionResult($_getExpressionResult)
    {
        return ($this->GetExpressionResult = $_getExpressionResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetExpressionResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
