<?php
/**
 * File for class MELOCSWSStructGetSearchCriteriaResponse
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
/**
 * This class stands for MELOCSWSStructGetSearchCriteriaResponse originally named GetSearchCriteriaResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link https://cip.ocs.cgiar.org/agressowshost_cip_abw_prod/service.svc?QueryEngineService/QueryEngineV201101}
 * @package MELOCSWS
 * @subpackage Structs
 * @date 2016-01-19
 */
class MELOCSWSStructGetSearchCriteriaResponse extends MELOCSWSWsdlClass
{
    /**
     * The GetSearchCriteriaResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var MELOCSWSStructSearchCriteria
     */
    public $GetSearchCriteriaResult;
    /**
     * Constructor method for GetSearchCriteriaResponse
     * @see parent::__construct()
     * @param MELOCSWSStructSearchCriteria $_getSearchCriteriaResult
     * @return MELOCSWSStructGetSearchCriteriaResponse
     */
    public function __construct($_getSearchCriteriaResult = NULL)
    {
        parent::__construct(array('GetSearchCriteriaResult'=>$_getSearchCriteriaResult),false);
    }
    /**
     * Get GetSearchCriteriaResult value
     * @return MELOCSWSStructSearchCriteria|null
     */
    public function getGetSearchCriteriaResult()
    {
        return $this->GetSearchCriteriaResult;
    }
    /**
     * Set GetSearchCriteriaResult value
     * @param MELOCSWSStructSearchCriteria $_getSearchCriteriaResult the GetSearchCriteriaResult
     * @return MELOCSWSStructSearchCriteria
     */
    public function setGetSearchCriteriaResult($_getSearchCriteriaResult)
    {
        return ($this->GetSearchCriteriaResult = $_getSearchCriteriaResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see MELOCSWSWsdlClass::__set_state()
     * @uses MELOCSWSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return MELOCSWSStructGetSearchCriteriaResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
