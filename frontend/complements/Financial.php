<?php

/*
Copyright (C) 2021 International Potato Centre (CIP)

This file is part of CIP Pathlogy System, developed by CIP IT staff from RIU unit and external software provider contracted by CIP.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

This Software and the work of the developers shall be expressly acknowledged in any modified or derivative product based on the Software.

This notice shall be included in all copies or substantial portions of the Software.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

International Potato Centre
Apartado 1558, Lima 12, Peru
cip@cgiar.org - www.cipotato.org
*/


namespace frontend\complements;

use Yii;

class Financial extends \yii\db\ActiveRecord
{


  public function getBuses()
  {
    $bus = (include 'Buses.php');
    $arrayBus = $bus['paymentInfo'];
    $arrayBus = array_filter(
      $arrayBus,
      function ($element) {
        return ($element['bus_status'] != null);
      }
    );
    return
      [
        'paymentInfo' => $arrayBus,
      ];
  }

  public function getBus($task)
  {
    $arrayListBus = $this->getBuses();
    $arrayListBusFilter = array_filter($arrayListBus['paymentInfo'], function ($element) use ($task) {
      return ($element['task_id']
        == $task);
    });
    $arrayBus = null;
    foreach ($arrayListBusFilter as $key => $value) {
      $arrayBus = array(
        'tab' =>  $value['tab'],
        'project_id' =>  $value['project_id'],
        'project' =>  $value['project'],
        'bus' =>  $value['bus'],
        'bus_id' =>  $value['bus_id'],
        'bus_status' =>  $value['bus_status'],
        'date_to' =>  $value['date_to'],
        'resource_id' =>  $value['resource_id'],
        'description' =>  $value['description'],
        'task_id' =>  $value['task_id'],
        'task' =>  $value['task'],
        'task_status' =>  $value['task_satus'],
        'email' =>  $value['e_mail'],
      );
    }

    //LOGIC IF MORE THAN ONE
    return $arrayBus;
  }
}
