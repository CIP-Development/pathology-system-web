<?php
$this->title = 'CYP Pathology';
?>
<div class="site-index">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="lab-pictures\4000_3000\banner1.jpg" alt="" width="100%" max-height="100%">
                <div class="carousel-caption">
                    <div class="jumbotron">
                        <h1>CIP-PATHOLOGY!</h1>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="lab-pictures\4000_3000\banner2.jpg" alt="" width="100%" max-height="100%">
                <div class="carousel-caption">
                    <div class="jumbotron">
                        <h1>CIP-PATHOLOGY!</h1>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="lab-pictures\4000_3000\banner3.jpg" alt="" width="100%" max-height="100%">
                <div class="carousel-caption">
                    <div class="jumbotron">
                        <h1>CIP-PATHOLOGY!</h1>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="lab-pictures\4000_3000\banner4.jpg" alt="" width="100%" max-height="100%">
                <div class="carousel-caption">
                    <div class="jumbotron">
                        <h1>CIP-PATHOLOGY!</h1>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="lab-pictures\4000_3000\banner5.jpg" alt="" width="100%" max-height="100%">
                <div class="carousel-caption">
                    <div class="jumbotron">
                        <h1>CIP-PATHOLOGY!</h1>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>