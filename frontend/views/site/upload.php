<?php

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
?>
<div class="upload">
  <?php Pjax::begin(); ?>
  <?php $form_upload = ActiveForm::begin(
    [
      'id' => 'form-upload',
      'options' =>
      [
        'data-pjax' => true,
        'id' => 'dynamic-form-upload',
      ]
    ]
  ) ?>
  <div class="box-body">
    <div class="row">
      <?= $form_upload->field($model, 'imageFile')->fileInput() ?>

      <?= Html::a(
        'Upload',
        [
          'upload',
        ],
        [
          'title' => 'Upload Image',
          'data-method' => 'post',
          'class' => 'btn btn-primary pull-right',
        ]
      ); ?>

    </div>
  </div>
  <?php ActiveForm::end() ?>
  <?php Pjax::end(); ?>
</div>