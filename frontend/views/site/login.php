<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?php Pjax::begin(['id' => 'pjax-site-login']); ?>

    <p><i class="fa fa-info-circle"></i> Please fill out the following fields to login:</p>
    <div class="row">
        <div class="col-lg-12">
            <?php $form_site_login = ActiveForm::begin(
                [
                    'id' => 'form-site-login',
                    'options' =>
                    [
                        'data-pjax' => true,
                        'id' => 'dynamic-form-site-login',
                    ]
                ]
            ); ?>

            <?= $form_site_login->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form_site_login->field($model, 'password')->passwordInput() ?>

            <?= $form_site_login->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
            </div>

            <div class="form-group">

                <?= Html::a(
                    'Login',
                    [
                        'login',
                    ],
                    [
                        'class' => 'btn btn-primary pull-right',
                        'title' => 'Login',
                        'data-method' => 'post',
                    ]
                ) ?>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php Pjax::end(); ?>
</div>