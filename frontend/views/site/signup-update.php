<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(
        [
            'id' => 'form-signup',
            'options' =>
            [
                'enctype' => 'multipart/form-data'
            ]
        ]
    ); ?>

    <div class="row">

        <div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>Upload <?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="modal-upload-content"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="box">

        <div class="box-header with-border">
            <h3 class="modal-title"><i class="fa fa-user"></i> Basic Information</h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'username')->textInput(['disabled' => 'disabled']) ?>
                    <?= $form->field($model, 'email')->textInput(['disabled' => 'disabled']) ?>
                    <?= $form->field($model, 'firstName')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'lastName')->textInput() ?>
                    <?= $form->field($model, 'details')->textInput(['disabled' => 'disabled']) ?>
                </div>
                <div class="col-lg-6">
                    <div style="text-align: center;">
                        <img id="userImageId" title=<?= $model->photo ?> style="max-width: 160px;" src=<?= $photoUser ?> class="img-circle" alt="User Image">
                    </div>
                    <br />
                    <div style="text-align: center;">
                        <?php foreach ($arrayRadioAvatar as $key => $value) { ?>
                            <?php if ($key > 0) { ?>
                                <label>
                                    <input type="radio" name="RadioAvatar" value=<?= $arrayRadioAvatar[$key]['value'] ?> <?= $arrayRadioAvatar[$key]['checked'] ? "checked" : "" ?>>
                                    <img style="max-width: 60px;" src=<?= $arrayRadioAvatar[$key]['value'] ?> class="img-circle" alt="User Image">
                                </label>
                            <?php } ?>
                        <?php } ?>
                        <br />
                        <label>
                            <input type="radio" name="RadioAvatar" value=<?= $arrayRadioAvatar[0]['value'] ?> <?= $arrayRadioAvatar[0]['checked'] ? "checked" : "" ?>>
                            <img style="max-width: 60px;" src=<?= $arrayRadioAvatar[0]['value'] ?> class="img-circle" alt="User Image">

                            <?php
                            //  Html::button(
                            //     '<icon class="fa fa-cloud-upload"> </icon> Upload Image',
                            //     [
                            //         'value' => Url::to(['site/upload']),
                            //         'class' => 'btn btn-success btn-sm modal-upload-action',
                            //     ]
                            // ); 
                            ?>

                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>