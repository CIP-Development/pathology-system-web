<?php
return [
    'adminEmail' => 'p.juro@cgiar.org',
    'supportEmail' => 'p.juro@cgiar.org',
    'user.passwordResetTokenExpire' => 3600,
    'department' => 'Research Informatics Unit CIP',
    'ldap'      => [
        'host'      => 'ldaps://AZCGNEROOT2.CGIARAD.ORG',
        'port'      => 636,
        'domain'  => 'CGIAR.ORG',
    ],
    'institution' => "International Potato Center",
];
